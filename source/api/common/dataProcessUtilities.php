<?php
require_once(dirname(__FILE__) . "/../../vendor/autoload.php");

include_once(dirname(__FILE__) . "/dataFormat.php");
include_once(dirname(__FILE__) . "/mysql_config.php");
include_once(dirname(__FILE__) . "/config.php");
include_once(JSON_TO_MYSQL_PATH . "/include.classloader.php");
$classLoader->addToClasspath(JSON_TO_MYSQL_PATH);

class dataProcessUtilities {
	private static $dateColumns;

	private static $mysql;
	private static $_db;

	public function __construct() {
		$this->dateColumns = self::processDateColumns();

		$this->mysql = new MySQLConn(DATABASE_HOST, DATABASE_NAME, DATABASE_USER, DATABASE_PASS);
		$this->_db = new JSONtoMYSQL($this->mysql);
	}

	public  function __destruct() {
	}

	private function processDateColumns() {
		return ['YMD', 'START_DATE', 'NAV_E_DATE', 'OPR_DATE', 'NAV_S_DATE', 'END_DATE', 'S_IN_DATE', 'E_IN_DATE', 'ACTV_DATE', 'DTR_YMD1', 'DTR_YMD2', 'SITE_YMD', 'SICE_YMD', 'SBSITE_YMD', 'SBSICE_YMD', 'OFUN_YMD', 'FUTURE_YMD'];
	}

	private function date_format($date) {
		if(preg_match('/\d{4}\d{2}\d{2}/', $date)) {
			$d = DateTime::createFromFormat("Ymd", $date);
			return $d->format("Y-m-d");
		}

		return false;
	}

	private function convert_str($str) {
		return mb_convert_encoding($str, "UTF-8", "BIG5");
	}

	private function finalProcess($array, $isTrim = true) {
		foreach ($array as $key => $value) {
			if ($key == "CUR_NM") {
				$value = preg_replace('/\s+/', '', $value);
			}
			$array[$key] = $isTrim ? trim(self::convert_str($value)) : self::convert_str($value);
		}
		return $array;
	}

	private function specialTreatment($func_name, $obj) {
		$dataFormat = new DataFormat;

		$_processFormat = $dataFormat->getFormatFilter($func_name);
		if($_processFormat != false) {
			//var_dump($_processFormat);
			foreach ($_processFormat as $key => $value) {
				if((int)$obj[$key] == (int)$value) {
					$obj[$key] = 0;
				}
			}
		} 

		if(($func_name == 'FUNBAS' || $func_name == 'FUNBBA') && (int)$obj['AST_AMT'] != 0) {
			$rate = $this->_db->find(array("CUR_NM" => $obj['ABREV']), "ExchangeRate");
			if (!empty($rate)) {
				$obj['AST_AMT'] = $obj['AST_AMT'] * $rate[0]['VALUE'];
			}
		}

		if ($func_name == 'FUNBAS' && (int)$obj['AST_AMT'] != 0 ) {
			//sync FUNBAS and FUNBBA 'AST_AMT' unit ten thousand base.
			$obj['AST_AMT'] = $obj['AST_AMT'] / 10000;
		}

		if ($func_name == 'FUNBAS' && (int)$obj['FUN_S_AST'] != 0 ) {
			//sync FUNBAS and FUNBBA 'AST_AMT' unit ten thousand base.
			$obj['FUN_S_AST'] = $obj['FUN_S_AST'] / 10000;
		}

		return $obj;
	}

	private function writeCSV($csv, $fields) {
		$fp = fopen($csv, 'a');
		fputcsv($fp, $fields);
		fclose($fp);
	}

	private function filterFile($file) {
		if (preg_match("/.DS_Store/i", $file)) {
			return true;
		}

		if (preg_match("/_INV_OBJ/i", $file)) {
			return true;
		}

		if (preg_match("/_MEMO/i", $file)) {
			return true;
		}

		return false;
	}

	public function recursiveRemoveDirectory($directory)
	{
	    foreach(glob("{$directory}/*") as $file)
	    {
	        if(is_dir($file)) { 
	            recursiveRemoveDirectory($file);
	        } else {
	            unlink($file);
	        }
	    }
	    rmdir($directory);
	}

	public function mvZipFile($filePath) {
		$processedDir = DATA_FILE_PATH . '/processed/' . date("Ym", filemtime($filePath));
		if (!file_exists($processedDir)) {
		    mkdir($processedDir, 0777, true);
		}

		rename($filePath, $processedDir . '/' . basename($filePath));
	}

	public function unpackData($func_name, $data) {
		$dataFormat = new DataFormat;

		if($dataFormat->getFormat($func_name) == false) {
			return array();
		} 

		$builder = new Binary\SchemaBuilder;
		$schema = $builder->createFromArray($dataFormat->getFormat($func_name));

		$stream = new Binary\Stream\StringStream($data);
		$result = $schema->readStream($stream);

		return $result;
	}

	public function dataInToDB($func_name, $obj) {
		$action = $obj['ACTION'];
		unset($obj['ACTION']);

		foreach ($this->dateColumns as $column) {
			if(isset($obj[$column])) {
				if (self::date_format($obj[$column]) != false) {
					$obj[$column] = self::date_format($obj[$column]);
				}
			}
		}

		$_syncTable = DataFormat::getSyncTable($func_name);

		if ($action == "D") {
			// delete it to a table
			$this->_db->delete($obj, $func_name);
			if ($_syncTable != false) {
				$this->_db->delete($obj, $_syncTable[0]);
			}
			//echo "delete data[{$func_name}][{$action}].\n";

			return 0;
		}
		else {
			// do something...
			$obj = $this->specialTreatment($func_name, $obj);

			// save it to a table
			$res = $this->_db->save($obj, $func_name);
			if ($res == 'insert') {
				$obj['createdate'] = date("Y-m-d H:i:s");
				$this->_db->save($obj, $func_name);
			}

			if ($_syncTable != false) {
				$columns = $this->_db->getColumns($_syncTable[0]);

				$newObj = array_intersect_key($obj, array_flip($columns));

				$newObj['FUND_TYPE'] = $_syncTable[1];
				$res = $this->_db->save($newObj, $_syncTable[0]);

				if ($res == 'insert') {
					$newObj['createdate'] = date("Y-m-d H:i:s");
					$this->_db->save($newObj, $_syncTable[0]);
				}
			}
			//echo "save data [{$func_name}][{$action}][{$res}].\n";

			return 1;
		}

		return -1;
	}

	public function parseDataFile($filePath, $output) {
		$func_name = basename($filePath, ".dat");
		$csv = $output;
		if (file_exists($csv)) { unlink($csv); } // clear csv file.

		$line = 0;
		$resDBArray = array(
			'delete' => 0,
			'save' => 0,
			'unknown' => 0,
		);

		$fp = fopen($filePath, "r");

		$data = "";
		// Read the file line by line until the end
		while (!feof($fp)) {
			$data = $data . fgets($fp);
			//print substr($data, -3, 1);

			if(substr($data, -3, 1) == "$") {
				$unpackData = self::unpackData($func_name, $data);
				if (!empty($unpackData)) {
					$processData = self::finalProcess(self::unpackData($func_name, $data));
					//var_dump($processData);

					$res = self::dataInToDB($func_name, $processData);

					switch ($res) {
						case 0:
							$resDBArray['delete'] += 1;
							break;
						case 1:
							$resDBArray['save'] += 1;
							break;
						default:
							$resDBArray['unknown'] += 1;
							break;
					}

					if ($line == 0) {
						self::writeCSV($csv, array_keys($processData));
					}
					self::writeCSV($csv, array_values($processData));
				}
				
				$data = "";
				$line++;
			}
		}

		//var_dump($resDBArray);
		echo "[{$func_name}] save : " . $resDBArray['save'] . ", delete : " . $resDBArray['delete'] . ", unknown : " . $resDBArray['unknown'] . ".\n";

		// Close the file that no longer in use
		fclose($fp);
	}

	public function getDataFile($path) {
		foreach (glob($path . "/*.dat") as $filename) {
			if (self::filterFile(basename($filename))) {
				// skipped
				continue;
			}

		    echo "$filename size " . filesize($filename) . "\n";

		    $func_name = basename($filename, ".dat");
			$path = dirname($filename);
			$csv = OUTPUT_CSV_PATH . "/" . $func_name . '_' . date("Y-m-d-H") . '.csv';

		    self::parseDataFile($filename, $csv);
		}
	}
}
