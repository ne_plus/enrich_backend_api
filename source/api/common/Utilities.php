<?php
namespace Utilities;

class Utilities
{
    public static function filterObj( $obj, $filter ) {
      foreach ($filter as $key) {
        if (isset($obj[$key])) {
            unset($obj[$key]);
        }
      }
      return $obj;
    }

    public static function toOutput( $var, $filter, $isArray = false ) {
      if ($isArray) {
        $newVar = array();
        foreach ($var as $obj) {
            $newVar[] = self::filterObj($obj, $filter);
        }

        return $newVar;
      }

      return self::filterObj($var, $filter);
    }

    public static function validateDate($date) {
      $d = \DateTime::createFromFormat('Y-m-d', $date);
      return $d && $d->format('Y-m-d') === $date;

      //return strtotime(str_replace('/', '-', $date));
    }
}
