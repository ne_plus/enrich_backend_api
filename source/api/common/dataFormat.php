<?php
require_once(dirname(__FILE__) . "/../../vendor/autoload.php");

class DataFormat {

	private $_formats;
	private $_filter_formats;

    public function __construct() {
    	$this->_formats = array();
    	$this->_formats['FUNBAS'] = $this->FUNBAS();
    	$this->_formats['FUNBBA'] = $this->FUNBBA();
    	$this->_formats['FUNBBA_INV_OBJ'] = $this->FUNBBA_INV_OBJ();
    	$this->_formats['FUNBDT'] = $this->FUNBDT();
    	$this->_formats['FUNBNA'] = $this->FUNBNA();
    	$this->_formats['FUNBRT'] = $this->FUNBRT();
    	$this->_formats['FUBIDM'] = $this->FUBIDM();
    	$this->_formats['FUNBNQ'] = $this->FUNBNQ();
    	$this->_formats['FUNBAS_INV_OBJ'] = $this->FUNBAS_INV_OBJ();
    	$this->_formats['FUNDTR'] = $this->FUNDTR();
    	$this->_formats['FUNNAV'] = $this->FUNNAV();
    	$this->_formats['FUNRET1'] = $this->FUNRET1();
    	$this->_formats['FUNNAVC'] = $this->FUNNAVC();
    	$this->_formats['FUNNAVO'] = $this->FUNNAVO();
    	$this->_formats['FUNSCU'] = $this->FUNSCU();
    	$this->_formats['FUNIDM'] = $this->FUNIDM();
    	$this->_formats['FUNFEM'] = $this->FUNFEM();
    	$this->_formats['FUNNAQ'] = $this->FUNNAQ();
    	$this->_formats['FUNITM'] = $this->FUNITM();
    	$this->_formats['FUNCOD'] = $this->FUNCOD();
    	$this->_formats['FUNOBJ'] = $this->FUNOBJ();
    	$this->_formats['FUNARE'] = $this->FUNARE();
    	$this->_formats['FUNITC'] = $this->FUNITC();
    	$this->_formats['FUNITC_MEMO'] = $this->FUNITC_MEMO();
    	$this->_formats['FUNCOR'] = $this->FUNCOR();
    	$this->_formats['CT0CUR'] = $this->CT0CUR();
    }

    protected function setPartSize($size, $type = "Text") {
		return array(
			"_type" => $type,
			"size" => $size,
		);
	}

	protected function pairFormat($array) {
		$_format = array();
		foreach ($array as $key => $value) {
			$size = (int)$value;

			if(is_string($value)) {
				if(strpos($value, ".") == false) {
					$size = (int)$value;
				}
				else {
					$valueArray = explode(".", $value);
					$size = $valueArray[0];
				}
			}

			$_format[$key] = $this->setPartSize($size);
		}

		return $_format;
	}

	protected function pairFormatFilter($array) {
		$_format = array();
		foreach ($array as $key => $value) {
			if (!is_string($value)) {
				// skipped
				continue;
			}

			$_maxValue = '';
			$_LEFT = 0;
			$_RIGHT = 0;

			if(strpos($value, ".") == false) {
				$_LEFT = (int)$value;
			}
			else {
				$valueArray = explode(".", $value);
				$_LEFT = (int)$valueArray[0] - (int)$valueArray[1] - 1;
				$_RIGHT = (int)$valueArray[0];
			}
			
			$_maxValue = str_pad($_maxValue, $_LEFT, "9", STR_PAD_LEFT);
			if ($_RIGHT != 0) {
				$_maxValue .= ".";
				$_maxValue = str_pad($_maxValue, $_RIGHT, "9", STR_PAD_RIGHT);
			}
			$_format[$key] = $_maxValue;
		}

		return $_format;
	}

	public function getSyncTable($_format) {
		$_syncTable = array(
    		'FUNBAS' => [ 'FUNDATA', 0 ],
    		'FUNBBA' => [ 'FUNDATA', 1 ],

    		'FUNDTR' => [ 'FUNDTR_DATA', 0 ],
    		'FUNBDT' => [ 'FUNDTR_DATA', 1 ],

    		'FUNIDM' => [ 'FUNIDM_DATA', 0 ],
    		'FUBIDM' => [ 'FUNIDM_DATA', 1 ],

    		'FUNNAV' => [ 'FUNNAV_DATA', 0 ],
    		'FUNBNA' => [ 'FUNNAV_DATA', 1 ],

    		'FUNRET1' => [ 'FUNRET_DATA', 0 ],
    		'FUNBRT' => [ 'FUNRET_DATA', 1 ],
    	);

		if(isset($_syncTable[$_format])) {
			return $_syncTable[$_format];
		}

		return false;
	}

	public function getFormatLength($_format) {
		$dataFormat = $this->_formats[$_format];

		$len = 0;

		foreach ($dataFormat as $key => $value) {
			$len += $value;
		}

		return $len;
	}

	public function getFormat($_format) {
		if(isset($this->_formats[$_format])) {
			$dataFormat = $this->_formats[$_format];
			return $this->pairFormat($dataFormat);
		}

		return false;
	}
	
	public function getFormatFilter($_format) {
		if(isset($this->_formats[$_format])) {
			$dataFormat = $this->_formats[$_format];
			$res = $this->pairFormatFilter($dataFormat);
			return empty($res) ? false : $res;
		}

		return false;
	}

	protected function FUNBAS() {
		return array(
			'ACTION' => 1,
			'FUN_CODE' => 7,
			'NM_C' => 16,
			'NM_C_F' => 60,
			'NM_E' => 60,
			'START_DATE' => 8,
			'NAV_E_DATE' => 8,
			'FUN_MARK' => 1,
			'FUN_REG' => 15,
			'CUR_NM' => 6,
			'TYPE' => 1,
			'FUN_TYPE' => 1,
			'TXN_TYPE' => 1,
			'REG_TYPE' => 1,
			'OBJ_DESC' => 62,
			'ARE_DESC' => 62,
			'AST_AMT' => "12",
			'NAV_ORG' => "12.4",
			'FUN_INTT' => 1,
	        'F_NM_C' => 50,
	        'K_NM_C' => 50,
	        'M_COST' => 20,
	        'K_COST' => 20,
	        'R_COST' => 20,
	        'O_COST' => 20,
	        'MF_REP' => 1,
	        'AF_REP' => 1,
	        'INV_PER' => "6.2",
	        'M_NM_C' => 10,
	        'OPR_DATE' => 8,
	        'EXP' => 40,
	        'EDUCATE' => 30,
	        'ABREV' => 3,
	        'OBJ_CODE' => 14,
	        'ARE_CODE' => 14,
	        'MGR_ID' => 5,
	        'RTN_RANGE' => "7.2",
	        'CO_CODE' => 9,
	        'RR_RATE' => 1,
	        'FUN_S_AST' => "12", //new
	        'ITC_CODE' => 5,
	        'ITC_NM_C' => 50,
	        'BUY_CURR' => 3,
	        'SNG_LO_P' => "12",
	        'REG_LO_P' => "8",
	        'H_COST' => 20,
	        'NAV_FLAG' => 1,
	        'TYPE_N' => 5,
	        'NAV_S_DATE' => 8,
	        'FUN_BMRK' => 1,
	        'INV_LIMI' => 60,
	        'ORG_CODE' => 5, // new
	        'FUN_INTD' => 23,
	        'END_DATE' => 8,
	        'FCT_CODE' => 5,
	        'KCT_CODE' => 5,
	        'TYPE_F' => 1, // new
	        'TRAD_UNIT' => "5",
	        'ISIN_CODE' => 12,
	        'ETF_TYPE' => 1,
	        'BS_CODE' => 6,
		);
	}

	protected function FUNBBA() {
		return array(
			'ACTION' => 1,
	    	'FUN_CODE' => 7,
	    	'NM_C' => 16, 
	    	'NM_C_F' => 60,
	    	'NM_E' => 60,
	    	'START_DATE' => 8,
	    	'NAV_E_DATE' => 8,
	    	'FUN_MARK' => 1,
	    	'FUN_REG' => 15,
	    	'CUR_NM' => 6,
	    	'TYPE' => 1,
	    	'FUN_TYPE' => 1,
	    	'TXN_TYPE' => 1,
	    	'REG_TYPE' => 1,
	    	'OBJ_DESC' => 62,
	    	'ARE_DESC' => 62,
	    	'AST_AMT' => "12",
	    	'NAV_ORG' => "12.4",
	    	'FUN_INTT' => 1,
	    	'F_NM_C' => 50,
	    	'K_NM_C' => 50,
	    	'M_COST' => 20,
	    	'K_COST' => 20,
	    	'R_COST' => 20,
	    	'O_COST' => 20,
	    	'MF_REP' => 1,
	    	'AF_REP' => 1,
	    	'INV_PER' => "6.2",
	    	'M_NM_C' => 10,
	    	'OPR_DATE' => 8,
	    	'EXP' => 40,
	    	'EDUCATE' => 30,
	    	'ABREV' => 3,
	    	'OBJ_CODE' => 14,
	    	'ARE_CODE' => 14,
	    	'MGR_ID' => 5,
	    	'RTN_RANGE' => "7.2",
	    	'CO_CODE' => 9,
	    	'RR_RATE' => 1,
	    	'ITC_CODE' => 5,
	    	'ITC_NM_C' => 50,
	    	'ISIN_CODE' => 12,
	    	'FSC_ID' => 30,
	    	'BUY_CURR' => 3,
	    	'SNG_LO_P' => "12",
	    	'REG_LO_P' => "8",
	    	'H_COST' => 20,
	    	'NORPT_RMK' => 12,
	    	'NAV_FLAG' => 1,
	    	'TYPE_N' => 5,
	    	'AST_CUR' => 3,
	    	'NAV_S_DATE' => 8,
	    	'FUN_BMRK' => 1,
	    	'INV_LIMI' => 60,
	    	'FUN_INTD' => 23,
	    	'END_DATE' => 8,
	    	'S_IN_DATE' => 8,
	    	'E_IN_DATE' => 8,
	    	'FUN_LAMT' => 2,
	    	'FCT_CODE' => 5,
	    	'KCT_CODE' => 5,
	    	'ACTV_DATE' => 8,
	    	'TRAD_UNIT' => "5",
		);
	}

	protected function FUNBBA_INV_OBJ() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 7,
	        'INV_OBJ' => 255, // temp
		);
	}

	protected function FUNBDT() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 4,
	        'DTR_YMD1' => 8,
	        'DTR_AMT' => '12.6',
	        'DTR_YMD2' => 8,
	        'DTR_B_YMD' => 8,
	        'ABREV' => 3,
	        'DTR_RAT_Y' => '12.4',
		);
	}

	protected function FUNBDT_Filter() {
		return array(
	        'DTR_AMT' => "12.6",
		);
	}

	protected function FUNBNA() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 7,
	        'YMD' => 8,
	        'CUR_NM' => 10,
	        'NAV_ORG' => "12.4",
	        'NAV_NT' => "12.4",
	        'RET_1D' => "8.2",
	        'RET_1M' => "8.2",
	        'RET_3M' => "8.2",
	        'RET_6M' => "8.2",
	        'RET_9M' => "8.2",
	        'RET_1Y' => "8.2",
	        'RET_2Y' => "8.2",
	        'RET_3Y' => "8.2",
	        'RET_CUR_Y' => "8.2",
	        'RET_BGN' => "8.2",
	        'RET_L_M' => "8.2",
	        'RET_L_Q' => "8.2",
	        'UP_DN' => "10.4",
	        'RET_5Y' => "8.2",
	        'UP_DN_NT' => "12.4",
	        'RET_1D_NT' => "8.2",
	        'RET_1W' => "8.2",
		);
	}

	protected function FUNBRT() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 4,
	        'YMD' => 8,
	        'RET_3M' => "7.2",
	        'RET_6M' => "7.2",
	        'RET_9M' => "7.2",
	        'RET_1Y' => "7.2",
	        'RET_2Y' => "7.2",
	        'RET_3Y' => "7.2",
	        'RET_5Y' => "7.2",
		);
	}

	protected function FUBIDM() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 4,
	        'IDX_CODE' => 8,
	        'YM' => 6,
	        'DATA_VALUE' => "7.2",
		);
	}

	protected function FUNBNQ() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 4,
	        'YQ' => 5,
	        'NAV_ORG' => "12.4",
	        'NAV_NT' => "12.4",
	        'RTN_ORG' => "7.2",
	        'RTN_NT' => "7.2",
	        'RTN_6M' => "7.2",
	        'RTN_9M' => "7.2",
	        'RTN_1Y' => "7.2",
	        'RTN_2Y' => "7.2",
	        'RTN_3Y' => "7.2",
	        'CUR_NM' => 10,
	        'UP_DN' => "12.4",
	        'UP_DN_NT' => "12.4",
	        'RET_3M' => "8.2",
	        'RET_6M' => "8.2",
	        'RET_9M' => "8.2",
	        'RET_1Y' => "8.2",
	        'RET_2Y' => "8.2",
	        'RET_3Y' => "8.2",
	        'RET_5Y' => "8.2",
		);
	}

	protected function FUNBAS_INV_OBJ() {
		return array(
			'ACTION' => 1,
        	'FUN_CODE' => 7,
        	'INV_OBJ' => 255, //temp
		);
	}

	protected function FUNDTR() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 4,
	        'DTR_YMD1' => 8,
	        'DTR_AMT' => "12.6",
	        'DTR_YMD2' => 8,
	        'DTR_B_YMD' => 8,
	        'ABREV' => 3,
	        'DTR_RAT_Y' => "12.4",
		);
	}

	protected function FUNNAV() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 7,
	        'YMD' => 8,
	        'NAV_ORG' => "12.4",
	        'N_C_R' => "7.2",
	        'N_C_D' => "12.4",
	        'PRICE' => "7.2",
	        'RET_1D' => "8.2",
	        'AVG_N_C_R' => "7.2",
	        'RET_1M' => "8.2",
	        'RET_3M' => "8.2",
	        'RET_6M' => "8.2",
	        'RET_9M' => "8.2",
	        'RET_1Y' => "8.2",
	        'RET_2Y' => "8.2",
	        'RET_3Y' => "8.2",
	        'RET_CUR_Y' => "8.2",
	        'RET_BGN' => "8.2",
	        'RET_L_M' => "8.2",
	        'RET_L_Q' => "8.2",
	        'UP_DN' => "10.4",
	        'CLOSE_UP_DN' => "7.2",
	        'CLOSE_UP_DN_R' => "7.2",
	        'RET_5Y' => "8.2",
	        'CUR_NM' => 10,
	        'NAV_NT' => "12.4",
	        'UP_DN_NT' => "12.4",
	        'RET_1D_NT' => "8.2",
	        'RET_1W' => "8.2",
		);
	}

	protected function FUNRET1() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 4,
	        'YMD' => 8,
	        'RET_3M' => "7.2",
	        'RET_6M' => "7.2",
	        'RET_9M' => "7.2",
	        'RET_1Y' => "7.2",
	        'RET_2Y' => "7.2",
	        'RET_3Y' => "7.2",
	        'RET_5Y' => "7.2",
		);
	}

	protected function FUNNAVC() {
		return array(
			'ACTION' => 1,
	        'YMD' => 8,
	        'LSC_CODE' => 6,
	        'NM_C' => 12,
	        'MKT' => 4,
	        'ZCLOSE' => "7.2",
	        'PREV_CLOSE' => "7.2",
	        'AVG_CLOSE_3M' => "7.2",
	        'STKNO' => "15.3",
	        'NAV_NT' => "12.4",
	        'CO_CODE' => 9,
	        'CO_NM_C' => 14,
		);
	}

	protected function FUNNAVO() {
		return array(
			'ACTION' => 1,
	        'YMD' => 8,
	        'FUN_CODE' => 7,
	        'NM_C_F' => 60,
	        'CO_CODE' => 9,
	        'NAV_NT' => "12.4",
	        'PREV_NAV' => "12.4",
	        'AVG_NAV_NT_3M' => "12.4",
		);
	}

	protected function FUNSCU() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 4,
	        'LIST_CODE' => 6,
	        'NM_C4' => 8,
	        'CT_CODE' => 5,
	        'SCU_CODE' => 4,
		);
	}

	protected function FUNIDM() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 4,
	        'IDX_CODE' => 8,
	        'YM' => 6,
	        'DATA_VALUE' => "7.2",
		);
	}

	protected function FUNFEM() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 4,
	        'YM' => 6,
	        'DC_HDCHG' => "18",
	        'DC_HDCHGR' => "5.2",
	        'DC_TRNTAX' => "18",
	        'DC_TRNTAXR' => "5.2",
	        'DC_TOTAL' => "18",
	        'DC_RATIO' => "5.2",
	        'EP_MGNFEE' => "18",
	        'EP_MGNFEER' => "5.2",
	        'EP_KEPFEE' => "18",
	        'EP_KEPFEER' => "5.2",
	        'EP_GENFEE' => "18",
	        'EP_GENFEER' => "5.2",
	        'EP_OTHFEE' => "18",
	        'EP_OTHFEER' => "5.2",
	        'EP_TOTAL' => "18",
	        'EP_RATIO' => "5.2",
	        'EP_DC_TOT' => "18",
	        'EP_DC_RAT' => "5.2",
		);
	}

	protected function FUNNAQ() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 4,
	        'YQ' => 5,
	        'NAV_ORG' => "12.4",
	        'NAV_NT' => "12.4",
	        'RTN_ORG' => "7.2",
	        'RTN_NT' => "7.2",
	        'RTN_6M' => "7.2",
	        'RTN_9M' => "7.2",
	        'RTN_1Y' => "7.2",
	        'RTN_2Y' => "7.2",
	        'RTN_3Y' => "7.2",
	        'CUR_NM' => 10,
	        'UP_DN' => "12.4",
	        'UP_DN_NT' => "12.4",
	        'N_C_R' => "7.2",
	        'N_C_D' => "12.4",
	        'PRICE' => "7.2",
	        'CLOSE_UP_DN' => "7.2",
	        'CLOSE_UP_DN_R' => "7.2",
	        'RET_3M' => "8.2",
	        'RET_6M' => "8.2",
	        'RET_9M' => "8.2",
	        'RET_1Y' => "8.2",
	        'RET_2Y' => "8.2",
	        'RET_3Y' => "8.2",
	        'RET_5Y' => "8.2",
	        'RET_CUR_Y' => "8.2",
	        'RET_BGN' => "8.2",
		);
	}

	protected function FUNITM() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 4,
	        'ITC_CODE' => 5,
	        'TYPE' => 1,
		);
	}

	protected function FUNCOD() {
		return array(
			'ACTION' => 1,
	        'FUN_CODE' => 4,
	        'NM_C' => 16,
		);
	}

	protected function FUNOBJ() {
		return array(
			'ACTION' => 1,
	        'OBJ_CODE' => 4,
	        'OBJ_DESC' => 20,
		);
	}

	protected function FUNARE() {
		return array(
			'ACTION' => 1,
	        'ARE_CODE' => 4,
	        'ARE_DESC' => 20,
		);
	}

	protected function FUNITC() {
		return array(
			'ACTION' => 1,
	        'ITC_CODE' => 5,
	        'NM_C' => 50,
	        'PRE_NM' => 10,
	        'GEL_NM' => 10,
	        'ZIP_CODE' => 5,
	        'ADDRESS' => 80,
	        'TEL' => 15,
	        'FAX' => 15,
	        'URL' => 70,
	        'END_DATE' => 8,
	        'CT_CODE' => 5,
	        'SITE_YMD' => 8,
	        'SICE_YMD' => 8,
	        'SBSITE_YMD' => 8,
	        'SBSICE_YMD' => 8,
	        'OFUN_YMD' => 8,
	        'FUTURE_YMD' => 8,
		);
	}

	protected function FUNITC_MEMO() {
		return array(
			'ACTION' => 1,
	        'ITC_CODE' => 5,
	        'MEMO' => 255, //trmp
		);
	}

	protected function FUNCOR() {
		return array(
			'ACTION' => 1,
	        'TYPE' => 1,
	        'XCT_CODE' => 5,
	        'NM_C_F' => 50,
	        'NM_E_F' => 50,
	        'ADD_C' => 70,
	        'ADD_E' => 70,
	        'TEL' => 31,
	        'FAX' => 15,
	        'CON_PER' => 30,
	        'PRE_NM_C' => 30,
	        'PRE_NM_E' => 50,
	        'GEL_NM_C' => 30,
	        'GEL_NM_E' => 50,
	        'URL' => 60,
		);
	}

	protected function CT0CUR() {
		return array(
			'ACTION' => 1,
	        'CUR_CODE' => 3,
	        'NM_C' => 8,
	        'NM_E' => 30,
	        'NAT_CODE' => 3,
	        'NTN_CODE' => 3,
	        'ITEM_CODE' => 8,
		);
	}
}