<?php
/**
 * sample configuration, you can use it as base for
 * manual configuration. For easier setup you can use setup/
 */

define("COMMON_ROOT", dirname(__FILE__) . "/");
define("JSON_TO_MYSQL_PATH", COMMON_ROOT . "json-to-mysql/");

define("RAW_DATA_ROOT", dirname(__FILE__) . "/../../data");
define("OUTPUT_CSV_FOLDER", "/csv");
define("DATA_FILE_FOLDER", "/raw_data");
define("STOCKAI_FILE_FOLDER", "/stockAI");

define("ECO_LIST_JSON", "ecoList.json");

define("FUNDS_COMBINATION_JSON", "fundsCombination.json");

define("OUTPUT_CSV_PATH", RAW_DATA_ROOT . OUTPUT_CSV_FOLDER);
define("DATA_FILE_PATH", RAW_DATA_ROOT . DATA_FILE_FOLDER);
define("STOCKAI_FILE_PATH", RAW_DATA_ROOT . STOCKAI_FILE_FOLDER);

define("FTP_HOST", "ftp.infotimes.com.tw");
define("FTP_USER", "ENRIC1");
define("FTP_PASS", "ENRICH1");

define("STOCKAI_URL", "https://stock-ai.com/ddl?s=%s&r=%s");

//APN
//certkey_development
define("APN_CERT", dirname(__FILE__) . "/pushNotification/certkey_production.pem");
define("APN_CERT_DEV", dirname(__FILE__) . "/pushNotification/certkey_development.pem");
define("PEMpassphrase", dirname(__FILE__) . "12345");
//GCM
define("GOOGLE_API_KEY", "AIzaSyBnoEa5bL8qLtTyxrRs9ewnScxQxKeyco8");
?>