<?php
namespace Slim\Extras;

use RuntimeException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use Slim\Http\Body;

use UserInfo;

class Jsonp
{

    public function __construct()
    {
    }

    /**
     * Invoke middleware
     *
     * @param  RequestInterface $request PSR7 request object
     * @param  ResponseInterface $response PSR7 response object
     * @param  callable $next Next middleware callable
     *
     * @return ResponseInterface PSR7 response object
     */
    public function __invoke( RequestInterface $request, ResponseInterface $response, callable $next )
    {
        $callback = $request->getParam('callback');
        
        $response = $next($request, $response);

        //If the JSONP callback parameter is set then wrap the response body in the original
        //callback string.
        if(!empty($callback)){
            //The response becomes a javascript response
           	$headers = $response->getHeaders();
      		$body = $response->getBody();

      		$jsonp_response = htmlspecialchars($callback) . "(" .$response->getBody() . ")";

      		$response = $response->withBody(
		       new Body(fopen('php://temp', 'r+'))
		      );
      		$response->write($jsonp_response);
      		$response = $response->withAddedHeader('Content-Type','application/javascript');
        }

        return $response;
    }
}
