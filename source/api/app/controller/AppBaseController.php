<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/../../common/Utilities.php");

class AppBaseController {
    protected $ci;
    protected $_controller;

    protected $_filterArr;

    //Constructor
    public function __construct() {
        //$this->ci = $ci;
    }

    protected function setID($args) {

    }

    public function doAction($request, $response, $args) {
        if($request->isGet()) {
          
        }
        else if($request->isPost() || $request->isPut()) {
          
        }
        else if($request->isDelete()) {
          
        }

        return $response;
    }

    protected function statusCodeMsg($code) {
        switch ($code) {
            case 100: $text = 'Continue'; break;
            case 101: $text = 'Switching Protocols'; break;
            case 200: $text = 'OK'; break;
            case 201: $text = 'Created'; break;
            case 202: $text = 'Accepted'; break;
            case 203: $text = 'Non-Authoritative Information'; break;
            case 204: $text = 'No Content'; break;
            case 205: $text = 'Reset Content'; break;
            case 206: $text = 'Partial Content'; break;
            case 300: $text = 'Multiple Choices'; break;
            case 301: $text = 'Moved Permanently'; break;
            case 302: $text = 'Moved Temporarily'; break;
            case 303: $text = 'See Other'; break;
            case 304: $text = 'Not Modified'; break;
            case 305: $text = 'Use Proxy'; break;
            case 400: $text = 'Bad Request'; break;
            case 401: $text = 'Unauthorized'; break;
            case 402: $text = 'Payment Required'; break;
            case 403: $text = 'Forbidden'; break;
            case 404: $text = 'Not Found'; break;
            case 405: $text = 'Method Not Allowed'; break;
            case 406: $text = 'Not Acceptable'; break;
            case 407: $text = 'Proxy Authentication Required'; break;
            case 408: $text = 'Request Time-out'; break;
            case 409: $text = 'Conflict'; break;
            case 410: $text = 'Gone'; break;
            case 411: $text = 'Length Required'; break;
            case 412: $text = 'Precondition Failed'; break;
            case 413: $text = 'Request Entity Too Large'; break;
            case 414: $text = 'Request-URI Too Large'; break;
            case 415: $text = 'Unsupported Media Type'; break;
            case 500: $text = 'Internal Server Error'; break;
            case 501: $text = 'Not Implemented'; break;
            case 502: $text = 'Bad Gateway'; break;
            case 503: $text = 'Service Unavailable'; break;
            case 504: $text = 'Gateway Time-out'; break;
            case 505: $text = 'HTTP Version not supported'; break;
            default:
                $text = 'Unknown http status code "' . htmlentities($code) . '"';
            break;
        }

        return $text;
    }

    public function getPostParsedBody($request) {
        $allPostPutVars = $request->getParsedBody();

        if(empty($allPostPutVars)) {
            return false;
        }

        return $allPostPutVars;
    }

    public function getRequestBodyJSON($request) {
        $json = $request->getBody();
        if (!json_decode($json, true)) {
            return false;
        }

        return json_decode($json, true);
    }

    public function handleError($response, $statusCode, $body = '') {
    	$response = $response->withStatus($statusCode);

        if(empty($body)) {
            $body = self::statusCodeMsg($statusCode);
        }
    	//$response->getBody()->write($body);
        $errMsg = array('error' => $body);
        $response = $response->withJson($errMsg);

    	return $response;
    }

    public function paramStartAndEndDate($params) {
        $params = empty($params) ? array() : explode('/', $params);
        //$params = explode('/', $request->getAttribute('params'));

        $startDate = '';
        $endDate = '';
        if (count($params) > 0) {
            $year = $params[0];
            $month = !empty($params[1]) ? $params[1] : '1';
            $day = !empty($params[2]) ? $params[2] : '1';

            $date = new DateTime();
            $date->setDate((int)$year, (int)$month, (int)$day);
            $startDate = $date->format('Y-m-d');

            if (empty($params[1])) {
                $date->modify('last day of December');
            }
            else if (empty($params[2])) {
                $date->modify('last day of this month');
            }
            $endDate = $date->format('Y-m-d');
        }
        else {
            $startDate = null;
            $endDate = null;

            return array();
        }

        return array(
            'startDate' => $startDate,
            'endDate' => $endDate,
            );
    }

    public function getRequestPageInfo($request) {
    	if (empty($request->getParam('page'))) {
    		$offset = 1;
    	}
    	else {
    		$offset = $request->getParam('page') < 1 ? 1 : $request->getParam('page');
    	}

    	if (empty($request->getParam('page_limit'))) {
    		$per_page = 20;
    	}
    	else {
    		$per_page = $request->getParam('page_limit') < 1 ? 20 : $request->getParam('page_limit');
    	}

    	return array(
    		'offset' => $offset,
    		'count' => $per_page,
    		);
    }

    public function getFilterOptions($request) {
        $filters = array();

        if(!empty($request->getParam('filter_fields')) && !empty($request->getParam('filter_values'))) {
            $filter_fields = explode(',', $request->getParam('filter_fields'));
            $filter_values = explode(',', $request->getParam('filter_values'));

            //$filters['and'] = array_map(function($n, $m) { return array($n => $m); }, $filter_fields, $filter_values);
            $filters['and'] = array_map(function($n, $m) { 
                $operator = "=";
                if(preg_match("/^%/i", $m) || preg_match("/%$/i", $m)) {
                    $operator = "LIKE";
                }
                return array("key" => $n, "value" => $m, "operator" => $operator); 
            }, $filter_fields, $filter_values);
        }

        if (!empty($request->getParam('start_date')) && Utilities\Utilities::validateDate($request->getParam('start_date'))) {
            $date = new DateTime();
            $endDate = $date->format('Y-m-d');
            if (!empty($request->getParam('end_date')) && Utilities\Utilities::validateDate($request->getParam('end_date'))) {
                $endDate = $request->getParam('end_date');
            }
            $filters['date'] = array(
                'startDate' => $request->getParam('start_date'),
                'endDate' => $endDate,
            );
        }

        if(!empty($request->getParam('qs'))) {
            $filters['qs'] = "%".$request->getParam('qs')."%";
        }

        return $filters;
    }

    public function checkGetAllData($request) {
        $ret = false;
        if (!empty($request->getParam('data_all'))) {
            $ret = $request->getParam('data_all') == 1 ? true : false;
        }

        return $ret;
    }

    public function checkArrayData($data, $KeyArray) {
        foreach ($KeyArray as $key) {
            if (empty($data[$key])) {
                return false;
            }
        }

        return true;
    }

    public function toOutput($response, $obj) {
        if(!empty($this->_filterArr)) {
            $obj = Utilities\Utilities::toOutput($obj, $this->_filterArr);
        }

        $response = $response->withJson($obj);

        return $response;
    }
}
?>