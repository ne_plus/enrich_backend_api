<?php
use League\Csv\Reader;

include_once(dirname(__FILE__) . "/AppBaseController.php");
include_once(dirname(__FILE__) . "/../classes/DAO/News.php");

class NewsController extends AppBaseController {

  protected $_dao;

  public function __construct() {
    $this->_dao = new News();
  }

  public function doAction($request, $response, $args) {
    if($request->isGet()) {
      $pageInfo = self::getRequestPageInfo($request);
      $filter_options['date'] = self::paramStartAndEndDate($request->getAttribute('params'));
      $filter_options = array_merge($filter_options, self::getFilterOptions($request));
      //$filter_options['and'][] = array('FUN_CODE' => $fundID);

      $res = $this->_dao->getData($filter_options, array(), $pageInfo);

      if (count($res) > 0) {
          //$response->getBody()->write(self::$dbTable);
        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!! [$fundID]");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $datas = self::getRequestBodyJSON($request);

      if(empty($datas)) {
        $response = self::handleError($response, 400, "Failed!!");
        return $response;
      }
      
      $count = 0;
      foreach ($datas as $data) {
        if ($this->_dao->save($data)) {
          $count++;
        }
      }

      if($count > 0) {
        $response = $response->withJson(array("msg" => "Susses, save $count items."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }
    else if($request->isDelete()) {
      $datas = self::getRequestBodyJSON($request);

      if(empty($datas)) {
        $response = self::handleError($response, 400, "Failed!!");
        return $response;
      }

      $count = 0;
      foreach ($datas as $data) {
        if ($this->_dao->deleteByPks($data)) {
          $count++;
        }
      }

      if($count > 0) {
        $response = $response->withJson(array("msg" => "Susses, delete $count items."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }

    return $response;
  }
}
?>