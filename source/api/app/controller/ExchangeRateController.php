<?php
use League\Csv\Reader;

include_once(dirname(__FILE__) . "/AppBaseController.php");
include_once(dirname(__FILE__) . "/../classes/DAO/ExchangeRate.php");
include_once(dirname(__FILE__) . "/../classes/DAO/CT0CUR.php");

class ExchangeRateController extends AppBaseController {

  protected $_dao;

  public function __construct() {
    $this->_dao = new ExchangeRate();
  }

  public function doUploadAction($request, $response, $args) {
    $files = $request->getUploadedFiles();

    if (empty($files['csv'])) {
      throw new Exception('Expected a newfile');
    }

    $csv = $files['csv'];
    $csvFile = !is_object($csv) ? $csv[0]->file : $csv->file;
    //var_dump($csv);
    //return;
    $count = 0;

    try {
      $inputCsv = Reader::createFromPath($csvFile);
      $inputCsv->setDelimiter(',');
      $inputCsv->setEncodingFrom('UTF-8');

      //get the header
      $headers = array();
      $dataArray = array();


      foreach ($inputCsv as $index => $row) {
        //do something meaningful here with $row !!
        //$row is an array where each item represent a CSV data cell
        //$index is the CSV row index

        if($index == 0) {
          $headers = $row;
          continue;
        }
        $newRow = array_combine($headers, $row);
        $YMD = (new DateTime($newRow['DATE']))->format('Y-m-d');
        unset($newRow['DATE']);
        //var_dump($newRow);

        // save it to a table
        foreach ($newRow as $CUR_NM => $VALUE) {
          $obj = array(
            'YMD' => $YMD,
            'CUR_NM' => $CUR_NM,
            'VALUE' => $VALUE,
          );
          //var_dump($obj);
          $this->_dao->save($obj);
          $count++;
        }

        $dataArray[] = $newRow;

        if($index == 300) {
          break;
        }
      }
    } catch (Exception $e) {
        echo 'Caught exception: ',  $e->getMessage(), "\n";
        return;
    }

    if($count > 0) {
      $response = $response->withJson(array('msg' => "Susses, save $count items."));
    }
    else {
      $response = self::handleError($response, 404, "Failed!!");
    }

    return $response;
  }

  public function doAction($request, $response, $args) {
    if($request->isGet()) {
      $pageInfo = self::getRequestPageInfo($request);
      $filter_options = self::getFilterOptions($request);

      $res = array();

      if (!empty($args['CUR_NM']) && $args['CUR_NM'] != "all") {
        $filter_options['and'][] = DAO::setCondition('CUR_NM', $args['CUR_NM']);
      }

      if ($args['CUR_NM'] == "all") {
        $res = $this->_dao->getSingleCurInfo();
      }
      else {
        $res = $this->_dao->getData($filter_options, array(), $pageInfo);
      }

      if (count($res) > 0) {
          //$response->getBody()->write(self::$dbTable);
        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!! [$fundID]");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $datas = self::getRequestBodyJSON($request);

      if(empty($datas)) {
        $response = self::handleError($response, 400, "Failed!!");
        return $response;
      }
      
      $count = 0;
      foreach ($datas as $data) {
        if ($this->_dao->save($data)) {
          $count++;
        }
      }

      if($count > 0) {
        $response = $response->withJson(array('msg' => "Susses, save $count items."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }
    else if($request->isDelete()) {
      $datas = self::getRequestBodyJSON($request);

      if(empty($datas)) {
        $response = self::handleError($response, 400, "Failed!!");
        return $response;
      }

      $count = 0;
      foreach ($datas as $data) {
        if ($this->_dao->deleteByPks($data)) {
          $count++;
        }
      }

      if($count > 0) {
        $response = $response->withJson(array('msg' => "Susses, delete $count items."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }

    return $response;
  }

  public function getCurrMapTable($request, $response, $args) {
    $_CT0CUR = new CT0CUR();

    $pageInfo = self::getRequestPageInfo($request);
    $filter_options = self::getFilterOptions($request);

    $res = array();

    if (!empty($args['CUR_NM'])) {
      $filter_options['and'][] = DAO::setCondition('CUR_CODE', $args['CUR_NM']);
    }

    $res = $_CT0CUR->getData($filter_options, array(), $pageInfo);

    if (count($res) > 0) {
        //$response->getBody()->write(self::$dbTable);
      $response = $response->withJson($res);
    }
    else {
      $response = self::handleError($response, 404, "Not Found!!");
    }

    return $response;
  }
}
?>