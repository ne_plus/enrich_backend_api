<?php
include_once(dirname(__FILE__) . "/AppBaseController.php");
include_once(dirname(__FILE__) . "/../classes/MarketInfo.php");
include_once(dirname(__FILE__) . "/../classes/DAO/MarketBenchMark.php");
include_once(dirname(__FILE__) . "/../classes/DAO/MarketECO.php");
include_once(dirname(__FILE__) . "/../classes/DAO/CT0CUR.php");
include_once(dirname(__FILE__) . "/../classes/DAO/ECOList.php");
include_once(dirname(__FILE__) . "/../classes/DAO/MarketAlert.php");

include_once(dirname(__FILE__) . "/../../common/config.php");

class MarketController extends AppBaseController {

  public function __construct() {
  }

  protected function setLevel($args) {
    if (empty($args['marketLevel'])) {
      return null;
    }

    $LEVEL =  $args['marketLevel'];
    $this->_controller = new MarketInfo($LEVEL);

    return $LEVEL;
  }

  protected function setID($args) {
    if (empty($args['marketID'])) {
      return null;
    }

    $ID =  $args['marketID'];
    $this->_controller->setmarketID($ID);

    return $ID;
  }

  protected function sortData($dataArray, $order = 'ASC') {
    $cmp = function($a, $b) {
        return (strtotime($a["YMD"]) >= strtotime($b["YMD"]));
    };

    usort($dataArray, $cmp);

    return $dataArray;
  }

  public function doShow($request, $response, $args) {
    $this->setLevel($args);
    $pageInfo = self::getRequestPageInfo($request);

    $res = $this->_controller->marketDAO->getData(array(), array(), $pageInfo);

    if (count($res) > 0) {
      $response = $response->withJson($res);
    }
    else {
      $response = self::handleError($response, 404, "Not Found!!");
    }

    return $response;
  }

  public function doAction($request, $response, $args) {
    $this->setLevel($args);
    $marketID = $this->setID($args);

    if($request->isGet()) {
      if (!empty($this->_controller->marketInfo)) {
        $response = $response->withJson($this->_controller->getMarketInfo());
      }
      else {
        $response = self::handleError($response, 404, "Not Found!! [$marketID]");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $data = self::getRequestBodyJSON($request);

      if(empty($data)) {
        $response = self::handleError($response, 400, "Failed!!");
        return $response;
      }

      if(!empty($marketID)) {
        if ($this->_controller->save($data)) {
          $response = $response->withJson($this->_controller->marketInfo);
        }
        else {
          $response = self::handleError($response, 404, "Failed!! [$marketID]");
        }
      }
      else {
        $count = 0;
        foreach ($data as $value) {
          if ($this->_controller->save($value)) {
            $count++;
          }
        }

        if($count > 0) {
          $response = $response->withJson(array("msg" => "Susses, save $count items."));
        }
        else {
          $response = self::handleError($response, 404, "Failed!!");
        }
      }
    }
    else if($request->isDelete()) {
      $data = self::getRequestBodyJSON($request);

      if(!empty($marketID)) {
        if ($this->_controller->del()) {
          $response = $response->withJson($this->_controller->marketInfo);
        }
        else {
          $response = self::handleError($response, 404, "Failed!! [$marketID]");
        }
      }
      else {
        if(empty($data)) {
          $response = self::handleError($response, 400, "Failed!!");
          return $response;
        }

        $count = 0;
        foreach ($data as $value) {
          if ($this->_controller->marketDAO->deleteByID($value['ID'])) {
            $count++;
          }
        }

        if($count > 0) {
          $response = $response->withJson(array("msg" => "Susses, delete $count items."));
        }
        else {
          $response = self::handleError($response, 404, "Failed!!");
        }
      }
    }

    return $response;
  }

  protected function doGetOthers($method, $request, $response, $args) {
    $this->setLevel($args);
    $marketID = $this->setID($args);

    $res =$this->_controller->superMarketInfo;
    if($method) {
      $res = $this->_controller->getSubMarketList();
    }
    
    if (count($res) > 0) {
      $response = $response->withJson($res);
    }
    else {
      $response = self::handleError($response, 404, "Not Found!! [$marketID]");
    }

    return $response;
  }

  public function doShowSuperMarket($request, $response, $args) {
    return $this->doGetOthers(false, $request, $response, $args);
  }

  public function doShowSubMarket($request, $response, $args) {
    return $this->doGetOthers(true, $request, $response, $args);
  }

  public function doBenchMarkAction($request, $response, $args) {
    $this->setLevel($args);
    // the feature just enable marketLevelC
    if ($this->_controller->marketLevel != 'MarketLevelC') {
      $response = self::handleError($response, 400, "Not [$this->_controller->marketList[3]]");
      return $response;
    }

    $marketID = $this->setID($args);

    $benchmarkDao = new MarketBenchMark();

    if($request->isGet()) {
      $pageInfo = self::getRequestPageInfo($request);
      
      $filter_options['date'] = self::paramStartAndEndDate($request->getAttribute('params'));
      $filter_options = array_merge($filter_options, self::getFilterOptions($request));
      $filter_options['and'][] = DAO::setCondition('MarketID', $marketID);

      $res = $benchmarkDao->getData($filter_options, array(), $pageInfo);
      if (count($res) > 0) {
        //$response->getBody()->write(self::$dbTable);
        //var_dump(date("Y-m-d H:i:s"));

        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!!");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $datas = self::getRequestBodyJSON($request);

      $i = 0;

      $datas = $this->sortData($datas);

      foreach ($datas as $data) {
        $data = $benchmarkDao->benchmark($marketID, $data);
        if ($benchmarkDao->save($data) != false) {
          $i++;
        }
      }

      if ($i > 0) {
          $response = $response->withJson(array("result" => "$i records were updated."));
      }
      else {
        $response = self::handleError($response, 401, "Failed!!");
      }
    }
    else if($request->isDelete()) {
      $datas = self::getRequestBodyJSON($request);

      $i = 0;
      foreach ($datas as $data) {
        if ($benchmarkDao->deleteByPks($data)) {
          $i++;
        }
      }

      if ($i > 0) {
          $response = $response->withJson(array("result" => "successfully deleted. [$i]"));
      }
      else {
        $response = self::handleError($response, 401, "Failed!!");
      }
    }

    return $response;
  }

  function getECOData($ecoInfo, $filter_options) {
    $ecoDao = new MarketECO();
    $newECOInfo = $ecoInfo;

    foreach ($ecoInfo as $key => $value) {
      $filter_options['and'][] = DAO::setCondition('ECO_NAME', $value['key']);

      $data = $ecoDao->getData($filter_options, array());

      $value['data'] = $data;
      $newECOInfo[$key] = $value;
    }

    return $newECOInfo;
  }

  function getECOInfo($marketInfo, $data_all=false) {
    $ECOData = array();

    for ($x = 1; $x <= 10; $x++) {
      $ecoName = "ECO_".$x;

      if ($marketInfo[$ecoName."_SETTING"] == 0 && !$data_all) {
        continue;
      }

      $ECOData[$ecoName] = array(
        "key" => $marketInfo[$ecoName],
        "memo" => $marketInfo[$ecoName."_MEMO"],
        "setting" => $marketInfo[$ecoName."_SETTING"],
      );
    }

    return $ECOData;
  }

  public function getECOList($request, $response, $args) {
    /*
    $json = json_decode(file_get_contents(COMMON_ROOT . "/". ECO_LIST_JSON), true);
    */

    $_db = new ECOList();
    $json = $_db->get();

    if ($json) {
      //$response->getBody()->write(self::$dbTable);
      //var_dump(date("Y-m-d H:i:s"));

      $response = $response->withJson($json);
    }
    else {
      $response = self::handleError($response, 404, "Not Found!!");
    }

    return $response;
  }

  public function doECOShow($request, $response, $args) {
    $this->setLevel($args);

    // the feature just enable marketLevelC
    if ($this->_controller->marketLevel != 'MarketLevelC') {
      $response = self::handleError($response, 400, "Not [$this->_controller->marketList[3]]");
      return $response;
    }

    $marketID = $this->setID($args);

    if($request->isGet()) {
      $ecoInfo = $this->getECOInfo($this->_controller->marketInfo, $this->checkGetAllData($request));

      //$pageInfo = self::getRequestPageInfo($request);
      
      $filter_options['date'] = self::paramStartAndEndDate($request->getAttribute('params'));
      $filter_options = array_merge($filter_options, self::getFilterOptions($request));

      //$res = $benchmarkDao->getData($filter_options, array(), $pageInfo);
      //$res = $ecoInfo;
      $res = $this->getECOData($ecoInfo, $filter_options);
      if (count($res) > 0) {
        //$response->getBody()->write(self::$dbTable);
        //var_dump(date("Y-m-d H:i:s"));

        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!!");
      }
    }

    return $response;
  }

  public function doECOAction($request, $response, $args) {
    $this->setLevel($args);

    // the feature just enable marketLevelC
    if ($this->_controller->marketLevel != 'MarketLevelC') {
      $response = self::handleError($response, 400, "Not [$this->_controller->marketList[3]]");
      return $response;
    }

    $ecoName = empty($args['econame']) ? "" : $args['econame'];
    $ecoDao = new MarketECO();

    if($request->isGet()) {
      $pageInfo = self::getRequestPageInfo($request);
      
      $filter_options['date'] = self::paramStartAndEndDate($request->getAttribute('params'));
      $filter_options = array_merge($filter_options, self::getFilterOptions($request));
      $filter_options['and'][] = DAO::setCondition('ECO_NAME', $ecoName);

      $res = $ecoDao->getData($filter_options, array(), $pageInfo);
      if (count($res) > 0) {
        //$response->getBody()->write(self::$dbTable);
        //var_dump(date("Y-m-d H:i:s"));

        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!!");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $datas = self::getRequestBodyJSON($request);

      $i = 0;

      $datas = $this->sortData($datas);

      foreach ($datas as $data) {
        if ($ecoDao->save($data) != false) {
          $i++;
        }
      }

      if ($i > 0) {
          $response = $response->withJson(array("result" => "$i records were updated."));
      }
      else {
        $response = self::handleError($response, 401, "Failed!!");
      }
    }
    else if($request->isDelete()) {
      $datas = self::getRequestBodyJSON($request);

      $i = 0;
      foreach ($datas as $data) {
        if ($ecoDao->deleteByPks($data)) {
          $i++;
        }
      }

      if ($i > 0) {
          $response = $response->withJson(array("result" => "successfully deleted. [$i]"));
      }
      else {
        $response = self::handleError($response, 401, "Failed!!");
      }
    }

    return $response;
  }

  public function getAlertList($request, $response, $args) {
    $this->setLevel($args);

    // the feature just enable marketLevelC
    if ($this->_controller->marketLevel != 'MarketLevelC') {
      $response = self::handleError($response, 400, "Not [$this->_controller->marketList[3]]");
      return $response;
    }

    $alertDao = new MarketAlert();
    $_YMD = !empty($request->getParam('date')) ? $request->getParam('date') : date("Y-m-d");
    // $_YMD = '2017-02-27';

    $pageInfo = self::getRequestPageInfo($request);
    $orderInfo = DAO::setOrderBy('MarketID');
    
    $filter_options = array_merge(array(), self::getFilterOptions($request));
    $filter_options['and'][] = DAO::setCondition('YMD', $_YMD);

    $res = $alertDao->getData($filter_options, $orderInfo, $pageInfo);
    if (count($res) > 0) {
      $marketData = array_column($this->_controller->marketDAO->get(array(), array()), 'NAME', 'ID');

      for ($x = 0; $x < count($res['data']); $x++) {
        $_tmp = $res['data'][$x];
        $_tmp['NAME'] = $marketData[$_tmp['MarketID']];
        $_tmp['Alert'] = ($_tmp['EMA'] > 0 || $_tmp['RSI'] > 0 || $_tmp['BBands'] > 0) ? 1 : 0;
        $res['data'][$x] = $_tmp;
      }

      $response = $response->withJson($res);
    }
    else {
      $response = self::handleError($response, 404, "Not Found!!");
    }
    
    return $response;
  }

  public function doAlertAction($request, $response, $args) {
    $this->setLevel($args);
    $marketID = $this->setID($args);

    // the feature just enable marketLevelC
    if ($this->_controller->marketLevel != 'MarketLevelC') {
      $response = self::handleError($response, 400, "Not [$this->_controller->marketList[3]]");
      return $response;
    }

    $alertDao = new MarketAlert();

    if($request->isGet()) {
      $pageInfo = self::getRequestPageInfo($request);
      
      $filter_options['date'] = self::paramStartAndEndDate($request->getAttribute('params'));
      $filter_options = array_merge($filter_options, self::getFilterOptions($request));
      $filter_options['and'][] = DAO::setCondition('MarketID', $marketID);

      $res = $alertDao->getData($filter_options, array(), $pageInfo);
      if (count($res) > 0) {
        //$response->getBody()->write(self::$dbTable);
        //var_dump(date("Y-m-d H:i:s"));

        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!!");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $data = self::getRequestBodyJSON($request);

      $alertDao = new MarketAlert();

      $_conditions = array();
      $_conditions[] = DAO::setCondition('MarketID', $data['MarketID']);
      $_conditions[] = DAO::setCondition('YMD', $data['YMD']);

      $_data = $alertDao->getOne($_conditions);

      if (!empty($_data)) {
        if ($_data['Notifity'] ==0) {
          $_newData = array(
            'MarketID' => $marketID,
            'YMD' => $data['YMD'],
            'Notifity' => 1,
          );

          if ($alertDao->save($_newData) != false) {
            $response = $response->withJson(array("result" => "Alert were updated."));
          }
          else {
            $response = self::handleError($response, 401, "Failed!!");
          }
        }
        else {
          $response = self::handleError($response, 403, "Failed!!");
        }
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }

    return $response;
  }

  public function doBenchMarkReBuild($request, $response, $args) {
    $this->setLevel($args);
    // the feature just enable marketLevelC
    if ($this->_controller->marketLevel != 'MarketLevelC') {
      $response = self::handleError($response, 400, "Not [$this->_controller->marketList[3]]");
      return $response;
    }

    $marketID = $this->setID($args);

    $benchmarkDao = new MarketBenchMark();

    if($request->isGet()) {
      $date = array();

      if (!empty($request->getParam('end_date')) && Utilities\Utilities::validateDate($request->getParam('end_date'))) {
        $date['endDate'] = $request->getParam('end_date');
      }

      $res = $benchmarkDao->rebuildBenchmark($marketID, $date);
      if ($res > 0) {
        $response = $response->withJson(array("result" => "successfully rebuild. [$res]"));
      }
    }

    return $response;
  }

  public function getMarketCurrList($request, $response, $args) {
    $this->setLevel($args);
    // the feature just enable marketLevelB
    if ($this->_controller->marketLevel != 'MarketLevelB') {
      $response = self::handleError($response, 400, "Not [$this->_controller->marketList[2]]");
      return $response;
    }

    if($request->isGet()) {
      $_CT0CURDao = new CT0CUR();

      $data = ['ASH', 'AUD', 'CAD', 'CHF', 'DKK', 'DM', 'ESP', 'EUR', 'FFR', 'HFL', 'HKD', 'IEP' ,'ITL', 'JPY', 'NTD', 'NZD', 'RMB', 'RND', 'SGD', 'SKR', 'STL', 'USD', 'XEU'];

      $currData = $_CT0CURDao->getCurrsInfo($data);
      if (count($currData) > 0) {
        $response = $response->withJson(array("data" => $currData, "count" => count($currData)));
      }
    }

    return $response;
  }

}
?>