<?php
include_once(dirname(__FILE__) . "/AppBaseController.php");
include_once(dirname(__FILE__) . "/../classes/UserInfo.php");

include_once(dirname(__FILE__) . "/../classes/DAO/UserDeviceInfo.php");

class UserController extends AppBaseController {

  public function __construct() {
    $this->_controller = new UserInfo();

    $this->_filterArr = array("PASSWORD");
  }

  protected function setID($args) {
    if (empty($args['userID'])) {
      return null;
    }

    $ID =  $args['userID'];
    $this->_controller->setUserID($ID);

    return $ID;
  }

  public function doShow($request, $response, $args) {
    $pageInfo = self::getRequestPageInfo($request);

    $res = $this->_controller->userDAO->getData(array(), array(), $pageInfo);

    if (count($res) > 0) {
      $response = self::toOutput($response, $res, true);
    }
    else {
      $response = self::handleError($response, 404);
    }

    return $response;
  }

  public function doAction($request, $response, $args) {
    $userID = $this->setID($args);

    if($request->isGet()) {
      if (!empty($this->_controller->userInfo) > 0) {
        //$response->getBody()->write(self::$dbTable);
        $response = self::toOutput($response, $this->_controller->userInfo);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!! [$userID]");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $data = self::getRequestBodyJSON($request);

      if(empty($data)) {
        $response = self::handleError($response, 400, "Failed!!");
        return $response;
      }

      if ($this->_controller->save($data)) {
        //$response->getBody()->write(json_encode($data));
        $response = self::toOutput($response, $this->_controller->userInfo);
      }
      else {
        $response = self::handleError($response, 400, "Failed!! [$userID]");
      }
    }
    else if($request->isDelete()) {
      //$data = self::getRequestBodyJSON($request);

      if ($this->_controller->del()) {
        $response = self::toOutput($response, $this->_controller->userInfo);
      }
      else {
        $response = self::handleError($response, 400, "Failed!! [$userID]");
      }
    }

    return $response;
  }

  public function doSignUp($request, $response, $args) {
    $data = self::getRequestBodyJSON($request);

    if ($data == false) {
      $response = self::handleError($response, 400);
      return $response;
    }

    $res = $this->_controller->userSignUp($data);

    if (!empty($res)) {
      $response = self::toOutput($response, $res);
    }
    else {
      $response = self::handleError($response, 404);
    }

    return $response;
  }

  public function doLogin($request, $response, $args) {
    $allPostPutVars = self::getPostParsedBody($request);

    if (!self::checkArrayData($allPostPutVars, array('email', 'password'))) {
      $response = self::handleError($response, 400, "miss parameter");
      return $response;
    }

    //echo var_dump($allPostPutVars);
    $email = $allPostPutVars['email'];
    $passwd = $allPostPutVars['password'];
    $isGetToken = empty($allPostPutVars['getToken']) ? false : (int)$allPostPutVars['getToken'];

    $res = $this->_controller->Login($email, $passwd, $isGetToken);

    if (!empty($res)) {
      if (self::checkArrayData($allPostPutVars, array('udid'))) {
        $device = UserDeviceInfo::Unknown;
        if (!empty($allPostPutVars['device'])) {
          if ($allPostPutVars['device'] == "ios") {
            $device = UserDeviceInfo::iOS;
          }
          else {
            $device = UserDeviceInfo::Android;
          }
        }
        $udid = $allPostPutVars['udid'];
        $_data = array(
          'DEVICE_ID' => $udid,
          'DEIVCE_TYPE' => $device,
          'lastlogindate' => date("Y-m-d H:i:s"),
        );

        $this->_controller->saveDeviceInfo($_data);
      }

      $response = self::toOutput($response, $res);
    }
    else {
      $response = self::handleError($response, 401);
    }

    return $response;
  }

  public function doRefreshToken($request, $response, $args) {
    $allPostPutVars = self::getPostParsedBody($request);
        
    if (!self::checkArrayData($allPostPutVars, array('email', 'password', 'access_token'))) {
      $response = self::handleError($response, 400);
      return $response;
    }

    $email = $allPostPutVars['email'];
    $passwd = $allPostPutVars['password'];
    $token = $allPostPutVars['access_token'];

    $this->_controller->setAccessToken($token);

    if (!empty($this->_controller->userInfo)) {
      if($this->_controller->validateUser($email, $passwd)) {
        $res = $this->_controller->refreshAccessToken();
        if ($res != false) {
          $response = $response->withJson($res);

          return $response;
        }
      }
    }
    
    $response = self::handleError($response, 400);

    return $response;
  }

  public function doResetPassword($request, $response, $args) {
    $allPostPutVars = self::getPostParsedBody($request);
        
    if (!self::checkArrayData($allPostPutVars, array('email', 'old_password', 'new_password'))) {
      $response = self::handleError($response, 400);
      return $response;
    }

    $email = $allPostPutVars['email'];
    $passwd = $allPostPutVars['old_password'];
    $new_passwd = $allPostPutVars['new_password'];

    $this->_controller->setEmail($email);

    if($this->_controller->validateUser($email, $passwd)) {
      $res = $this->_controller->setPassword($new_passwd);
      if ($res != false) {
        $response = $response->withJson(array('msg' => 'Susses, password changed.'));

        return $response;
      }
    }    
    $response = self::handleError($response, 404);

    return $response;
  }

  public function doForgetPassword($request, $response, $args) {
    $allPostPutVars = self::getPostParsedBody($request);

    if (!self::checkArrayData($allPostPutVars, array('email'))) {
      $response = self::handleError($response, 400);
      return $response;
    }

    $email = $allPostPutVars['email'];

    if($this->_controller->forgetPassword($email)) {
      $response = $response->withJson(array('msg' => 'Susses, password forget.'));

      return $response;
    }

    $response = self::handleError($response, 404);

    return $response;
  }

  public function doWatchListAction($request, $response, $args) {
    $userID = $this->setID($args);

    if($request->isGet()) {
      $pageInfo = self::getRequestPageInfo($request);
      $filter_options['date'] = self::paramStartAndEndDate($request->getAttribute('params'));
      $filter_options = array_merge($filter_options, self::getFilterOptions($request));
      $filter_options['and'][] = DAO::setCondition('USER_ID', $userID);

      $res = $this->_controller->userWatchFundsDAO->getData($filter_options, array(), $pageInfo);

      if (count($res) > 0) {
        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!! [$userID]");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $data = self::getRequestBodyJSON($request);

      if ($this->_controller->saveWatchFund($data)) {
        $res = $this->_controller->getUserWatchFund($data['FUN_CODE']);
        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 400, "Failed!! [$userID]");
      }
    }
    else if($request->isDelete()) {
      $datas = self::getRequestBodyJSON($request);

      $count = 0;
      foreach ($datas as $data) {
        if ($this->_controller->removeWatchFund($data)) {
          $count++;
        }
      }

      if($count > 0) {
        $response = $response->withJson(array('msg' => "Susses, delete $count items."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!! [$userID]");
      }
    }

    return $response;
  }

  public function doBuyFundsAction($request, $response, $args) {
    $userID = $this->setID($args);

    if($request->isGet()) {
      $pageInfo = self::getRequestPageInfo($request);
      $filter_options['date'] = self::paramStartAndEndDate($request->getAttribute('params'));
      $filter_options = array_merge($filter_options, self::getFilterOptions($request));
      $filter_options['and'][] = DAO::setCondition('USER_ID', $userID);

      $res = $this->_controller->userFundsRecordDAO->getData($filter_options, array(), $pageInfo);

      if (count($res) > 0) {
        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!! [$userID]");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $data = self::getRequestBodyJSON($request);

      $res = $this->_controller->saveFundsRecord($data);
      if ($res != false) {
        $res = $this->_controller->getUserFundsRecord(is_string($res) ? $res : $data['ID']);
        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 400, "Failed!! [$userID]");
      }
    }
    else if($request->isDelete()) {
      $datas = self::getRequestBodyJSON($request);

      $count = 0;
      foreach ($datas as $data) {
        if ($this->_controller->removeFundsRecord($data)) {
          $count++;
        }
      }

      if($count > 0) {
        $response = $response->withJson(array('msg' => "Susses, delete $count items."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!! [$userID]");
      }
    }

    return $response;
  }

}
?>