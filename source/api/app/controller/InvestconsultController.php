<?php
include_once(dirname(__FILE__) . "/AppBaseController.php");
include_once(dirname(__FILE__) . "/../classes/DAO/Investconsult.php");

class InvestconsultController extends AppBaseController {

  protected $_dao;

  public function __construct() {
    $this->_dao = new Investconsult();
  }

  public function doGet($request, $response, $args) {
    $res = $this->_dao->getData();
    $json = array(
        'ID' => $res['ID'],
        'YQ' => $res['YQ'],
        'INVESTCONSULT' => json_decode($res['INVESTCONSULT']),
        'DATA' => json_decode($res['DATA']),
        'lastupdatetime' => $res['lastupdatetime'],
      );
    $response = $response->withJson($json);

    return $response;
  }

  public function doPost($request, $response, $args) {
    $data = self::getRequestBodyJSON($request);
    
    $newData = array(
        'YQ' => $data['YQ'],
        'INVESTCONSULT' => json_encode($data['INVESTCONSULT'], JSON_UNESCAPED_UNICODE),
        'DATA' => json_encode($data['DATA'], JSON_UNESCAPED_UNICODE),
      );
    
    $res = $this->_dao->save($newData);
    if ($res != false) {
      $response = $response->withJson($data);
    }
    else {
      $response = self::handleError($response, 400, "Failed!!");
    }

    return $response;
  }

  public function doAction($request, $response, $args) {
    if($request->isGet()) {
      $response = $this->doGet($request, $response, $args);
    }
    else if($request->isPost() || $request->isPut()) {
      $response = $this->doPost($request, $response, $args);
    }

    return $response;
  }
}
?>