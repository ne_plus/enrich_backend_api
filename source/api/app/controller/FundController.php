<?php
include_once(dirname(__FILE__) . "/AppBaseController.php");
include_once(dirname(__FILE__) . "/../classes/FundInfo.php");
include_once(dirname(__FILE__) . "/../classes/MarketInfo.php");

include_once(dirname(__FILE__) . "/../../common/config.php");

class FundController extends AppBaseController {

  public function __construct() {
       $this->_controller = new FundInfo();
   }

  protected function setID($args) {
    if (empty($args['fundID'])) {
      return null;
    }

    $ID =  $args['fundID'];
    $this->_controller->setFundID($ID);

    return $ID;
  }

  public function doShow($request, $response, $args) {
    $pageInfo = self::getRequestPageInfo($request);
    $filter_options = self::getFilterOptions($request);

    if(!$this->checkGetAllData($request)) {
      $filter_options['and'][] = DAO::setCondition('eliminate', "1");
    }

    $res = $this->_controller->fundDAO->getData($filter_options, array(), $pageInfo);
    //print_r($this->_controller->fundDAO->debugMsg());
    if (count($res) > 0) {
      //$response->getBody()->write(self::$dbTable);
      $response = $response->withJson($res);
    }
    else {
      $response = self::handleError($response, 404, "Not Found!!");
    }

    return $response;
  }

  public function doAction($request, $response, $args) {
    $fundID = $this->setID($args);

    if($request->isGet()) {
      if (!empty($this->_controller->fundInfo) > 0) {
        $response = $response->withJson($this->_controller->fundInfo);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!! [$fundID]");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $data = self::getRequestBodyJSON($request);

      if(empty($data)) {
        $response = self::handleError($response, 400, "Failed!!");
        return $response;
      }

      if(!empty($fundID)) {
        if ($this->_controller->saveFund($data)) {
          $response = $response->withJson($this->_controller->fundInfo);
        }
        else {
          $response = self::handleError($response, 404, "Failed!! [$fundID]");
        }
      }
      else {
        $count = 0;
        foreach ($data as $value) {
          if ($this->_controller->saveFund($value)) {
            $count++;
          }
        }

        if($count > 0) {
          $response = $response->withJson(array("msg" => "Susses, save $count items."));
        }
        else {
          $response = self::handleError($response, 404, "Failed!!");
        }
      }
    }
    else if($request->isDelete()) {
      $data = self::getRequestBodyJSON($request);

      if(!empty($fundID)) {
        if ($this->_controller->delFund()) {
          $response = $response->withJson($this->_controller->fundInfo);
        }
        else {
          $response = self::handleError($response, 404, "Failed!! [$fundID]");
        }
      }
      else {
        if(empty($data)) {
          $response = self::handleError($response, 400, "Failed!!");
          return $response;
        }

        $count = 0;
        foreach ($data as $value) {
          if ($this->_controller->fundDAO->deleteByID($value['FUN_CODE'])) {
            $count++;
          }
        }

        if($count > 0) {
          $response = $response->withJson(array("msg" => "Susses, delete $count items."));
        }
        else {
          $response = self::handleError($response, 404, "Failed!!");
        }
      }
    }

    return $response;
  }

  public function doNavAction($request, $response, $args) {
    $fundID = $this->setID($args);

    if($request->isGet()) {
      $pageInfo = self::getRequestPageInfo($request);
      $filter_options['date'] = self::paramStartAndEndDate($request->getAttribute('params'));
      $filter_options = array_merge($filter_options, self::getFilterOptions($request));
      $filter_options['and'][] = DAO::setCondition('FUN_CODE', $fundID);

      $res = $this->_controller->fundNavDAO->getData($filter_options, array(), $pageInfo);

      if (count($res) > 0) {
          //$response->getBody()->write(self::$dbTable);
        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!! [$fundID]");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $datas = self::getRequestBodyJSON($request);

      if(empty($datas)) {
        $response = self::handleError($response, 400, "Failed!!");
        return $response;
      }
      
      $count = 0;
      foreach ($datas as $data) {
        if ($this->_controller->saveFundNav($data)) {
          $count++;
        }
      }

      if($count > 0) {
        $response = $response->withJson(array('msg' => "Susses, save $count items."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }
    else if($request->isDelete()) {
      $datas = self::getRequestBodyJSON($request);

      if(empty($datas)) {
        $response = self::handleError($response, 400, "Failed!!");
        return $response;
      }

      $count = 0;
      foreach ($datas as $data) {
        if ($this->_controller->fundNavDAO->deleteByPks($data)) {
          $count++;
        }
      }

      if($count > 0) {
        $response = $response->withJson(array('msg' => "Susses, delete $count items."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }

    return $response;
  }

  public function doRetAction($request, $response, $args) {
    $fundID = $this->setID($args);

    if($request->isGet()) {
      $pageInfo = self::getRequestPageInfo($request);
      $filter_options['date'] = self::paramStartAndEndDate($request->getAttribute('params'));
      $filter_options = array_merge($filter_options, self::getFilterOptions($request));
      $filter_options['and'][] = DAO::setCondition('FUN_CODE', $fundID);

      $res = $this->_controller->fundRetDAO->getData($filter_options, array(), $pageInfo);

      if (count($res) > 0) {
        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!! [$fundID]");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $datas = self::getRequestBodyJSON($request);

      if(empty($datas)) {
        $response = self::handleError($response, 400, "Failed!!");
        return $response;
      }

      $count = 0;
      foreach ($datas as $data) {
        if ($this->_controller->saveFundRet($data)) {
          $count++;
        }
      }

      if($count > 0) {
        $response = $response->withJson(array('msg' => "Susses, save $count items."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }
    else if($request->isDelete()) {
      $datas = self::getRequestBodyJSON($request);

      $count = 0;
      foreach ($datas as $data) {
        if ($this->_controller->fundRetDAO->deleteByPks($data)) {
          $count++;
        }
      }

      if($count > 0) {
        $response = $response->withJson(array('msg' => "Susses, delete $count items."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }

    return $response;
  }

  public function doDtrAction($request, $response, $args) {
    $fundID = $this->setID($args);

    if($request->isGet()) {
      $pageInfo = self::getRequestPageInfo($request);
      $filter_options['date'] = self::paramStartAndEndDate($request->getAttribute('params'));
      $filter_options = array_merge($filter_options, self::getFilterOptions($request));
      $filter_options['and'][] = DAO::setCondition('FUN_CODE', $fundID);

      $res = $this->_controller->fundDtrDAO->getData($filter_options, array(), $pageInfo);

      if (count($res) > 0) {
        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!! [$fundID]");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $datas = self::getRequestBodyJSON($request);

      if(empty($datas)) {
        $response = self::handleError($response, 400, "Failed!!");
        return $response;
      }

      $count = 0;
      foreach ($datas as $data) {
        if ($this->_controller->saveFundDtr($data)) {
          $count++;
        }
      }

      if($count > 0) {
        $response = $response->withJson(array('msg' => "Susses, save $count items."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }
    else if($request->isDelete()) {
      $datas = self::getRequestBodyJSON($request);

      $count = 0;
      foreach ($datas as $data) {
        if ($this->_controller->fundDtrDAO->deleteByPks($data)) {
          $count++;
        }
      }

      if($count > 0) {
        $response = $response->withJson(array('msg' => "Susses, delete $count items."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }

    return $response;
  }

  public function exportToJson($records) {
    file_put_contents(COMMON_ROOT . "/". FUNDS_COMBINATION_JSON, json_encode($records));
  }

  public function sortCombination($data, $_type) {
    usort($data, function($a, $b) use ($_type) {
      $ret = intval($b['info']['PROPORTION_'.$_type]) - intval($a['info']['PROPORTION_'.$_type]);
      if($ret == 0) {
        return $a['info']['ID'] - $b['info']['ID'];
      }
      return $ret;
    });

    return $data;
  }

  public function getCombination($arr = array('AST_AMT', 'BETA', 'RULE_4433', 'SHARPE', 'DTR')) {
    $marketInfo = new MarketInfo('MarketLevelB');
    
    $res = $marketInfo->marketDAO->get(array());

    $data = [];

    $date = new DateTime();

    if (0) {
      $date = new DateTime('2016-10-25');
    }

    foreach ($res as $value) {
      $market = new MarketInfo('MarketLevelB', $value['ID']);

      $subMarketIDs = array();

      if (!empty($market->marketInfo['CombinationMarketLevelCID'])) {
        $subMarketIDs[] = $market->marketInfo['CombinationMarketLevelCID'];
      }
      else {
        $subMarkets = $market->getSubMarketList();
        $subMarketIDs = array_column($subMarkets, 'ID');
      }

      if(in_array('AST_AMT', $arr)) {
        $data['AST_AMT'][$value['ID']] = array(
          'info' => $value,
          'combination' => $this->_controller->AST_AMT($subMarketIDs, $value['ABREV']),
          );
      }
      if(in_array('BETA', $arr)) {
        $date->sub(new DateInterval('P1M'));

        $data['BETA'][$value['ID']] = array(
          'info' => $value,
          'combination' => $this->_controller->BETA($subMarketIDs, $date->format('Ym')),
          );
      }
      if(in_array('RULE_4433', $arr)) {
        $date->sub(new DateInterval('P1D'));

        $data['RULE_4433'][$value['ID']] = array(
          'info' => $value,
          'combination' => $this->_controller->RULE_4433($subMarketIDs, $date->format('Y-m-d')),
          );
      }
      if(in_array('SHARPE', $arr)) {
        $date->sub(new DateInterval('P1M'));

        $data['SHARPE'][$value['ID']] = array(
          'info' => $value,
          'combination' => $this->_controller->SHARPE($subMarketIDs, $date->format('Ym')),
          );
      }
      if(in_array('DTR', $arr)) {
        $date->sub(new DateInterval('P1D'));

        $data['DTR'][$value['ID']] = array(
          'info' => $value,
          'combination' => $this->_controller->DTR($subMarketIDs, $date->format('Y')),
          );
      }
    }

    foreach ($data as $key => $value) {
      $new_value = $this->sortCombination($value, $key);
      $data[$key] = $new_value;
    }

    if(empty($data)) {
      return array();
    }
    
    return $data;
  }

  public function doCombinationShow($request, $response, $args) {
    $data = array();

    if (empty($args['type'])) {
      if (file_exists(COMMON_ROOT . "/". FUNDS_COMBINATION_JSON)) {
        $data = json_decode(file_get_contents(COMMON_ROOT . "/". FUNDS_COMBINATION_JSON), true);
      }
      else {
        $data = $this->getCombination();
      }
    }
    else {
      $data = $this->getCombination(array($args['type']));
    }

    if(!empty($data)) {
      $response = $response->withJson($data);
    }
    else {
      $response = self::handleError($response, 404, "Failed!!");
    }
    
    return $response;
  }
}
?>