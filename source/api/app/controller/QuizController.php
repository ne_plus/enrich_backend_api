<?php
include_once(dirname(__FILE__) . "/AppBaseController.php");
include_once(dirname(__FILE__) . "/../classes/QuizInfo.php");

class QuizController extends AppBaseController {

  public function __construct() {
    $this->_controller = new QuizInfo();
  }

  protected function setID($args) {
    if (empty($args['quizID'])) {
      return null;
    }

    $ID =  $args['quizID'];
    $this->_controller->setQuizID($ID);

    return $ID;
  }

  public function doShow($request, $response, $args) {
    $pageInfo = self::getRequestPageInfo($request);

    $filter_options['date'] = self::paramStartAndEndDate($request->getAttribute('params'));
    $filter_options = array_merge($filter_options, self::getFilterOptions($request));

    $isAll = false;
    $conditions = array();

    if (!$isAll) {
      $conditions[] = DAO::setCondition('is_active', 1);
    }

    $res = $this->_controller->quizDAO->getData($conditions, array(), $pageInfo);

    if (count($res) > 0) {
      $response = self::toOutput($response, $res, true);
    }
    else {
      $response = self::handleError($response, 404);
    }

    return $response;
  }

  public function doAction($request, $response, $args) {
    $_id = $this->setID($args);

    if($request->isGet()) {
      //$filter_options['and'][] = array('FUN_CODE' => $fundID);

      $res = $this->_controller->getQuizInfo();

      if (count($res) > 0) {
          //$response->getBody()->write(self::$dbTable);
        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!! [$_id]");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $data = self::getRequestBodyJSON($request);

      if(empty($data)) {
        $response = self::handleError($response, 400, "Failed!!");
        return $response;
      }

      if($this->_controller->save($data)) {
        $response = $response->withJson(array("msg" => "Susses, save this quiz."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }
    else if($request->isDelete()) {
      if($this->_controller->del()) {
        $response = $response->withJson(array("msg" => "Susses, delete this quiz."));
      }
      else {
        $response = self::handleError($response, 404, "Failed!!");
      }
    }

    return $response;
  }

  protected function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
      $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
      $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
      $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
      $ipaddress = getenv('REMOTE_ADDR');
    else
      $ipaddress = 'UNKNOWN';

    return $ipaddress;
  }

  public function doVoteAction($request, $response, $args) {
    $id = $this->setID($args);

    if($request->isGet()) {
      //$filter_options['and'][] = array('FUN_CODE' => $fundID);

      $res = $this->_controller->getQuizResult();

      if (count($res) > 0) {
          //$response->getBody()->write(self::$dbTable);
        $response = $response->withJson($res);
      }
      else {
        $response = self::handleError($response, 404, "Not Found!! [$id]");
      }
    }
    else if($request->isPost() || $request->isPut()) {
      $ret = 0;
      $res = $this->_controller->getQuizResult();

      if (count($res) <= 0) {
        $ret = -1;
      }
      elseif (!empty($args['choicesID'])) {
        $ret = $this->_controller->saveQuizAns($args['choicesID'], $this->get_client_ip());
        //$ret = $this->_controller->saveQuizAns($args['choicesID'], "");
        $ret = $ret ==true ? 1 : 0;
      }

      if($ret == 1) {
        $res = $this->_controller->getQuizResult();
        $response = $response->withJson(
          array(
            "msg" => "Susses, save your vote. [".$this->get_client_ip()."]",
            "result" => $res,
        ));
        //$response = $response->withJson(array("msg" => "Susses, save your vote. []"));
      }
      else {
        $response = self::handleError($response, 404, $ret == -1 ? "not found the choices." : "Failed!!");
      }
    }

    return $response;
  }
}
?>