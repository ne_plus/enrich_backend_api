<?php
include_once(dirname(__FILE__) . "/DAO/FundQuiz.php");
include_once(dirname(__FILE__) . "/DAO/FundQuizAns.php");
include_once(dirname(__FILE__) . "/DAO/FundQuizChoices.php");

class QuizInfo {

    public $quizInfo;
    public $quizItemInfo;
    public $quizResult;
    public $quizAns;

    public $quizID;

    public $quizDAO;
    public $quizAnsDAO;
    public $quizChoicesDAO;

    public $isShowAll;

    public function __construct($isAll = false, $quizID = '') {
        $this->quizDAO = new FundQuiz();
        $this->quizAnsDAO = new FundQuizAns();
        $this->quizChoicesDAO = new FundQuizChoices();

        $this->setQuizID($quizID);

        $this->isShowAll = $isAll;
    }

    public function setQuizID($quizID) {
        if (!empty($quizID)) {
            $this->quizID = $quizID;

            $this->updateInfo();
        }
    }

    public function setAll($quizID) {
        $this->isShowAll = $isAll;

        $this->updateInfo();
    }

    public function updateInfo() {
        if ($this->quizDAO) {
            $_info = $this->quizDAO->getByID($this->quizID);
            if (count($_info)) {
                $this->quizInfo = $_info[0];
            }

            $this->getChoiceItems();
            $this->getQuizAns();
        }
    }

    public function getQuizInfo() {
        if (count($this->quizInfo) <= 0) {
            return array();
        }

        $_data = $this->quizInfo;
        //$_data['items'] = $this->quizItemInfo;
        $_data['items'] = $this->getQuizResult();

        return $_data;
    }

    public function getChoiceItems() {
        $this->quizItemInfo = $this->quizChoicesDAO->getByQuizID($this->quizID);

        return $this->quizItemInfo;
    }

    public function getQuizResult() {
        $_quizResult = array();
        $_res = $this->quizAnsDAO->getResult($this->quizID);
        $_data = array();

        foreach ($_res as $_ansData) {
            $_data[$_ansData['choice_id']] += $_ansData['value'];
        }

        foreach ($this->quizItemInfo as $quizItem) {
            $_result = empty($_data[$quizItem['id']]) ? 0 : $_data[$quizItem['id']];
            $_quizResult[] = array_merge($quizItem, array('result' => $_result));
        }

        $this->quizResult = $_quizResult;

        return $_quizResult;
    }

    public function getQuizAns() {
        $this->quizAns = $this->quizAnsDAO->getByQuizID($this->quizID);

        return $this->quizAns;
    }

    public function saveQuizAns($choiceID, $ip) {
        $data = array(
            'question_id' => $this->quizID,
            'choice_id' => $choiceID,
            'ip' => $ip,
        );

        $id = $this->quizAnsDAO->save($data);
        
        if($id != false) {
            return true;
        }

        return false;
    }

    public function save($data) {
        $_items = $data['items'];

        foreach ($_items as $_item) {
            $this->quizChoicesDAO->save($item);
        }

        return $this->quizDAO->save($data);
    }

    public function del() {
        if (empty($this->quizID)) {
            return false;
        }

        $this->quizAnsDAO->deleteByID($this->quizID);
        $this->quizChoicesDAO->deleteByID($this->quizID);
        $this->quizDAO->deleteByID($this->quizID);

        return true;
    }
}
