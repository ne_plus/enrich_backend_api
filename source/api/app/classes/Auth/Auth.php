<?php
namespace Slim\Auth;

include_once(dirname(__FILE__) . "/../UserInfo.php");

use RuntimeException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

use UserInfo;

class AuthScope
{
    CONST ADMIN = 2;
    CONST USER  = 1;
    CONST GUEST = 0;

    private $container;

    public $userInfo;
    public $authScope;

    public function __construct( $container, $scope = self::USER )
    {
        $this->container = $container;
        $this->authScope = $scope;
        $this->userInfo = array();
    }

    /**
     * Invoke middleware
     *
     * @param  RequestInterface $request PSR7 request object
     * @param  ResponseInterface $response PSR7 response object
     * @param  callable $next Next middleware callable
     *
     * @return ResponseInterface PSR7 response object
     */
    public function __invoke( RequestInterface $request, ResponseInterface $response, callable $next )
    {
        // Fetches the current user or returns a default
        $this->userInfo = $request->getAttribute('user');

        if (empty($this->userInfo)) {
            $response = $response->withStatus(401);
            $response = $response->withJson(array('error' => 'Access denied.'));
            return $response;
        }

        $usrRole = $this->getRole($this->userInfo);
        if ($usrRole < $this->authScope) {
            $response = $response->withStatus(403);
            $response = $response->withJson(array('error' => 'Forbidden.'));
            return $response;
        }

        //$response->getBody()->write($this->authScope);

        return $next( $request, $response );
    }

    /**
     * Gets role from user's identity.
     *
     * @param  mixed  $identity User's identity. If null, returns role 'guest'
     * @return string User's role
     */
    public function getRole($identity = null)
    {
        $role = null;
        if (is_array($identity) && isset($identity['ROLE'])) {
            $role = $identity['ROLE'];
        }

        if (!$role) {
            $role = self::USER;
        }

        return $role;
    }
}

class User
{
    private $container;

    public function __construct( $container )
    {
        $this->container = $container;
    }

    /**
     * Invoke middleware
     *
     * @param  RequestInterface $request PSR7 request object
     * @param  ResponseInterface $response PSR7 response object
     * @param  callable $next Next middleware callable
     *
     * @return ResponseInterface PSR7 response object
     */
    public function __invoke( RequestInterface $request, ResponseInterface $response, callable $next )
    {
        // Fetches the current user or returns a default
        $accessToken = $this->parseForAuthentication($request->getHeader('Authorization'));
        $accessToken = !empty($request->getParam('accessToken')) ? $request->getParam('accessToken') : $accessToken;
        $usrRole = AuthScope::GUEST;

        if (!empty($accessToken)) {
            $user = new UserInfo();
            $user->setAccessToken($accessToken);

            if(!empty($user->userInfo)) {
                $request  = $request->withAttribute('user', $user->userInfo);
                $usrRole = AuthScope::getRole($user->userInfo);
            }

            $response = $response->withHeader('Authorization', 'token '.$accessToken);
        }

        $request  = $request->withAttribute('user-role', $usrRole);

        return $next( $request, $response );
    }

    /**
     * Parse the Authorization header for auth tokens
     *
     * @param  array $authHeaders Array of PSR7 headers specific to authorization
     *
     * @return string|false Return either the auth token of false if none found
     *
     */
    private function parseForAuthentication(array $authHeaders)
    {
        $authValue  = false;
        if (count($authHeaders) > 0) {
            foreach ($authHeaders as $authHeader) {
                $authValues = explode(' ', $authHeader);
                if (2 === count($authValues) && array_search(strtolower($authValues[0]), ['token'])) {
                    $authValue = $authValues[1];
                    break;
                }
            }
        }
        return $authValue;
    }
}
