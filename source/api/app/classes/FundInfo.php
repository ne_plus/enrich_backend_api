<?php
include_once(dirname(__FILE__) . "/DAO/Fund.php");
include_once(dirname(__FILE__) . "/DAO/FundNav.php");
include_once(dirname(__FILE__) . "/DAO/FundRet.php");
include_once(dirname(__FILE__) . "/DAO/FundDtr.php");
include_once(dirname(__FILE__) . "/DAO/FundIDM.php");

class FundInfo {

    public $fundID;
    public $fundType;

    public $fundInfo;
    public $lastFundNavInfo;

    public $fundDAO;
    public $fundNavDAO;
    public $fundRetDAO;
    public $fundDtrDAO;
    public $fundIDMDAO;

    public static $IDX_CODE_LIST = ['ASH', 'AUD', 'CAD', 'CHF', 'DKK', 'DM', 'ESP', 'EUR', 'FFR', 'HFL', 'HKD', 'IEP' ,'ITL', 'JPY', 'NTD', 'NZD', 'RMB', 'RND', 'SGD', 'SKR', 'STL', 'USD', 'XEU'];

    public function __construct($fundID = '', $fundType = '') {
        $this->setFundType($fundType);
        $this->setFundID($fundID);
    }

    public function setFundType($fundType) {
        if (!empty($fundType)) {
            $this->fundType = $fundType;

            $this->fundDAO = new Fund(Fund::getDBTableByType($this->fundType));
            $this->fundNavDAO = new FundNav(FundNav::getDBTableByType($this->fundType));
            $this->fundRetDAO = new FundRet(FundRet::getDBTableByType($this->fundType));

            $this->fundDtrDAO = new FundDtr(FundDtr::getDBTableByType($this->fundType));
            $this->fundIDMDAO = new FundIDM(FundIDM::getDBTableByType($this->fundType));
        }
        else {
            $this->fundDAO = new Fund();
            $this->fundNavDAO = new FundNav();
            $this->fundRetDAO = new FundRet();
            $this->fundDtrDAO = new FundDtr();
            $this->fundIDMDAO = new FundIDM();
        }
    }

    public function setFundID($fundID) {
        if (!empty($fundID)) {
            $this->fundID = $fundID;
            $this->updateInfo();
        }
    }

    public function updateInfo() {
        if ($this->fundDAO) {
            $this->fundInfo = $this->fundDAO->getByID($this->fundID)[0];
        }
    }

    public function getFundListByMarketID($markets) {
        $conditions[] = DAO::setCondition('MARKET_ID', $markets, 'IN');
        $conditions[] = DAO::setCondition('eliminate', 1, '=');

        return $this->fundDAO->get($conditions);
    }

    public function saveFund($data) {
        $newData = array_merge($this->fundInfo, $data);
        $id = $this->fundDAO->save($newData);
        if($id != false) {
            if(is_string($id)) {
                $this->fundID = $id;
            }
            $this->updateInfo();
            return true;
        }

        return false;
    }

    public function delFund() {
        return $this->fundDAO->deleteByID($this->fundID);
    }

    public function saveFundNav($data) {
        //$newData = array_merge($this->fundInfo, $data);
        $id = $this->fundNavDAO->save($data);
        if($id != false) {
            if(is_string($id)) {
                $this->fundID = $id;
            }
            //$this->updateInfo();
            return true;
        }

        return false;
    }

    public function saveFundRet($data) {
        //$newData = array_merge($this->fundInfo, $data);
        $id = $this->fundRetDAO->save($data);
        if($id != false) {
            if(is_string($id)) {
                $this->fundID = $id;
            }
            //$this->updateInfo();
            return true;
        }

        return false;
    }

    public function saveFundDtr($data) {
        //$newData = array_merge($this->fundInfo, $data);
        $id = $this->fundDtrDAO->save($data);
        if($id != false) {
            if(is_string($id)) {
                $this->fundID = $id;
            }
            //$this->updateInfo();
            return true;
        }

        return false;
    }

    public function AST_AMT($markets, $ABREV, $n = 5) {
        if(count($markets) <=0) {
            return array();
        }

        $conditions[] = DAO::setCondition('MARKET_ID', $markets, 'IN');
        $conditions[] = DAO::setCondition('eliminate', 1, '=');
        $conditions[] = DAO::setCondition('ABREV', $ABREV);

        $orderBys[] =DAO::setOrderBy('AST_AMT', 'DESC');

        $res = $this->fundDAO->get($conditions, $orderBys, $n);
        return $res;
    }

    public function DTR($markets, $Y, $n = 5) {
        if(count($markets) <=0) {
            return array();
        }

        $funds = $this->getFundListByMarketID($markets);

        $conditions[] = DAO::setCondition('FUN_CODE', array_column($funds, 'FUN_CODE'), 'IN');
        $conditions[] = DAO::setCondition('DTR_B_YMD', $Y."%", "LIKE");

        $orderBys[] =DAO::setOrderBy('DTR_B_YMD', 'desc');
        $orderBys[] =DAO::setOrderBy('DTR_RAT_Y', 'desc');

        $res = $this->fundDtrDAO->getCombination($conditions, $orderBys, $n);
        return $this->_getSelfByID($res);
    }

    public function BETA($markets, $YM, $n = 5) {
        if(count($markets) <=0) {
            return array();
        }

        $funds = $this->getFundListByMarketID($markets);

        $conditions[] = DAO::setCondition('FUN_CODE', array_column($funds, 'FUN_CODE'), 'IN');

        $conditions[] = DAO::setCondition('IDX_CODE', 'BETA_M');
        $conditions[] = DAO::setCondition('YM', $YM);

        $orderBys[] =DAO::setOrderBy('DATA_VALUE', 'desc');

        $res = $this->fundIDMDAO->get($conditions, $orderBys, $n);
        return $this->_getSelfByID($res);
    }

    public function RULE_4433($markets, $YMD, $n = 5) {
        if(count($markets) <=0) {
            return array();
        }

        $funds = $this->getFundListByMarketID($markets);

        $conditions[] = DAO::setCondition('FUN_CODE', array_column($funds, 'FUN_CODE'), 'IN');

        $conditions[] = DAO::setCondition('YMD', $YMD);

        $orderBys[] =DAO::setOrderBy('RET_1Y', 'DESC');
        $orderBys[] =DAO::setOrderBy('RET_2Y', 'DESC');
        $orderBys[] =DAO::setOrderBy('RET_3Y', 'DESC');
        $orderBys[] =DAO::setOrderBy('RET_5Y', 'DESC');
        $orderBys[] =DAO::setOrderBy('RET_6M', 'DESC');
        $orderBys[] =DAO::setOrderBy('RET_3M', 'DESC');

        $res = $this->fundNavDAO->get($conditions, $orderBys, $n);
        return $this->_getSelfByID($res);
    }

    public function SHARPE($markets, $YM, $n = 5) {
        if(count($markets) <=0) {
            return array();
        }

        $funds = $this->getFundListByMarketID($markets);

        $conditions[] = DAO::setCondition('FUN_CODE', array_column($funds, 'FUN_CODE'), 'IN');

        $conditions[] = DAO::setCondition('IDX_CODE', 'SHARPE_M');
        $conditions[] = DAO::setCondition('YM', $YM);

        $orderBys[] =DAO::setOrderBy('DATA_VALUE', 'DESC');

        $res = $this->fundIDMDAO->get($conditions, $orderBys, $n);
        return $this->_getSelfByID($res);
    }

    protected function _getSelfByID($data) {
        if (empty(array_column($data, 'FUN_CODE'))) {
            return array();
        }

        $conditions[] = DAO::setCondition('FUN_CODE', array_column($data, 'FUN_CODE'), 'IN');
        $conditions[] = DAO::setCondition('eliminate', 1, '=');

        $orderBys[] =DAO::setOrderBy('NAV_ORG', 'DESC');

        $res = $this->fundDAO->get($conditions, $orderBys);
        return $res;
    }
}
