<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/DAO/User.php");
include_once(dirname(__FILE__) . "/DAO/UserToken.php");
include_once(dirname(__FILE__) . "/DAO/UserDeviceInfo.php");
include_once(dirname(__FILE__) . "/DAO/UserFundsRecord.php");
include_once(dirname(__FILE__) . "/DAO/UserWatchFunds.php");

class UserInfo {

    public $userID;
    public $userAccessToken;
    public $userInfo;

    public $userDAO;
    public $userTokenDAO;

    public $userDeviceInfoDAO;
    public $userFundsRecordDAO;
    public $userWatchFundsDAO;

    protected $fKey;

    public function __construct($userID = '') {
        $this->fKey = 'USER_ID';
        $this->userInfo = array();

        $this->setUserID($userID);

        $this->userTokenDAO = new UserToken();
    }

    public function setUserID($userID) {
        if (!$this->userDAO) {
            $this->userDAO = new User();
        }

        if (!empty($userID)) {
            if (!$this->userDeviceInfoDAO) {
                $this->userDeviceInfoDAO = new UserDeviceInfo();
            }

            if (!$this->userFundsRecordDAO) {
                $this->userFundsRecordDAO = new UserFundsRecord();
            }

            if (!$this->userWatchFundsDAO) {
                $this->userWatchFundsDAO = new UserWatchFunds();
            }

            $this->userID = $userID;
            $this->updateInfo();
        }
    }

    public function setAccessToken($token) {
        if (!empty($token)) {
            $userID = $this->userTokenDAO->getUserIDByToken($token);
            if ($userID != false) {
                $this->userAccessToken = $token;
                $this->setUserID($userID);
            }
        }
    }

    public function setEmail($eMail) {
        if (!empty($eMail)) {
            $res = $this->userDAO->getByEmail($eMail);
            if(!empty($res)) {
                $this->setUserID($res['USER_ID']);
            }
        }
    }

    public function passwordHash($password) {
        return hash('sha1', $password);
    }

    public function updateInfo() {
        $this->userInfo = $this->userDAO->getByID($this->userID);
    }

    public function setPassword($password) {
        $data = array(
            'PASSWORD' => $this->passwordHash($password),
            );

        return $this->userDAO->updateInfo($this->userID, $data);
    }

    public function validateUser($email, $password) {
        $passwordHash = $this->passwordHash($password);

        if($passwordHash = $this->userInfo['PASSWORD'] && $email == $this->userInfo['EMAIL']) {
            return true;
        }

        return false;
    }

    public function refreshAccessToken() {
        $token = $this->userTokenDAO->refreshToken($this->userID);
        if ($token != false) {
            $this->userAccessToken = $token;

            $accessTokenInfo = $this->userTokenDAO->getToken($this->userAccessToken);

            return $this->userTokenDAO->output($accessTokenInfo);
        }

        return false;
    }

    public function Login($email, $password, $isRefreshAccessToken = false) {
        $conditions[] = array(
            'field' => 'EMAIL', 
            'value' => $email,
            );

        $conditions[] = array(
            'field' => 'PASSWORD', 
            'value' => $this->passwordHash($password),
            );

        $res = $this->userDAO->getOne($conditions);
        if(!empty($res)) {
            $this->setUserID($res['USER_ID']);
            if ($isRefreshAccessToken) {
                // auto create new token for User
                $res = $this->refreshAccessToken();
                if($res != false) {
                    return array_merge($this->userInfo, $res);
                }
            }
            return $this->userInfo;
        }

        return false;
    }

    public function userSignUp($data) {
        if (empty($data['EMAIL'])) {
            return false;
        }

        $res = $this->userDAO->getByEmail($data['EMAIL']);

        if(count($res) > 0) {
            return false;
        }

        $this->userInfo = array();

        $res = $this->save($data);
        if ($res != false) {
            $res = $this->userDAO->getByEmail($data['EMAIL']);

            $this->setUserID($res['USER_ID']);

            if(!empty($data['PASSWORD'])) {
                $this->setPassword($data['PASSWORD']);
                $this->updateInfo();
            }

            return $this->userInfo;
        }

        return false;
    }

    public function save($data) {
        $newData = array_merge($this->userInfo, $data);
        unset($newData['PASSWORD']);
        $id = $this->userDAO->save($newData);
        if($id != false) {
            //if(is_string(strval($id))) {
                //$this->userID = strval($id);
                //$this->updateToken(null);
            //}
            $this->updateInfo();
            return true;
        }

        return false;
    }

    protected function randomPassword($_length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $_length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    protected function sendMail($mailToname, $mailTo, $mailfromname, $mailfrom, $mailSubject, $mailContent) {
        $mailTo="=?UTF-8?B?".base64_encode($mailToname)."?= <" . $mailTo . ">";
        $mailfrom="=?UTF-8?B?" . base64_encode($mailfromname) . "?= <" . $mailfrom . ">";
        $mailSubject = "=?UTF-8?B?".base64_encode($mailSubject)."?=";  

        return mail($mailTo,$mailSubject,$mailContent,"Mime-Version: 1.0\nFrom:" . $mailfrom . "\nContent-Type: text/html ; charset=UTF-8");
    }

    public function forgetPassword($email) {
        $res = $this->userDAO->getByEmail($email);

        if(count($res) <= 0) {
            return false;
        }

        $_randPassword = $this->randomPassword();

        //Email information
        $mailToname=empty($res['NAME']) ? "" : $res['NAME'];   //收件者
        $mailTo=$email;   //收件者
        $mailfromname="DFund";  //寄件者姓名
        $mailfrom="do-not-reply@dfund.com";  //寄件者電子郵件
        $mailSubject="[DFund]重新寄送密碼";    //主旨
        $mailContent = "＊ 此信件為系統發出信件，請勿直接回覆，感謝您的配合。謝謝！＊<br>
<br>
親愛的會員 您好：<br>
<br>
這封信是由DFund D基金 發出，用以處理您忘記密碼，當您收到本「重新寄送密碼信函」後，請直接密碼登入網站，無需回信。<br>
<br>
您的密碼為 <strong>".$_randPassword."</strong><br>
<br>
為了確保您的會員資料安全，請勿將密碼給予第三人使用。<br>
<br>
 D基金    敬上";  //內容
  
        //send email
        if ($this->sendMail($mailToname, $mailTo, $mailfromname, $mailfrom, $mailSubject, $mailContent)) {
            $this->setUserID($res['USER_ID']);
            $this->setPassword($_randPassword);
            return true;
        }
        else {
            print_r(error_get_last());
        }

        return false;
    }

    public function del() {
        return $this->userDAO->deleteByID($this->userID);
    }

    // buy list
    public function getUserFundsRecordList() {
        return $this->userFundsRecordDAO->getByID($this->userID);
    }

    public function getUserFundsRecord($ID) {
        return $this->userFundsRecordDAO->getByID($this->userID, $ID);
    }

    public function saveFundsRecord($data) {
        $data[$this->fKey] = $this->userID;
        return $this->userFundsRecordDAO->save($data);
    }

    public function removeFundsRecord($data) {
        return $this->userFundsRecordDAO->deleteByPks($data);
    }

    // watch list
    public function getUserWatchFundList() {
        return $this->userWatchFundsDAO->getByID($this->userID);
    }

    public function getUserWatchFund($fundCode) {
        return $this->userWatchFundsDAO->getByID($this->userID, $fundCode);
    }

    public function saveWatchFund($data) {
        $data[$this->fKey] = $this->userID;
        return $this->userWatchFundsDAO->save($data);
    }

    public function removeWatchFund($data) {
        return $this->userWatchFundsDAO->deleteByPks($data);
    }

    // devices
    public function getUserDeviceList() {
        return $this->userDeviceInfoDAO->getByID($this->userID);
    }

    public function getUserDeviceInfo($ID) {
        return $this->userDeviceInfoDAO->getByID($this->userID, $ID);
    }

    public function saveDeviceInfo($data) {
        $data[$this->fKey] = $this->userID;
        return $this->userDeviceInfoDAO->save($data);
    }

    public function removeDeviceInfo($data) {
        return $this->userDeviceInfoDAO->deleteByPks($data);
    }
}
