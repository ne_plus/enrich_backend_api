<?php
include_once(dirname(__FILE__) . "/DAO/Market.php");

class MarketInfo {

    public static $marketList = array(
        1 => 'MarketLevelA',
        2 => 'MarketLevelB',
        3 => 'MarketLevelC',
        4 => 'MarketLevelD',
        );

    public $marketLevel;
    public $marketID;

    public $marketInfo;

    public $superMarketInfo;
    public $subMarketInfo;

    public $marketDAO;

    public function __construct($marketLevel, $marketID = '') {
        $this->setMarketLevel($marketLevel);
        $this->setmarketID($marketID);
    }

    public function setMarketLevel($marketLevel) {
        if (!empty($marketLevel)) {
            $this->marketLevel = $marketLevel;

            $this->marketDAO = new Market($this->marketLevel);
        }
    }

    public function setmarketID($marketID) {
        if (!empty($marketID)) {
            $this->marketID = $marketID;
            $this->updateInfo();
        }
    }

    public function updateInfo() {
        if ($this->marketDAO) {
            $this->marketInfo = $this->marketDAO->getByID($this->marketID);

            $this->getSuperMarketList();
            $this->getSubMarketList();
        }
    }

    public function getMarketInfo() {
        if (count($this->marketInfo) <= 0) {
            return array();
        }
        
        return array(
            'marketInfo' => $this->marketInfo,
            'superMarketList' => $this->superMarketInfo,
            'subMarketList' => $this->subMarketInfo,
            );
    }

    public function getSuperMarketList() {
        $this->superMarketInfo = array();
        $marketLevelIDX = array_search($this->marketLevel, self::$marketList);
        if (isset(self::$marketList[$marketLevelIDX - 1]) && !empty($this->marketInfo['UpMarketLevelID'])) {
            $marketDao = new Market(self::$marketList[$marketLevelIDX - 1]);
            $this->superMarketInfo = $marketDao->getByID($this->marketInfo['UpMarketLevelID']);
        }

        return $this->superMarketInfo;
    }

    public function getSubMarketList() {
        $this->subMarketInfo = array();
        $marketLevelIDX = array_search($this->marketLevel, self::$marketList);
        if (isset(self::$marketList[$marketLevelIDX + 1])) {
            $marketDao = new Market(self::$marketList[$marketLevelIDX + 1]);
            $this->subMarketInfo = $marketDao->getDownMarketByID($this->marketID);
        }

        return $this->subMarketInfo;
    }

    public function save($data) {
        $newData = $data;
        if (!empty($this->marketInfo)) {
            $newData = array_merge($this->marketInfo, $data);
        }
        
        $id = $this->marketDAO->save($newData);
        if($id != false) {
            if(is_string($id)) {
                $this->marketID = $id;
            }
            $this->updateInfo();
            return true;
        }

        return false;
    }

    public function del() {
        return $this->marketDAO->deleteByID($this->marketID);
    }
}
