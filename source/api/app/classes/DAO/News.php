<?php
include_once(dirname(__FILE__) . "/DAO.php");

class News extends DAO {

    public function _init() {
        //$this->_pks[] = 'FUN_CODE';
        //$this->_pks[] = 'YMD';
        if (empty($this->dbTable)) {
            $this->dbTable = "News";
        }

        $this->filter_date_filed = 'YMD';
        $this->_orderBy = 'DESC';
    }
}
