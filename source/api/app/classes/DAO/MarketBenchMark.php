<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/DAO.php");

class MarketBenchMark extends DAO {

    CONST PLACE = 0;
    CONST BUY = 1;
    CONST SELL  = 2;

    public function _init() {
        //$this->_pk = 'ID';
        $this->dbTable = "MarketLevelC_BenchMark";

        $this->filter_date_filed = 'YMD';
        $this->_orderBy = 'DESC';
    }

    public function getByID($marketID) {
        $conditions[] = self::setCondition($this->_pk, $marketID);

        return $this->getOne($conditions);
    }

    public function deleteByID($marketID) {
        $conditions[] = self::setCondition($this->_pk, $marketID);

        return $this->delete($conditions);
    }

    public function getEMA($marketID, $data) {
        $EMAList = [ 12, 20, 24, 60, 260 ];
        $EMA = 0.0;
        $todayValue = $data["BenchMark_Value"];

        foreach ($EMAList as $n) {
            $data["EMA_$n"] = 0.0;
        }

        $conditions[] = self::setCondition('MarketID', $marketID);
        $conditions[] = self::setCondition('YMD', $data['YMD'], "<");

        $orderBys[] = self::setOrderBy('YMD', 'DESC');

        $res = $this->get($conditions, $orderBys, 2);

        foreach ($EMAList as $n) {
            $t1EMA = 0;
            if (count($res) > 0) {
                $t1EMA = $res[0]["EMA_$n"];
            }
            $data["EMA_$n"] = ($t1EMA * ($n - 1) + $todayValue*2)/($n + 1);
        }

        return $data;
    }

    public function getRSI($marketID, $data, $n = 14) {
        $wRSI = 0.0;
        $todayValue = $data["BenchMark_Value"];
        $upSUM = 0.0;
        $dnSUM = 0.0;

        $conditions[] = self::setCondition('MarketID', $marketID);
        $conditions[] = self::setCondition('YMD', $data['YMD'], "<");

        $orderBys[] = self::setOrderBy('YMD', 'DESC');

        $res = $this->get($conditions, $orderBys, $n - 1);

        if (count($res) > 0) {
            for ($i = 0; $i < count($res); $i++) {
                $UPDN = ($i == 0) ? $todayValue - $res[$i]["BenchMark_Value"] : $res[$i]["BenchMark_Value"] - $res[$i-1]["BenchMark_Value"];

                if($UPDN > 0) {
                    $upSUM+=$UPDN;
                }
                else if($UPDN <0) {
                    $dnSUM+=abs($UPDN);
                }
            }

            if($dnSUM > 0) {
                $wRSI = 100-100/(1+($upSUM/$n)/($dnSUM/$n));
            }
        }
        return $wRSI;
    }

    public function getMACD($marketID, $data) {
        $todayValue = $data["BenchMark_Value"];

        $DIFF = $data['EMA_12'] - $data['EMA_24'];
        $MACD = 0.0;
        $OSC = 0.0;

        $conditions[] = self::setCondition('MarketID', $marketID);
        $conditions[] = self::setCondition('YMD', $data['YMD'], "<");

        $orderBys[] = self::setOrderBy('YMD', 'DESC');

        $res = $this->get($conditions, $orderBys, 2);

        $t1MACD_2 = 0;
        if (count($res) > 0) {
            $t1MACD_2 = $res[0]["MACD_2"];
        }

        $x = 9;
        $MACD = ($t1MACD_2*($x - 1) + $DIFF * 2)/($x + 1);

        $OSC = $DIFF - $MACD;

        $data["MACD_1"] = $DIFF;
        $data["MACD_2"] = $MACD;
        $data["MACD_3"] = $OSC;

        return $data;
    }

    public function benchmark($marketID, $data) {
        $data = $this->getEMA($marketID, $data);
        $data["RSI_1"] = $this->getRSI($marketID, $data);
        $data = $this->getMACD($marketID, $data);

        return $data;
    }

    public function rebuildBenchmark($marketID, $dateArray = array()) {
        $conditions[] = self::setCondition('MarketID', $marketID);

        if(empty($dateArray) || empty($dateArray['startDate'])) {
            $date = new DateTime(empty($dateArray['endDate']) ? 'now' : $dateArray['endDate']);
            $dateArray['endDate'] = $date->format('Y-m-d');
            $date->modify('-2 year');
            $dateArray['startDate'] = $date->format('Y-m-d');
        }
        $conditions[] = self::getBetweenDate('YMD', $dateArray['startDate'], $dateArray['endDate']);

        $orderBys[] = self::setOrderBy('YMD');
        $i = 0;

        $res = $this->get($conditions, $orderBys, 3000);
        foreach ($res as $data) {
            $data = $this->benchmark($marketID, $data);
            if ($this->save($data) != false) {
              $i++;
            }
        }

        return count($res);
    }

    public function getBenchmarkAlertData($marketID, $YMD) {
        $conditions[] = self::setCondition('MarketID', $marketID);
        $conditions[] = self::setCondition('YMD', $YMD, "<=");

        $orderBys[] = self::setOrderBy('YMD', 'DESC');

        $data = $this->get($conditions, $orderBys, 20);

        return count($data) > 0 ? $data : array();
    }

    public function getLastYearMinEMA($marketID, $YMD) {
        $_dateTime = $YMD.' 23:59:59';
        $data = $this->_db->rawQuery('SELECT *, MIN(LEAST(EMA_20, EMA_60)) min FROM `'.$this->dbTable.'` WHERE MarketID = '.$marketID.' AND YMD >= DATE_SUB("'.$_dateTime.'",INTERVAL 1 YEAR) AND YMD <= "'.$_dateTime.'"');

        return count($data) > 0 ? $data : array();
    }

    public function EMA_Alert($data, $YMD) {
        if (count($data) < 2) {
            return self::PLACE;
        }

        $_minEMA = $this->getLastYearMinEMA($data[0]['MarketID'], $YMD);

        $_emaValue1 = ($data[0]["EMA_20"] - $data[0]["EMA_60"]) - ($data[1]["EMA_20"] - $data[1]["EMA_60"]);
        $_emaValue2 = ($data[0]["EMA_20"] - $data[0]["EMA_60"]) / ($_minEMA[0]["EMA_20"] - $_minEMA[0]["EMA_60"]);

        if ($_emaValue1 > 0 && $_emaValue2 > 0.05) {
            return self::BUY;
        }
        else if ($_emaValue1 < 0 && $_emaValue2 > 0.05 ) {
            return self::SELL;
        }

        return self::PLACE;
    }

    public function RSI_Alert($data) {
        if (count($data) < 2) {
            return self::PLACE;
        }

        if ($data[1]["RSI_1"] < 20 && $data[0]["RSI_1"] > 20) {
            return self::BUY;
        }

        if ($data[1]["RSI_1"] > 80 && $data[0]["RSI_1"] < 80) {
            return self::SELL;
        }

        return self::PLACE;
    }

    public function BBands_Alert($data) {
        if (count($data) < 20) {
            return self::PLACE;
        }

        $simple_mean = 0.0;
        $standard_deviation = 0.0;

        for ($i = 0; $i < 20; $i++) {
            $simple_mean += $data[$i]['BenchMark_Value'];
        }

        $simple_mean = $simple_mean / 20;

        for ($i = 0; $i < 20; $i++) {
            $standard_deviation += pow(($data[$i]['BenchMark_Value'] - $simple_mean), 2);
        }

        $standard_deviation = sqrt($standard_deviation / 20);

        if ($data[0]["BenchMark_Value"] < ($simple_mean - $standard_deviation * 2)) {
            return self::BUY;
        }
        else if ($data[0]["BenchMark_Value"] > ($simple_mean - $standard_deviation * 2)) {
            return self::SELL;
        }

        return self::PLACE;
    }
}
