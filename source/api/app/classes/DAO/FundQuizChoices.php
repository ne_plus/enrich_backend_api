<?php
include_once(dirname(__FILE__) . "/DAO.php");

class FundQuizChoices extends DAO {

	public $_fk;

    public function _init() {
        if (empty($this->dbTable)) {
            $this->dbTable = "FUN_QUIZ_CHOICES";
        }

        //$this->filter_date_filed = 'YMD';
        $this->_orderBy = 'DESC';
        $this->_fk = 'question_id';
    }

    public function getByQuizID($id) {
        $conditions[] = self::setCondition($this->_fk, $id);

        $orderBys[] = self::setOrderBy('sequence');
        $orderBys[] = self::setOrderBy($this->_pk);

        return $this->get($conditions, $orderBys);
    }

    public function deleteByID($id) {
        $conditions[] = self::setCondition($this->_fk, $id);

        return $this->delete($conditions);
    }
}
