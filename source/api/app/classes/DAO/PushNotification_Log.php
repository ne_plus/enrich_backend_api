<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/DAO.php");

class PushNotification_Log extends DAO {

    public function _init() {
        $this->_pk = 'ID';
        $this->dbTable = "PushNotification_Log";

        //$this->filter_date_filed = 'CODE';
        $this->_orderBy = 'DESC';
    }
}
