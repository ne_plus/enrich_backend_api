<?php
include_once(dirname(__FILE__) . "/DAO.php");

//基金淨值

class FundNav extends DAO {

    public function _init() {
        //$this->_pks[] = 'FUN_CODE';
        //$this->_pks[] = 'YMD';
        if (empty($this->dbTable)) {
            $this->dbTable = "FUNNAV_DATA";
        }

        $this->filter_date_filed = 'YMD';
        $this->_orderBy = 'DESC';
    }

    public function getDBTableByType($type=0) {
        if ($type == 0) {
            return "FUNNAV";
        }

        return "FUNBNA";
    }

    public function updateInfo($data) {
        if (!is_array($data)) {
            return array();
        }

        if (count($data) <= 0) {
            return array();
        }

        $conditions = $this->getConditionsByPKs($data);

        return $this->update($conditions, $data);
    }

    public function getFundNavSingle($data) {
        if (!is_array($data)) {
            return array();
        }

        if (count($data) <= 0) {
            return array();
        }

        $conditions = $this->getConditionsByPKs($data);

        return $this->get($conditions);
    }
}
