<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/DAO.php");

class Market extends DAO {

    public function _init() {
        //$this->_pk = 'ID';
    }

    public function updateInfo($marketID, $data) {
        $conditions[] = self::setCondition($this->_pk, $marketID);

        return $this->update($conditions, $data);
    }

    public function getByID($marketID) {
        $conditions[] = self::setCondition($this->_pk, $marketID);

        return $this->getOne($conditions);
    }

    public function getDownMarketByID($marketLevelID) {
        $conditions[] = self::setCondition('UpMarketLevelID', $marketLevelID);

        return $this->get($conditions);
    }

    public function deleteByID($marketID) {
        $conditions[] = self::setCondition($this->_pk, $marketID);

        return $this->delete($conditions);
    }
}
