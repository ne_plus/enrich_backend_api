<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//include_once(dirname(__FILE__) . "/../classes/json-to-mysql/include.classloader.php");
//$classLoader->addToClasspath(dirname(__FILE__) . "/../classes/json-to-mysql/");

class DAO {
    protected $dbTable;

    protected $_pk;
    protected $_pks;

    protected $_db;

    protected $_debug = false;

    public $_dbFields;

    public $_lastError;

    protected $q_fileds;
    protected $filter_date_filed;

    protected $_orderBy;

    public function __construct($dbTable = '') {
        $this->_db = self::getDBConnect();
        $this->dbTable = $dbTable;
        $this->_pk = null;
        $this->_pks = array();
        $this->_dbFields = array();

        $this->_orderBy = 'ASC';

        $this->_init();

        $this->getTableFields();

        if($this->_debug) {
            $this->_db->setTrace(true);
        }
    }

    public function _init() {

    }

    public function getDBConnect($type = 0) {
    	if ($type == 0) {
    		$db = new MysqliDb (DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME);
       		if(!$db) die("Database error");

       		return $db;
    	}
    	
    	$mysql = new MySQLConn(DATABASE_HOST, DATABASE_NAME, DATABASE_USER, DATABASE_PASS);
		$db = new JSONtoMYSQL($mysql);

       	return $db;
    }

    public function getTableFields() {
        $fields = $this->_db->rawQuery('show fields from `'.$this->dbTable.'`');
        $this->_dbFields = array_column($fields, 'Field');

        foreach ($fields as $value) {
            if ($value['Key'] == 'PRI') {
                $this->_pks[] = $value['Field'];
            }
        }

        $this->_pks = array_unique($this->_pks);

        if (count($this->_pks) == 1 && is_null($this->_pk)) {
            $this->_pk = $this->_pks[0];
        }
    }

    public function dataExistPKsValue($data) {
        foreach ($this->_pks as $pk) {
            if (empty($data[$pk])) {
                return false;
            }
        }

        return true;
    }

    public function getConditionsByPKs($data) {
        $conditions = array();
        foreach ($this->_pks as $pk) {
            if (!empty($data[$pk])) {

                $conditions[] = self::setCondition($pk, $data[$pk]);
            }
        }

        return $conditions;
    }

    public function setCondition($field, $value, $operator = '=') {
        return array(
            'field' => $field, 
            'value' => $value,
            'operator' => $operator,
        );
    }

    public function setOrderBy($field, $value = 'ASC') {
        return array(
            'field' => $field, 
            'value' => $value,
        );
    }

    public function setConditions($conditions) {
        if (!empty($conditions)) {
            foreach ($conditions as $condition) {
                $operator = empty($condition['operator']) ? '=' : $condition['operator'];

                $this->_db->where($condition['field'], $condition['value'], $operator);
            }
        }
    }

    public function setOrderBys($orderBys) {
        if (!empty($orderBys)) {
            foreach ($orderBys as $orderBy) {
                $this->_db->orderBy($orderBy['field'], $orderBy['value']);
            }
        }
    }

    public function getBetweenDate($field, $startDate, $endDate = null) {
        if(empty($endDate)) {
            $endDate = date("Y-m-d");
        }

        //$tomorrow = date('Y-m-d', strtotime(str_replace('-', '/', $endDate) . "+1 days"));

        return self::setCondition($field, array($startDate, $endDate), 'BETWEEN');
    }

    public function processFilterOptions($filterOptions) {
        if(empty($filterOptions)) {
            return array();
        }

        $conditions = array();

        if(!empty($filterOptions['and'])) {
            foreach ($filterOptions['and'] as $and) {
                if(!empty($and) && isset($and['field'])) {
                    $conditions[] = self::setCondition($and['field'], $and['value'], $and['operator']);
                }
            }
        }

        if(!empty($filterOptions['date']) && !empty($this->filter_date_filed)) {
            $conditions[] = self::getBetweenDate($this->filter_date_filed, $filterOptions['date']['startDate'], $filterOptions['date']['endDate']);
        }

        if(!empty($filterOptions['qs']) && count($this->q_fileds) > 0) {
            $queryString = "";
            $queryValues = array();

            foreach ($this->q_fileds as $value) {
                $queryString = $queryString . ($queryString != '' ? ' OR ' : '') . "$value LIKE ? ";
                $queryValues[] = $filterOptions['qs'];
            }
            $queryString = "( $queryString )";

            $conditions[] = self::setCondition($queryString, $queryValues, 'like');
        }

        return $conditions;
    }

    public function get($conditions = null, $orderBys = null, $limit = 1000) {
        //$this->_db->reset();
        $this->setConditions($conditions);
        $this->setOrderBys($orderBys);

        $res = $this->_db->get($this->dbTable, $limit);

        if ($this->_db->count >0) {
            return $res;
        }

        return array();
    }

    public function getOne($conditions = null, $orderBys = null) {
        //$this->_db->reset();
        $res = $this->get($conditions, $orderBys, 1);
        if (count($res) > 0) {
            return $res[0];
        }

        return array();
    }

    public function getByPage($pageInfo, $conditions = array(), $orderBys = null) {
        $this->setConditions($conditions);
        $this->setOrderBys($orderBys);

        // set page limit to 2 results per page. 20 by default
        $this->_db->pageLimit = $pageInfo['count'];
        $res = $this->_db->arraybuilder()->paginate($this->dbTable, $pageInfo['offset']);

        if ($this->_db->count >0) {
            $pageInfo = array(
                'total_pages' => (int)$this->_db->totalPages,
                'page_limit' => (int)$this->_db->pageLimit,
                'current_page' => (int)$pageInfo['offset'],
                'total' => (int)$this->_db->totalCount,
                );
            return array(
                'data' => $res,
                'page_info' => $pageInfo,
                );
        }

        return array();
    }

    public function getData($filterOptions = array(), $orderOptions = array(), $pageInfo = array()) {
        $orderBys = array();
        $conditions = $this->processFilterOptions($filterOptions);

        if(empty($orderOptions)) {
            foreach ($this->_pks as $pk) {
                $orderBys[] = self::setOrderBy($pk, $this->_orderBy);
            }
        }

        if(empty($pageInfo)) {
            return $this->get($conditions, $orderBys);
        }

        return $this->getByPage($pageInfo, $conditions, $orderBys);
    }

    public function insert($data) {
        // auto add create date time
        if (!isset($data['createdate'])) {
            $data['createdate'] = date("Y-m-d H:i:s");
        }

        $insertData = array_intersect_key($data, array_flip($this->_dbFields));

        $id = $this->_db->insert($this->dbTable, $insertData);
        if ($id != false) {
            //$id = $this->_db->getInsertId();
            //echo 'user was created. Id=' . $id;
            return $id;
        }
        else {
            //echo 'insert failed: ' . $this->_db->getLastError();
            $this->_lastError = 'insert failed: ' . $this->_db->getLastError();
        }

        return false;
    }

    public function update($conditions, $data) {
        $updateData = array_intersect_key($data, array_flip($this->_dbFields));

        if(isset($updateData['lastupdatetime'])) {
            unset($updateData['lastupdatetime']);
        }
        if(isset($updateData['createdate'])) {
            unset($updateData['createdate']);
        }

        $this->setConditions($conditions);

        if ($this->_db->update($this->dbTable, $updateData)) {
            //echo $this->_db->count . ' records were updated';
            return true;
        }
        else {
            //echo 'update failed: ' . $this->_db->getLastError();
            $this->_lastError = 'update failed: ' . $this->_db->getLastError();
            return false;
        }
    }

    public function save($data) {
        $saveData = array_intersect_key($data, array_flip($this->_dbFields));

        $res = array();
        $conditions = array();

        if ($this->dataExistPKsValue($data)) {
            $conditions = $this->getConditionsByPKs($data);
            $res = $this->getOne($conditions);
        }

        if (!empty($res)) {
            return $this->update($conditions, array_merge($res, $saveData));
        }
        
        return $this->insert($saveData);
    }

    public function delete($conditions) {
        //$this->_db->reset();
        $this->setConditions($conditions);

        if($this->_db->delete($this->dbTable)) {
            //echo 'successfully deleted';
            return true;
        }
        else {
            //echo 'delete failed: ' . $this->_db->getLastError();
            $this->_lastError = 'delete failed: ' . $this->_db->getLastError();
            return false;
        }
    }

    public function deleteByPks($data) {
        if (!$this->dataExistPKsValue($data)) {
            return false;
        }

        $conditions = $this->getConditionsByPKs($data);

        return $this->delete($conditions);
    }

    public function grouping($clause, $cols = null, $conditions = null, $orderBys = null, $limit = 1000) {
        $this->setConditions($conditions);
        $this->setOrderBys($orderBys);

        $this->_db->groupBy($clause);
        $res = $this->_db->get($this->dbTable, $limit, $cols);

        if ($this->_db->count >0) {
            return $res;
        }

        return array();
    }

    public function rawQuery($_sql, $_params = array()) {
        return $this->_db->rawQuery($_sql, $_params);
    }

    public function debugMsg()
    {
        if($this->_debug) {
            return $this->_db->trace;
        }
    }
}
