<?php
include_once(dirname(__FILE__) . "/DAO.php");

class CT0CUR extends DAO {

    public function _init() {
        //$this->_pks[] = 'FUN_CODE';
        //$this->_pks[] = 'YMD';
        if (empty($this->dbTable)) {
            $this->dbTable = "CT0CUR";
        }

        $this->filter_date_filed = 'CUR_CODE';
        $this->_orderBy = 'DESC';
    }

    public function getCurrsInfo($data = array()) {
    	$conditions = array();

        if (!empty($data)) {
            $conditions[] = DAO::setCondition('CUR_CODE', $data, 'IN');
        }

        $orderBys[] =DAO::setOrderBy('CUR_CODE', 'DESC');

        $res = $this->get($conditions, $orderBys);
        return $res;
    }
}
