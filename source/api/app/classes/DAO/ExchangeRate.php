<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/DAO.php");

class ExchangeRate extends DAO {

    public function _init() {
        //$this->_pk = 'ID';
        $this->dbTable = "ExchangeRate";

        $this->filter_date_filed = 'YMD';
        $this->_orderBy = 'DESC';
    }

    public function getSingleCurInfo() {
    	$data = $this->_db->rawQuery('SELECT MAX(YMD) AS YMD, CUR_NM, VALUE, createdate, lastupdatetime FROM `'.$this->dbTable.'` GROUP BY CUR_NM');
    	return $data;
    }
}
