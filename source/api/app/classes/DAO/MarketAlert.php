<?php
include_once(dirname(__FILE__) . "/DAO.php");

//市場經濟指標

class MarketAlert extends DAO {

    public function _init() {
        //$this->_pks[] = 'FUN_CODE';
        //$this->_pks[] = 'YMD';
        if (empty($this->dbTable)) {
            $this->dbTable = "MarketLevelC_Alert";
        }

        $this->filter_date_filed = 'YMD';
        $this->_orderBy = 'DESC';
    }

    public function getAlertList($_YMD) {
    	$data = $this->_db->rawQuery('SELECT * FROM `'.$this->dbTable.'` WHERE YMD='.$_YMD);
    	return $data;
    }
}
