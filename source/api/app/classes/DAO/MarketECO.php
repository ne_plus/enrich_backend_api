<?php
include_once(dirname(__FILE__) . "/DAO.php");

//市場經濟指標

class MarketECO extends DAO {

    public function _init() {
        //$this->_pks[] = 'FUN_CODE';
        //$this->_pks[] = 'YMD';
        if (empty($this->dbTable)) {
            $this->dbTable = "MarketLevelC_ECO";
        }

        $this->filter_date_filed = 'YMD';
        $this->_orderBy = 'DESC';
    }
}
