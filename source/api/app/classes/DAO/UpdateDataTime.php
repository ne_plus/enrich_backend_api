<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/DAO.php");

class UpdateDataTime extends DAO {

	const FTP = 0;
    const INFOTIME_DATA = 1;
    const STOCKAI_DATA = 2;

    public function _init() {
        //$this->_pk = 'ID';
        $this->dbTable = "UPDATE_DATA";
    }

    public function start_update($type) {
        $obj = array(
        	'TYPE' => $type,
        	'updatetime' => '',
        	'OTHERS' => '',
        );

        return $this->save($obj);
    }

    public function finish_update($id, $type, $data) {
        $obj = array(
        	'ID' => $id,
        	'TYPE' => $type,
        	'updatetime' => date("Y-m-d H:i:s"),
        	'OTHERS' => $data,
        );

        return $this->save($obj);
    }

    public function getLastUpdateTimestamp() {
        $conditions[] = DAO::setCondition('TYPE', UpdateDataTime::INFOTIME_DATA, '=');
        $orderBys[] =DAO::setOrderBy('ID', 'DESC');

        $res = $this->get($conditions, $orderBys, 1);
        return $res[0];
    }
}
