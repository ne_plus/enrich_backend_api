<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/DAO.php");

class Investconsult extends DAO {

    public function _init() {
        //$this->_pk = 'ID';
        $this->dbTable = "Investconsult";
    }

    public function getData() {
        $orderBys[] = self::setOrderBy($this->_pk, 'DESC');

        return $this->getOne(array(), $orderBys);
    }
}
