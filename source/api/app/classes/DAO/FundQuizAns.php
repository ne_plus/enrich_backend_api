<?php
include_once(dirname(__FILE__) . "/DAO.php");

class FundQuizAns extends DAO {

	public $_fk;

    public function _init() {
        if (empty($this->dbTable)) {
            $this->dbTable = "FUN_QUIZ_ANSWER";
        }

        //$this->filter_date_filed = 'YMD';
        $this->_orderBy = 'DESC';
        $this->_fk = 'question_id';
    }

    public function getByQuizID($id) {
        $conditions[] = self::setCondition($this->_fk, $id);

        return $this->get($conditions);
    }

    public function deleteByID($id) {
        $conditions[] = self::setCondition($this->_fk, $id);

        return $this->delete($conditions);
    }

    public function getResult($id) {
        $conditions[] = self::setCondition($this->_fk, $id);

        return $this->grouping("choice_id", Array("choice_id", "count(choice_id) as value"), $conditions);
    }
}
