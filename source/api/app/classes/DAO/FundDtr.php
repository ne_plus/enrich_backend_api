<?php
include_once(dirname(__FILE__) . "/DAO.php");

//基金報酬

class FundDtr extends DAO {

    public function _init() {
        //$this->_pks[] = 'FUN_CODE';
        //$this->_pks[] = 'YMD';
        if (empty($this->dbTable)) {
            $this->dbTable = "FUNDTR_DATA";
        }

        $this->filter_date_filed = 'DTR_YMD1';
        $this->_orderBy = 'DESC';
    }

    public function getDBTableByType($type=0) {
        if ($type == 0) {
            return "FUNDTR";
        }

        return "FUNBDT";
    }

    public function getCombination($conditions, $orderBys = null, $limit = 1000) {
        //$this->_db->reset();

        $cols = Array ("FUN_CODE", "DTR_YMD1", "DTR_AMT", "DTR_YMD2", "MAX(DTR_B_YMD) AS DTR_B_YMD", "ABREV", "DTR_RAT_Y", "FUND_TYPE", "createdate", "lastupdatetime");

        $this->setConditions($conditions);
        
        $this->_db->groupBy("FUN_CODE");

        //$this->setOrderBys($orderBys);
        $this->_db->orderBy("DTR_B_YMD","DESC");
        $this->_db->orderBy("DTR_RAT_Y","DESC");

        $res = $this->_db->get($this->dbTable, $limit, $cols);

        if ($this->_db->count >0) {
            return $res;
        }

        return array();
    }

    public function updateInfo($data) {
        if (!is_array($data)) {
            return array();
        }

        if (count($data) <= 0) {
            return array();
        }

        $conditions = $this->getConditionsByPKs($data);

        return $this->update($conditions, $data);
    }

    public function getFundBtrSingle($data) {
        if (!is_array($data)) {
            return array();
        }

        if (count($data) <= 0) {
            return array();
        }

        $conditions = $this->getConditionsByPKs($data);

        return $this->get($conditions);
    }
}
