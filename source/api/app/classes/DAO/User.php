<?php
include_once(dirname(__FILE__) . "/DAO.php");

class User extends DAO {

    public static $RiskTYPE = array(
        'FX' => '外匯型',
        'Coupon' => '配息型',
        'RR2' => '穩健型',
        'RR3' => '積極型',
        'Risky' => '賭徒型',
        );

    public static $AGE = array(
        5 => '21-30歲',
        4 => '31-40歲',
        3 => '41-50歲',
        2 => '51-60歲',
        1 => '60up歲',
        );

    public static $GANDER = array(
        'M' => '帥哥',
        'F' => '美女',
        );

    public static $ASSETS = array(
        0 => '每月1萬以下',
        1 => '每月1~3萬',
        2 => '每月5萬以上',
        3 => '單筆100~300萬',
        4 => '單筆300萬以上',
        );

    public static $WORK = array(
        0 => '金融業',
        1 => '科技業',
        2 => '服務業',
        3 => '傳產',
        4 => '學生',
        5 => '家管',
        6 => '退休族',
        );

    public static $INFOSOURCE = array(
        0 => '金融機構、理專',
        1 => '電視',
        2 => '網路',
        3 => '報章雜誌',
        4 => '親朋好友',
        );

    public static $INVESTMENT = array(
        0 => '基金',
        1 => '股票',
        2 => '衍生性商品',
        3 => '保險',
        4 => '存款',
        5 => '房地產',
        6 => '古董藝術品',
        );

    public static $OBJECTIVE = array(
        0 => '固定領息',
        1 => '貨幣配置',
        2 => '資產增值',
        3 => '資產保全',
        4 => '閒置資金運用',
        5 => '子女教育',
        6 => '退休規劃',
        );

    public static $PROFIT = array(
        1 => '5%/-3%',
        2 => '8%/-10%',
        3 => '15%/-20%',
        6 => '200%/-100%',
        );

    public static $LOSSSITUATION = array(
        1 => '立即停損',
        2 => '暫且觀望，虧損30%以上才賣',
        3 => '分批加碼',
        4 => '尋找最低點一次加碼',
        );

    public static $DURING = array(
        4 => '投資就是短期快速獲利',
        1 => '偏好一年內的投資',
        2 => '維持三年內該資金不做他用',
        3 => '可以規劃三年以上的組合，不需領回',
        );

    public function _init() {
        $this->dbTable = "USER";
        //$this->_pk = 'ID';
    }

    public function updateInfo($userID, $data) {
        $conditions[] = array(
            'field' => $this->_pk, 
            'value' => $userID,
            );

        return $this->update($conditions, $data);
    }

    public function getByID($userID) {
        $conditions[] = array(
            'field' => $this->_pk, 
            'value' => $userID,
            );

        return $this->getOne($conditions);
    }

    public function getByEmail($eMail) {
        $conditions[] = array(
            'field' => 'EMAIL', 
            'value' => $eMail,
            );

        return $this->getOne($conditions);
    }

    public function deleteByID($userID) {
        $conditions[] = array(
            'field' => $this->_pk, 
            'value' => $userID,
            );

        return $this->delete($conditions);
    }
}
