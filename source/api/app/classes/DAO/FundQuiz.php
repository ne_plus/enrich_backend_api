<?php
include_once(dirname(__FILE__) . "/DAO.php");

class FundQuiz extends DAO {

    public function _init() {
        //$this->_pks[] = 'FUN_CODE';
        //$this->_pks[] = 'YMD';
        if (empty($this->dbTable)) {
            $this->dbTable = "FUN_QUIZ";
        }

        //$this->filter_date_filed = 'YMD';
        $this->_orderBy = 'DESC';
    }

    public function getByID($id) {
        $conditions[] = self::setCondition($this->_pk, $id);

        return $this->get($conditions);
    }

    public function deleteByID($id) {
        $conditions[] = self::setCondition($this->_pk, $id);

        return $this->delete($conditions);
    }
}
