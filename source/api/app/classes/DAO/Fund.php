<?php
include_once(dirname(__FILE__) . "/DAO.php");

class Fund extends DAO {

    public function _init() {
        //$this->_pk = 'FUN_CODE';
        if (empty($this->dbTable)) {
            $this->dbTable = "FUNDATA";
        }

        $this->q_fileds = array('NM_C', 'NM_C_F', 'NM_E');
    }

    public function save($data) {
        if($data['END_DATE'] != '0000-00-00') {
            $today = date("Y-m-d");

            $data['eliminate'] = (strtotime($data['END_DATE']) < strtotime($today)) ? 0 : 1;
        }
        else {
            $data['eliminate'] = 1;
        }
        
        return parent::save($data);
    }

    public function getDBTableByType($type=0) {
        if ($type == 0) {
            return "FUNBAS";
        }

        return "FUNBBA";
    }

    public function updateInfo($fundID, $data) {
        $conditions[] = self::setCondition($this->_pk, $fundID);

        return $this->update($conditions, $data);
    }

    public function getByID($fundID) {
        $conditions[] = self::setCondition($this->_pk, $fundID);

        return $this->get($conditions);
    }

    public function deleteByID($fundID) {
        $conditions[] = self::setCondition($this->_pk, $fundID);

        return $this->delete($conditions);
    }
}
