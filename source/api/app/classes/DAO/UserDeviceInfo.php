<?php
include_once(dirname(__FILE__) . "/DAO.php");

class UserDeviceInfo extends DAO {

    const iOS = 0;
    const Android = 1;

    const Unknown = 99;

    public function _init() {
        $this->dbTable = "USER_DEVICE_INFO";
        //$this->_pk = 'ID';
    }

    public function getByID($userID, $ID = null) {
        $conditions[] = self::setcondition('USER_ID', $userID);

        if (!empty($ID)) {
            $conditions[] = self::setcondition('DEVICE_ID', $ID);
        }

        return $this->get($conditions);
    }
}
