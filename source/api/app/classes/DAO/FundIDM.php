<?php
include_once(dirname(__FILE__) . "/DAO.php");

//基金風險係數

class FundIDM extends DAO {

    public static $IDX_CODE_LIST = ['BETA_M', 'SHARPE_M', 'STDDIV_M', 'STDDIV_Q', 'STDDIV_Y', 'STDDV36M'];

    public function _init() {
        //$this->_pks[] = 'FUN_CODE';
        //$this->_pks[] = 'YMD';
        if (empty($this->dbTable)) {
            $this->dbTable = "FUNIDM_DATA";
        }

        $this->filter_date_filed = 'YM';
    }

    public function getDBTableByType($type=0) {
        if ($type == 0) {
            return "FUNIDM";
        }

        return "FUBIDM";
    }

    public function updateInfo($data) {
        if (!is_array($data)) {
            return array();
        }

        if (count($data) <= 0) {
            return array();
        }

        $conditions = $this->getConditionsByPKs($data);

        return $this->update($conditions, $data);
    }
}
