<?php
include_once(dirname(__FILE__) . "/DAO.php");

class UserFundsRecord extends DAO {

    public function _init() {
        $this->dbTable = "USER_FUNDS_RECORD";
        //$this->_pk = 'ID';
    }

    public function getByID($userID, $ID = null) {
        $conditions[] = self::setcondition('USER_ID', $userID);

        if (!empty($ID)) {
            $conditions[] = self::setcondition('ID', $ID);
        }

        return $this->get($conditions);
    }

}
