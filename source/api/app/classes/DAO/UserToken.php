<?php
include_once(dirname(__FILE__) . "/DAO.php");

class UserToken extends DAO {

    public function _init() {
        $this->dbTable = "USER_TOKEN";
        //$this->_pk = 'ID';
    }

    public function validateExpireTime($tokenExpireTime) {
        $today_dt = new DateTime('now');
        $expire_dt = new DateTime($tokenExpireTime);

        if ($expire_dt < $today_dt) { 
            return true;
        }

        return false;
    }

    public function validateToken($token) {
        $conditions[] = array(
            'field' => 'TOKEN', 
            'value' => $token,
            );

        $res = $this->getOne($conditions);

        if (!empty($res)) {
            return $this->validateExpireTime($res['TOKEN_EXPIRE']);
        }

        return false;
    }

    public function validateUserToken($userID, $token, $isCheckTime = true) {
        $conditions[] = self::setcondition('TOKEN', $token);
        $conditions[] = self::setcondition('USER_ID', $userID);

        $res = $this->getOne($conditions);

        if (!empty($res)) {
            if ($isCheckTime) {
                return $this->validateExpireTime($res['TOKEN_EXPIRE']);
            }
            else {
                return true;
            }
        }

        return false;
    }

    public function getDefaultExpiration() {
        return date('Y-m-d H:i:s', strtotime('+60 days'));
    }

    public function generateRandomString($length = 10) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        $pass = array(); //remember to declare $pass as an array
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function generateToken() {
        $token = "";
        $i = 0;

        while ( $i<10 ) {
            //$token = bin2hex(openssl_random_pseudo_bytes(20)); //generate a random token
            $token = bin2hex($this->generateRandomString(30)); //generate a random token

            $conditions[] = array(
            'field' => 'TOKEN', 
            'value' => $token,
            );

            $res = $this->getOne($conditions);

            if (empty($res)) {
                break;
            }

            $i++;
        }

        return array(
            'TOKEN' => $token,
            'TOKEN_EXPIRE' => $this->getDefaultExpiration(),
            );
    }

    public function output($tokenInfo) {
        $ret = array(
            'access_token' => $tokenInfo['TOKEN'],
            'access_token_expire' => $tokenInfo['TOKEN_EXPIRE'],
            );
        return $ret;
    }

    public function getToken($token) {
        $conditions[] = self::setcondition('TOKEN', $token);

        $res = $this->getOne($conditions);
        if(!empty($res)) {
            return $res;
        }

        return array();
    }


    public function getUserIDByToken($token) {
        $conditions[] = self::setcondition('TOKEN', $token);

        $res = $this->getOne($conditions);
        if(!empty($res)) {
            return $res['USER_ID'];
        }

        return false;
    }

    public function refreshToken($userID, $token = '', $tokenExpiration = '') {
        $data = Array (
            'USER_ID' => $userID,
            'TOKEN' => $token,
            'TOKEN_EXPIRE' => empty($tokenExpiration) ? $this->getDefaultExpiration() : $tokenExpiration,
        );

        if (empty($token)) {
            $data = array_merge($data, $this->generateToken());
        }

        if ($this->save($data) != false) {
            return $data['TOKEN'];
        }

        return false;
    }

    public function updateToken($userID, $token, $tokenExpiration) {
        return $this->refreshToken($userID, $token, $tokenExpiration);
    }

    public function delete($userID, $token) {
        $conditions[] = self::setcondition('TOKEN', $token);
        $conditions[] = self::setcondition('USER_ID', $userID);

        return $this->delete($conditions);
    }
}
