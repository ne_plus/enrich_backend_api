<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/baseRouter.php");

include_once(dirname(__FILE__) . "/../controller/UserController.php");

class UserRouter extends BaseRouter {
	//protected $app;

    public function __construct($app) {
       $this->app = $app;
    }

	public function createRoutes() {
		$app = $this->app;

    	// other routes, you may divide routes to class methods

		$app->group('/user', function () use ($app) {

			$app->get('[/]', '\UserController:doShow')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

			$app->map(['POST'], '/login', '\UserController:doLogin');

			$app->map(['POST'], '/signUp', '\UserController:doSignUp');

			$app->map(['POST'], '/refreshToken', '\UserController:doRefreshToken');

			$app->map(['POST'], '/forgetPassword', '\UserController:doForgetPassword');

			$app->map(['POST'], '/resetPassword', '\UserController:doResetPassword')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::USER);

			$app->group('/{userID}', function() use ($app) {
				$app->get('[/]', '\UserController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::USER);

				$app->map(['POST', 'PUT'], '[/]', '\UserController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::USER);

				$app->delete('[/]', '\UserController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

				//watch list
				$app->group('/watchlist', function () use ($app) {
					$app->get('[/]', '\UserController:doWatchListAction');

					$app->map(['POST', 'PUT'], '[/]', '\UserController:doWatchListAction');

					$app->delete('[/]', '\UserController:doWatchListAction');
				})->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::USER);
				
				//buy funds
				$app->group('/buyfunds', function () use ($app) {
					$app->get('[/]', '\UserController:doBuyFundsAction');

					$app->map(['POST', 'PUT'], '[/]', '\UserController:doBuyFundsAction');

					$app->delete('[/]', '\UserController:doBuyFundsAction');
				})->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::USER);
			});
		});
	}
}
