<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/baseRouter.php");
include_once(dirname(__FILE__) . "/../controller/QuizController.php");

class QuizRouter extends BaseRouter {
	//protected $app;

    public function __construct($app) {
       $this->app = $app;
    }

	public function createRoutes() {
		$app = $this->app;

    	// other routes, you may divide routes to class methods

		$app->group('/quiz', function () use ($app) {

			$app->map(['GET'], '[/]', '\QuizController:doShow');

			$app->group('/{quizID}', function() use ($app) {
				$app->get('[/]', '\QuizController:doAction');

				$app->map(['POST', 'PUT', 'DELETE'], '[/]', '\QuizController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

				//vote
				$app->group('/vote', function () use ($app) {
					$app->get('[/]', '\QuizController:doVoteAction');

					$app->map(['POST', 'PUT'], '/{choicesID}', '\QuizController:doVoteAction');
				});
			});
		});
	}
}
