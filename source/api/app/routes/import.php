<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Prezto\IpFilter\IpFilterMiddleware;
use Prezto\IpFilter\Mode;

use League\Csv\Reader;

include_once(dirname(__FILE__) . "/baseRouter.php");

class ImportRouter extends BaseRouter {
	protected $app;

    public function __construct($app) {
       $this->app = $app;
    }

	public function createRoutes() {
		$app = $this->app;

    	// other routes, you may divide routes to class methods

    	$app->group('/import', function () use ($app) {

    		$app->map(['GET'], '[/]', function (Request $request, Response $response, $args) {
			    $dataTable = $args['dataTable'];

			    
			    $response = $response->withJson(array('msg' => "[".$_SERVER['REMOTE_ADDR']."]"));

			    return $response;
			});

			$app->map(['POST', 'PUT'], '/[{dataTable}]', function (Request $request, Response $response, $args) {
			    $dataTable = $args['dataTable'];

			    //$db[] = self::getDBConnect(1);

			    $files = $request->getUploadedFiles();
			    var_dump($files);
			    //var_dump(file_get_contents($files['csv']->file));
			    return;

			    if (empty($files['csv'])) {
			        throw new Exception('Expected a newfile');
			    }

			    $csv = $files['csv'];

			    $inputCsv = Reader::createFromPath($csv->file);
				$inputCsv->setDelimiter(',');
				$inputCsv->setEncodingFrom('UTF-8');
				
				//get the header
				$headers = array();
				$dataArray = array();

				foreach ($inputCsv as $index => $row) {
				    //do something meaningful here with $row !!
				    //$row is an array where each item represent a CSV data cell
				    //$index is the CSV row index

				    if($index == 0) {
				    	$headers = $row;
				    	continue;
				    }
				    $newRow = array_combine($headers, $row);
				    unset($newRow['ACTION']);
				    //var_dump($newRow);

				    // save it to a table
					//$db->save($data, $dataTable);

					$dataArray[] = $newRow;

				    if($index == 20) {
				    	break;
				    }
				}

				//$response->getBody()->write($res);
			    $response = $response->withJson($dataArray);

			    return $response;
			});

		})->add(new IpFilterMiddleware(['192.168.99.1'], Mode::DENY));
	}
}
