<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/baseRouter.php");
include_once(dirname(__FILE__) . "/../classes/DAO/UpdateDataTime.php");

class MiscRouter extends BaseRouter {
	//protected $app;

    public function __construct($app) {
       $this->app = $app;
    }

	public function createRoutes() {
		$app = $this->app;

    	// other routes, you may divide routes to class methods

		$app->group('/misc', function () use ($app) {

			//$app->container['Auth'][] = 'aaaaa';
			$app->map(['GET'], '/updatetime[/]', function (Request $request, Response $response) {
				$_dao = new UpdateDataTime();
				$data = $_dao->getLastUpdateTimestamp();
				$response = $response->withJson($data);

			    return $response;
			});
		});
	}
}
