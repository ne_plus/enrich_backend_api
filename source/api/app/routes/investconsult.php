<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/baseRouter.php");
include_once(dirname(__FILE__) . "/../controller/InvestconsultController.php");

class InvestconsultRouter extends BaseRouter {
	//protected $app;

    public function __construct($app) {
       $this->app = $app;
    }

    public function setAuthScope($prefix = '') {
    	//return array(
    	//	$prefix.'/test' => Slim\Auth\User::USER,
    	//	);
    }

	public function createRoutes() {
		$app = $this->app;

    	// other routes, you may divide routes to class methods

		$app->group('/investconsult', function () use ($app) {

			//$app->container['Auth'][] = 'aaaaa';
			$app->map(['GET'], '[/]', '\InvestconsultController:doAction');

			$app->map(['POST', 'PUT'], '[/]', '\InvestconsultController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);
		});
	}
}
