<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/baseRouter.php");
include_once(dirname(__FILE__) . "/../controller/ExchangeRateController.php");

class ExchangeRateRouter extends BaseRouter {
	//protected $app;

    public function __construct($app) {
       $this->app = $app;
    }

	public function createRoutes() {
		$app = $this->app;

    	// other routes, you may divide routes to class methods

		$app->group('/exchangerate', function () use ($app) {

			//$app->container['Auth'][] = 'aaaaa';
			
			$app->group('/maptable', function () use ($app) {
				$app->map(['GET'], '[/{CUR_NM}]', '\ExchangeRateController:getCurrMapTable');
			})->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::USER);

			$app->map(['POST'], '/upload[/]', '\ExchangeRateController:doUploadAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

			$app->map(['GET'], '[/{CUR_NM}]', '\ExchangeRateController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::USER);

			$app->map(['POST', 'PUT', 'DELETE'], '[/]', '\ExchangeRateController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);
		});
	}
}
