<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/baseRouter.php");
include_once(dirname(__FILE__) . "/../controller/MarketController.php");

class MarketLevelRouter extends BaseRouter {
	protected $app;

    public function __construct($app) {
       $this->app = $app;
    }

	public function createRoutes() {
		$app = $this->app;

    	// other routes, you may divide routes to class methods

		$app->group('/market/{marketLevel}', function () use ($app) {

			$app->get('[/]', '\MarketController:doShow');

			$app->map(['POST', 'PUT', 'DELETE'], '[/]', '\MarketController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

			$app->get('/currency[/]', '\MarketController:getMarketCurrList');
			
			$app->group('/eco', function() use ($app) {
				$app->get('/list[/]', '\MarketController:getECOList');

				$app->get('/{econame}[/]', '\MarketController:doECOAction');
				
				//$app->map(['POST', 'PUT', 'DELETE'], '/{econame}[/]', '\MarketController:doECOAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);
			});

			$app->get('/alert[/]', '\MarketController:getAlertList')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);
			

			$app->group('/{marketID}', function() use ($app) {

				$app->get('/supermarket[/]', '\MarketController:doShowSuperMarket');

				$app->get('/submarket[/]', '\MarketController:doShowSubMarket');


				$app->map(['GET', 'POST', 'PUT'], '/alert[/]', '\MarketController:doAlertAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

				//doBenchMarkAction
				$app->map(['GET'], '/benchmark[/]', '\MarketController:doBenchMarkAction');
				$app->map(['GET'], '/benchmark/rebuild[/]', '\MarketController:doBenchMarkReBuild')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);
				
				$app->map(['POST', 'PUT', 'DELETE'], '/benchmark', '\MarketController:doBenchMarkAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

				//doBenchMarkAction
				$app->map(['GET'], '/ecovalue[/]', '\MarketController:doECOShow');

				$app->get('[/]', '\MarketController:doAction');

				$app->map(['POST', 'PUT'], '[/]', '\MarketController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

				$app->delete('[/]', '\MarketController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);
			});
		});
	}
}
