<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/baseRouter.php");
include_once(dirname(__FILE__) . "/../controller/FundController.php");

class FundRouter extends BaseRouter {

    public function __construct($app) {
       $this->app = $app;
    }

	public function createRoutes() {
		$app = $this->app;

    	// other routes, you may divide routes to class methods
    	
		$app->group('/fund', function () use ($app) {
			$app->get('[/]', '\FundController:doShow');

			$app->map(['POST', 'PUT'], '[/]', '\FundController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

			$app->delete('[/]', '\FundController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

			$app->group('/combination', function() use ($app) {
				$app->get('[/{type}]', '\FundController:doCombinationShow');
			});

			$app->group('/{fundID}', function() use ($app) {

			    $app->get('[/]', '\FundController:doAction');

				$app->map(['POST', 'PUT'], '[/]', '\FundController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

				$app->delete('[/]', '\FundController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

				//淨值
				$app->group('/Nav', function () use ($app) {
					$app->get('[/{params:.*}]', '\FundController:doNavAction');

					$app->map(['POST', 'PUT'], '[/]', '\FundController:doNavAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

					$app->delete('[/]', '\FundController:doNavAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);
				});

				//報酬率
				$app->group('/Ret', function () use ($app) {
					$app->get('[/{params:.*}]', '\FundController:doRetAction');

					$app->map(['POST', 'PUT'], '[/]', '\FundController:doRetAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

					$app->delete('[/]', '\FundController:doRetAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);
				});

				//報酬率
				$app->group('/Dtr', function () use ($app) {
					$app->get('[/{params:.*}]', '\FundController:doDtrAction');

					$app->map(['POST', 'PUT'], '[/]', '\FundController:doDtrAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);

					$app->delete('[/]', '\FundController:doDtrAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);
				});

			});
		});
	}
}
