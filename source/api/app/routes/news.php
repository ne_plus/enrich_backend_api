<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include_once(dirname(__FILE__) . "/baseRouter.php");
include_once(dirname(__FILE__) . "/../controller/NewsController.php");

class NewsRouter extends BaseRouter {
	//protected $app;

    public function __construct($app) {
       $this->app = $app;
    }

	public function createRoutes() {
		$app = $this->app;

    	// other routes, you may divide routes to class methods

		$app->group('/news', function () use ($app) {

			//$app->container['Auth'][] = 'aaaaa';
			$app->map(['GET'], '[/]', '\NewsController:doAction');

			$app->map(['POST', 'PUT', 'DELETE'], '[/]', '\NewsController:doAction')->add(new Slim\Auth\AuthScope($app->getContainer()), Slim\Auth\AuthScope::ADMIN);
		});
	}
}
