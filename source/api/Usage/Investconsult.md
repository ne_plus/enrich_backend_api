## Investconsult API Usage
Please reference the README. To use API.

**Show Investconsult Information**
----
  Returns json data about investconsult information.

* **URL**

  /api/investconsult

* **Method:**

  `GET`
  
*  **URL Params**
   
   **Optional:**
   None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 **Content:** 

`
{
  "ID": 19,
  "YQ": "2016 Q2",
  "INVESTCONSULT": [
    {
      "Title": "福蘭克林1",
      "Link": "http://wwww.google.com"
    },
    {
      "Title": "福蘭克林2",
      "Link": "http://wwww.google.com"
    },
    {
      "Title": "福蘭克林3",
      "Link": "http://wwww.google.com"
    },
    {
      "Title": "福蘭克林4",
      "Link": "http://wwww.google.com"
    }
  ],
  "DATA": [
    {
      "ID": 1,
      "NAME": "成熟股",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "中立",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 2,
      "NAME": "新興股",
      "LAST_SEASON_PERFORMANCE": "0.1%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "加碼",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 3,
      "NAME": "美股",
      "LAST_SEASON_PERFORMANCE": "2.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "加碼",
      "SEASON_2": "加碼",
      "LAST_SEASON_3": "加碼",
      "SEASON_3": "減碼",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 4,
      "NAME": "歐股",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "中立",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 5,
      "NAME": "日股",
      "LAST_SEASON_PERFORMANCE": "10.0%",
      "LAST_SEASON_1": "減碼",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "中立",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 6,
      "NAME": "亞股",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "中立",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 7,
      "NAME": "全球債",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "中立",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 8,
      "NAME": "新興債",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "中立",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 9,
      "NAME": "高收益債",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "中立",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "減碼"
    },
    {
      "ID": 10,
      "NAME": "產業",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "減碼",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 11,
      "NAME": "美元",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "中立",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 12,
      "NAME": "亞洲貨幣",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "中立",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 13,
      "NAME": "原物料貨幣",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "加碼",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "加碼",
      "LAST_SEASON_4": "減碼",
      "SEASON_4": "中立"
    },
    {
      "ID": 14,
      "NAME": "歐洲貨幣",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "中立",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 15,
      "NAME": "其它",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "中立",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    },
    {
      "ID": 16,
      "NAME": "避險現金",
      "LAST_SEASON_PERFORMANCE": "0.0%",
      "LAST_SEASON_1": "中立",
      "SEASON_1": "中立",
      "LAST_SEASON_2": "中立",
      "SEASON_2": "中立",
      "LAST_SEASON_3": "中立",
      "SEASON_3": "中立",
      "LAST_SEASON_4": "中立",
      "SEASON_4": "中立"
    }
  ],
  "lastupdatetime": "2016-05-23 02:57:40"
}
`
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Not Found!!" }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/investconsult",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```