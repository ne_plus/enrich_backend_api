## Market API Usage
Please reference the README. To use API.

**Show Market list by Pages**
----
  Returns json data about market list.

* **URL**

  /api/market/:marketLevel

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `marketLevel =[string]`
   
   **Optional:**
 
   `page=[integer]`, 
   `page_limt=[integer]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 **Content:** 

	```
	{
	  "data": [
	    {
	      "ID": 1,
	      "NAME": "成熟股",
	      "UpMarketLevelID": 1,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 2,
	      "NAME": "新興股",
	      "UpMarketLevelID": 1,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 3,
	      "NAME": "美股",
	      "UpMarketLevelID": 1,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 4,
	      "NAME": "歐股",
	      "UpMarketLevelID": 1,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 5,
	      "NAME": "日股",
	      "UpMarketLevelID": 1,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 6,
	      "NAME": "亞股",
	      "UpMarketLevelID": 1,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 7,
	      "NAME": "全球債",
	      "UpMarketLevelID": 2,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 8,
	      "NAME": "新興債",
	      "UpMarketLevelID": 2,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 9,
	      "NAME": "高收益債",
	      "UpMarketLevelID": 2,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 10,
	      "NAME": "產業",
	      "UpMarketLevelID": 4,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 11,
	      "NAME": "美元",
	      "UpMarketLevelID": 3,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 12,
	      "NAME": "亞洲貨幣",
	      "UpMarketLevelID": 3,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 13,
	      "NAME": "原物料貨幣",
	      "UpMarketLevelID": 3,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 14,
	      "NAME": "歐洲貨幣",
	      "UpMarketLevelID": 3,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 15,
	      "NAME": "其它",
	      "UpMarketLevelID": 4,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    },
	    {
	      "ID": 16,
	      "NAME": "避險現金",
	      "UpMarketLevelID": 4,
	      "createdate": null,
	      "lastupdatetime": "2016-04-20 08:04:27"
	    }
	  ],
	  "page_info": {
	    "total_pages": 1,
	    "page_limt": 20,
	    "current_page": 1
	  }
	}
	```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Not Found!!" }`

* **Sample Call:**

  ```
    $.ajax({
      url: "/api/market/MarketLevelB",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```

**Show Super Market Information**
----
  Returns json data about a single super martket information.

* **URL**

  /api/market/:marketLevel/:id/supermarket

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `marketLevel =[string]`, 
   `id=[integer]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 **Content:** 

	```
	{
	  "ID": 2,
	  "NAME": "債市",
	  "Description": null,
	  "createdate": null,
	  "lastupdatetime": "2016-04-20 08:04:20"
	}
	```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Not Found!! [id]" }`

* **Sample Call:**

  ```
    $.ajax({
      url: "/api/market/MarketLevelB/9/supermarket",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```
  
**Show Market Information**
----
  Returns json data about a single sub martket information.

* **URL**

  /api/market/:marketLevel/:id/submarket

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
 
   `marketLevel =[string]`, 
   `id=[integer]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 **Content:** 
	```
	[
	  {
	    "ID": 32,
	    "NAME": "高收益債",
	    "BenchmarkName": "HYG.US",
	    "UpMarketLevelID": 9,
	    "ECOIndicator_Field": null,
	    "ECO_1": "",
	    "ECO_1_MEMO": "",
	    "ECO_2": "",
	    "ECO_2_MEMO": "",
	    "ECO_3": "",
	    "ECO_3_MEMO": "",
	    "ECO_4": "",
	    "ECO_4_MEMO": "",
	    "ECO_5": "",
	    "ECO_5_MEMO": "",
	    "ECO_6": "",
	    "ECO_6_MEMO": "",
	    "ECO_7": "",
	    "ECO_7_MEMO": "",
	    "ECO_8": "",
	    "ECO_8_MEMO": "",
	    "ECO_9": "",
	    "ECO_9_MEMO": "",
	    "ECO_10": "",
	    "ECO_10_MEMO": "",
	    "createdate": null,
	    "lastupdatetime": "0000-00-00 00:00:00"
	  }
	]
	```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Not Found!! [id]" }`

* **Sample Call:**

  ```
    $.ajax({
      url: "/api/market/MarketLevelB/9/submarket",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**Show Market Benchmark Information**
----
  Returns json data about a martket benchmark information.

* **URL**

  /api/market/:marketLevel/:id/benchmark

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
   None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 **Content:** 
	```
	{
  "data": [
    {
      "MarketID": 3,
      "YMD": "2016-05-27",
      "BenchMark_Value": "23.18000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.17402",
      "EMA_20": "23.18704",
      "EMA_24": "23.18848",
      "EMA_60": "23.13227",
      "EMA_260": "21.82498",
      "RSI_1": "58.33333",
      "MACD_1": "-0.01446",
      "MACD_2": "-0.00600",
      "MACD_3": "-0.00847",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-26",
      "BenchMark_Value": "23.12000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.17293",
      "EMA_20": "23.18778",
      "EMA_24": "23.18922",
      "EMA_60": "23.13065",
      "EMA_260": "21.81452",
      "RSI_1": "51.11111",
      "MACD_1": "-0.01629",
      "MACD_2": "-0.00388",
      "MACD_3": "-0.01241",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-25",
      "BenchMark_Value": "23.16000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.18255",
      "EMA_20": "23.19492",
      "EMA_24": "23.19524",
      "EMA_60": "23.13101",
      "EMA_260": "21.80444",
      "RSI_1": "54.44444",
      "MACD_1": "-0.01269",
      "MACD_2": "-0.00078",
      "MACD_3": "-0.01191",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-24",
      "BenchMark_Value": "23.13000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.18665",
      "EMA_20": "23.19860",
      "EMA_24": "23.19830",
      "EMA_60": "23.13003",
      "EMA_260": "21.79397",
      "RSI_1": "49.42529",
      "MACD_1": "-0.01165",
      "MACD_2": "0.00220",
      "MACD_3": "-0.01385",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-23",
      "BenchMark_Value": "23.16000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.19695",
      "EMA_20": "23.20582",
      "EMA_24": "23.20424",
      "EMA_60": "23.13003",
      "EMA_260": "21.78365",
      "RSI_1": "52.38095",
      "MACD_1": "-0.00729",
      "MACD_2": "0.00566",
      "MACD_3": "-0.01295",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-20",
      "BenchMark_Value": "23.15000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.20367",
      "EMA_20": "23.21064",
      "EMA_24": "23.20809",
      "EMA_60": "23.12901",
      "EMA_260": "21.77302",
      "RSI_1": "54.44444",
      "MACD_1": "-0.00442",
      "MACD_2": "0.00890",
      "MACD_3": "-0.01332",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-19",
      "BenchMark_Value": "23.16000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.21343",
      "EMA_20": "23.21702",
      "EMA_24": "23.21314",
      "EMA_60": "23.12830",
      "EMA_260": "21.76239",
      "RSI_1": "56.38298",
      "MACD_1": "0.00030",
      "MACD_2": "0.01223",
      "MACD_3": "-0.01193",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-18",
      "BenchMark_Value": "23.17000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.22315",
      "EMA_20": "23.22302",
      "EMA_24": "23.21776",
      "EMA_60": "23.12723",
      "EMA_260": "21.75160",
      "RSI_1": "52.04082",
      "MACD_1": "0.00539",
      "MACD_2": "0.01521",
      "MACD_3": "-0.00982",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-17",
      "BenchMark_Value": "23.19000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.23281",
      "EMA_20": "23.22860",
      "EMA_24": "23.22191",
      "EMA_60": "23.12578",
      "EMA_260": "21.74065",
      "RSI_1": "39.17526",
      "MACD_1": "0.01090",
      "MACD_2": "0.01767",
      "MACD_3": "-0.00677",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-16",
      "BenchMark_Value": "23.33000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.24059",
      "EMA_20": "23.23266",
      "EMA_24": "23.22468",
      "EMA_60": "23.12360",
      "EMA_260": "21.72946",
      "RSI_1": "64.77273",
      "MACD_1": "0.01591",
      "MACD_2": "0.01936",
      "MACD_3": "-0.00345",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-13",
      "BenchMark_Value": "23.14000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.22433",
      "EMA_20": "23.22241",
      "EMA_24": "23.21552",
      "EMA_60": "23.11660",
      "EMA_260": "21.71710",
      "RSI_1": "46.37681",
      "MACD_1": "0.00880",
      "MACD_2": "0.02022",
      "MACD_3": "-0.01142",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-12",
      "BenchMark_Value": "23.20000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.23966",
      "EMA_20": "23.23108",
      "EMA_24": "23.22209",
      "EMA_60": "23.11581",
      "EMA_260": "21.70611",
      "RSI_1": "19.11765",
      "MACD_1": "0.01757",
      "MACD_2": "0.02308",
      "MACD_3": "-0.00551",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-11",
      "BenchMark_Value": "23.39000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.24687",
      "EMA_20": "23.23435",
      "EMA_24": "23.22401",
      "EMA_60": "23.11296",
      "EMA_260": "21.69457",
      "RSI_1": "61.22449",
      "MACD_1": "0.02286",
      "MACD_2": "0.02446",
      "MACD_3": "-0.00160",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-10",
      "BenchMark_Value": "23.22000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.22085",
      "EMA_20": "23.21797",
      "EMA_24": "23.20958",
      "EMA_60": "23.10357",
      "EMA_260": "21.68148",
      "RSI_1": "52.50000",
      "MACD_1": "0.01127",
      "MACD_2": "0.02486",
      "MACD_3": "-0.01359",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-09",
      "BenchMark_Value": "23.22000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.22100",
      "EMA_20": "23.21776",
      "EMA_24": "23.20867",
      "EMA_60": "23.09962",
      "EMA_260": "21.66960",
      "RSI_1": "67.39130",
      "MACD_1": "0.01232",
      "MACD_2": "0.02826",
      "MACD_3": "-0.01593",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-06",
      "BenchMark_Value": "23.18000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.22118",
      "EMA_20": "23.21752",
      "EMA_24": "23.20769",
      "EMA_60": "23.09554",
      "EMA_260": "21.65763",
      "RSI_1": "65.11628",
      "MACD_1": "0.01349",
      "MACD_2": "0.03224",
      "MACD_3": "-0.01875",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-05",
      "BenchMark_Value": "23.18000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.22867",
      "EMA_20": "23.22147",
      "EMA_24": "23.21010",
      "EMA_60": "23.09268",
      "EMA_260": "21.64587",
      "RSI_1": "65.11628",
      "MACD_1": "0.01857",
      "MACD_2": "0.03693",
      "MACD_3": "-0.01836",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-04",
      "BenchMark_Value": "23.18000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.23752",
      "EMA_20": "23.22583",
      "EMA_24": "23.21272",
      "EMA_60": "23.08972",
      "EMA_260": "21.63402",
      "RSI_1": "42.00000",
      "MACD_1": "0.02480",
      "MACD_2": "0.04152",
      "MACD_3": "-0.01672",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-03",
      "BenchMark_Value": "23.25000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.24798",
      "EMA_20": "23.23065",
      "EMA_24": "23.21557",
      "EMA_60": "23.08666",
      "EMA_260": "21.62208",
      "RSI_1": "47.05882",
      "MACD_1": "0.03240",
      "MACD_2": "0.04570",
      "MACD_3": "-0.01329",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    },
    {
      "MarketID": 3,
      "YMD": "2016-05-02",
      "BenchMark_Value": "23.30000",
      "BenchMark_Quantity": "0.00000",
      "EMA_12": "23.24761",
      "EMA_20": "23.22861",
      "EMA_24": "23.21258",
      "EMA_60": "23.08112",
      "EMA_260": "21.60951",
      "RSI_1": "63.04348",
      "MACD_1": "0.03502",
      "MACD_2": "0.04902",
      "MACD_3": "-0.01400",
      "createdate": null,
      "lastupdatetime": "2016-06-02 05:51:15"
    }
  ],
  "page_info": {
    "total_pages": 18,
    "page_limit": 20,
    "current_page": 1,
    "total": 358
  }
}
	```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Not Found!! [id]" }`

* **Sample Call:**

  ```
    $.ajax({
      url: "/api/market/MarketLevelC/3/benchmark",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```

**Show ECO Information about this Market**
----
  Returns json data about a martket ECO information.

* **URL**

  /api/market/:marketLevel/:id/ecovalue

* **Method:**

  `GET`
  
*  **URL Params**
	data_all=[integer]
   **Required:**
   None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 **Content:** 
	```
	{
  "ECO_1": {
    "key": "OECDENGDPYoY",
    "memo": "0012. 歐盟GDP年增率(OECD歐洲會員國)",
    "setting": 1,
    "data": [
      {
        "YMD": "2016-04-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "1.93146",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-20 15:30:39"
      },
      {
        "YMD": "2016-01-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "2.01954",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-20 15:30:39"
      },
      {
        "YMD": "2015-10-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "2.27780",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-20 15:30:39"
      },
      {
        "YMD": "2015-07-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "2.29450",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2015-04-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "2.10142",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2015-01-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "1.77321",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2014-10-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "1.58117",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2014-07-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "1.45570",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2014-04-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "1.41785",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2014-01-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "1.72137",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2013-10-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "1.23426",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2013-07-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "0.58988",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2013-04-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "0.26756",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2013-01-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "-0.40757",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2012-10-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "-0.50640",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2012-07-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "-0.36721",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2012-04-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "-0.18478",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2012-01-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "0.14441",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      },
      {
        "YMD": "2011-10-01",
        "ECO_NAME": "OECDENGDPYoY",
        "ECO_VALUE": "1.18003",
        "FROM": "stockAI",
        "createdate": "2016-09-16 08:07:39",
        "lastupdatetime": "2016-09-16 08:07:39"
      }
    ]
  },
  "ECO_4": {
    "key": "HSBCEUPMI",
    "memo": "0023. 歐盟Markit歐元區製造業採購經理人指數",
    "setting": 1,
    "data": []
  },
  "ECO_6": {
    "key": "oecdEuCPI",
    "memo": "0019. 歐盟消費者物價指數(OECD歐洲會員國)",
    "setting": 1,
    "data": []
  }
}
	```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Not Found!! [id]" }`

* **Sample Call:**

  ```
    $.ajax({
      url: "/api/market/MarketLevelC/3/ecovalue?data_all=0",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```

**Show ECO Information**
----
  Returns json data about a ECO information.

* **URL**

  /api/market/:marketLevel/eco/:ECOid

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
   None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 **Content:** 
	```
	{
  "data": [
    {
      "YMD": "2016-09-15",
      "ECO_NAME": "chIRDC",
      "ECO_VALUE": "-0.75000",
      "FROM": "stockAI",
      "createdate": "2016-09-16 08:07:40",
      "lastupdatetime": "2016-09-16 08:07:40"
    },
    {
      "YMD": "2016-06-16",
      "ECO_NAME": "chIRDC",
      "ECO_VALUE": "-0.75000",
      "FROM": "stockAI",
      "createdate": "2016-09-16 08:07:40",
      "lastupdatetime": "2016-09-16 08:07:40"
    },
    {
      "YMD": "2016-03-17",
      "ECO_NAME": "chIRDC",
      "ECO_VALUE": "-0.75000",
      "FROM": "stockAI",
      "createdate": "2016-09-16 08:07:40",
      "lastupdatetime": "2016-09-16 08:07:40"
    },
    {
      "YMD": "2015-12-10",
      "ECO_NAME": "chIRDC",
      "ECO_VALUE": "-0.75000",
      "FROM": "stockAI",
      "createdate": "2016-09-16 08:07:40",
      "lastupdatetime": "2016-09-16 08:07:40"
    },
    {
      "YMD": "2015-09-17",
      "ECO_NAME": "chIRDC",
      "ECO_VALUE": "-0.75000",
      "FROM": "stockAI",
      "createdate": "2016-09-16 08:07:40",
      "lastupdatetime": "2016-09-16 08:07:40"
    },
    {
      "YMD": "2015-06-18",
      "ECO_NAME": "chIRDC",
      "ECO_VALUE": "-0.75000",
      "FROM": "stockAI",
      "createdate": "2016-09-16 08:07:40",
      "lastupdatetime": "2016-09-16 08:07:40"
    },
    {
      "YMD": "2015-03-19",
      "ECO_NAME": "chIRDC",
      "ECO_VALUE": "-0.75000",
      "FROM": "stockAI",
      "createdate": "2016-09-16 08:07:40",
      "lastupdatetime": "2016-09-16 08:07:40"
    },
    {
      "YMD": "2015-01-15",
      "ECO_NAME": "chIRDC",
      "ECO_VALUE": "-0.75000",
      "FROM": "stockAI",
      "createdate": "2016-09-16 08:07:40",
      "lastupdatetime": "2016-09-16 08:07:40"
    },
    {
      "YMD": "2014-12-18",
      "ECO_NAME": "chIRDC",
      "ECO_VALUE": "-0.25000",
      "FROM": "stockAI",
      "createdate": "2016-09-16 08:07:40",
      "lastupdatetime": "2016-09-16 08:07:40"
    }
  ],
  "page_info": {
    "total_pages": 1,
    "page_limit": 20,
    "current_page": 1,
    "total": 9
  }
}
	```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Not Found!! [id]" }`

* **Sample Call:**

  ```
    $.ajax({
      url: "/api/market/MarketLevelC/eco/chIRDC",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```