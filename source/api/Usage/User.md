## User API Usage
Please reference the README. To use API.

**Show User list by Pages**
----
  Returns json data about users.

* **URL**

  /api/user

* **Method:**

  `GET`
  
*  **URL Params**
   
   **Optional:**
 
   `page=[integer]`, 
   `page_limt=[integer]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 **Content:** 

`
{ id : 12, name : "Michael Bloom" }
`
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Not Found!!" }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/user",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```

**Show User Information**
----
  Returns json data about a single user.

* **URL**

  /api/user//:id

* **Method:**

  `GET`
  
*  **URL Params**

   **Required:**
   
   `id=[integer]`

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 **Content:** 

	```
	{
  "USER_ID": 2,
  "EMAIL": "cartor@gmail.com",
  "NAME": "Cartor",
  "BUDGET": null,
  "AGE": null,
  "GANDER": null,
  "ASSETS": null,
  "WORKS": null,
  "INFOSOURCE": null,
  "INVESTMENT": null,
  "OBJECTIVE": null,
  "PROFIT": null,
  "LOSSSITUATION": null,
  "DURING": null,
  "RiskTYPE": null,
  "ROLE": 2,
  "createdate": null,
  "lastupdatetime": "2016-05-22 21:47:00"
}
	```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Not Found!! [01011]" }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/user/2",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```
  
**Update User Information**
----
  Update user data with json data.

* **URL**

  /api/user/:id

* **Method:**

  `POST`
  
* **URL Params**

  **Required:**
  None

* **Data Params**

	```
	{
  "USER_ID": 2,
  "EMAIL": "cartor@gmail.com",
  "NAME": "Cartor",
  "BUDGET": null,
  "AGE": null,
  "GANDER": null,
  "ASSETS": null,
  "WORKS": null,
  "INFOSOURCE": null,
  "INVESTMENT": null,
  "OBJECTIVE": null,
  "PROFIT": null,
  "LOSSSITUATION": null,
  "DURING": null,
  "RiskTYPE": null,
}
	```

* **Success Response:**

  * **Code:** 200 **Content:** 
  
	```
	{
  "USER_ID": 2,
  "EMAIL": "cartor@gmail.com",
  "NAME": "Cartor",
  "BUDGET": null,
  "AGE": null,
  "GANDER": null,
  "ASSETS": null,
  "WORKS": null,
  "INFOSOURCE": null,
  "INVESTMENT": null,
  "OBJECTIVE": null,
  "PROFIT": null,
  "LOSSSITUATION": null,
  "DURING": null,
  "RiskTYPE": null,
  "ROLE": 2,
  "createdate": null,
  "lastupdatetime": "2016-05-22 21:47:00"
}
	```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Failed!! [0101]" }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/user/2",
      data: JSON.stringify($data),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```
**User SignUp**
----
  User sign up a new account.

* **URL**

  /api/user/signUp

* **Method:**

  `POST`
  
* **URL Params**

  **Required:**
  None
   

* **Data Params**
  
  ```
  {
  "EMAIL": "cartor@gmail.com",
  "PASSWORD": "Cartor",
  "BUDGET": null,
  "AGE": null,
  "GANDER": null,
  "ASSETS": null,
  "WORKS": null,
  "INFOSOURCE": null,
  "INVESTMENT": null,
  "OBJECTIVE": null,
  "PROFIT": null,
  "LOSSSITUATION": null,
  "DURING": null,
  "RiskTYPE": null,
}
  ```
  **Required:**
   EMAIL, PASSWORD

* **Success Response:**

  * **Code:** 200 **Content:** 

	```
	{
  "USER_ID": 2,
  "EMAIL": "cartor@gmail.com",
  "NAME": "Cartor",
  "BUDGET": null,
  "AGE": null,
  "GANDER": null,
  "ASSETS": null,
  "WORKS": null,
  "INFOSOURCE": null,
  "INVESTMENT": null,
  "OBJECTIVE": null,
  "PROFIT": null,
  "LOSSSITUATION": null,
  "DURING": null,
  "RiskTYPE": null,
  "ROLE": 2,
  "createdate": null,
  "lastupdatetime": "2016-05-22 21:47:00"
}
	```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Failed!! [0101]" }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/user/signUp",
      data: JSON.stringify($data),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```

**User Login**
----
  User login.

* **URL**

  /api/user/login

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
   None
   

* **Data Params**

  **Required:**
  `email=[string]`
  `password=[string]`
  
  `getToken=[boolean]`

* **Success Response:**

  * **Code:** 200 **Content:** 

	```
	{
  "USER_ID": 1,
  "EMAIL": "aa@aa.com",
  "NAME": "aaaaa",
  "BUDGET": null,
  "AGE": null,
  "GANDER": null,
  "ASSETS": null,
  "WORKS": null,
  "INFOSOURCE": null,
  "INVESTMENT": null,
  "OBJECTIVE": null,
  "PROFIT": null,
  "LOSSSITUATION": null,
  "DURING": null,
  "RiskTYPE": null,
  "ROLE": 2,
  "createdate": null,
  "lastupdatetime": "2016-05-10 14:37:55"
}
	```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Failed!! [0101]" }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/user/login",
      data: JSON.stringify($data),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```

**User Refresh Token**
----
  User refresh access token.

* **URL**

  /api/user/refreshToken

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
   None
   

* **Data Params**
  
  **Required:**
  `email=[string]`
  `password=[string]`
  `access_token=[string]`

* **Success Response:**

  * **Code:** 200 **Content:** 

	```
	{
  "access_token": "704e547361496550556937434146786c636d725656525242583953504534",
  "access_token_expire": "2016-08-28 09:18:18"
}
	```
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Failed!! [0101]" }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/user/refreshToken",
      data: JSON.stringify($data),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```

**User Reset Password**
----
  User reset password.

* **URL**

  /api/user/resetPassword

* **Method:**

  `POST`
  
*  **URL Params**

   **Required:**
   None
   

* **Data Params**
  
  **Required:**
  `email=[string]`
  `old_password=[string]`
  `new_password=[string]`

* **Success Response:**

  * **Code:** 200 **Content:** 

	```
	{
  "msg": "Susses, password changed."
}
	```
 
* **Error Response:**

  * **Code:** 400 NOT FOUND <br />
    **Content:** `{ "error": "Failed!! [0101]" }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/user/resetPassword",
      data: JSON.stringify($data),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      type : "POST",
      success : function(r) {
        console.log(r);
      }
    });
  ```