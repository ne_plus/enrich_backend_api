## News API Usage
Please reference the README. To use API.

**Show All News Information**
----
  Returns json data about all news information.

* **URL**

  /api/news

* **Method:**

  `GET`
  
*  **URL Params**
   
   **Optional:**
   None

* **Data Params**

  None

* **Success Response:**

  * **Code:** 200 **Content:** 

`
{
  "data": [
    {
      "ID": 1,
      "YMD": "2016-09-15",
      "TYPE": 0,
      "TITLE": "aaaaa",
      "CONTENT": "dasdasadasdasdsa",
      "URL": "http://google.com",
      "isOpen": 1,
      "createdate": null,
      "lastupdatetime": "2016-09-17 19:50:28"
    }
  ],
  "page_info": {
    "total_pages": 1,
    "page_limit": 20,
    "current_page": 1,
    "total": 1
  }
}
`
 
* **Error Response:**

  * **Code:** 404 NOT FOUND <br />
    **Content:** `{ "error": "Not Found!!" }`

* **Sample Call:**

  ```javascript
    $.ajax({
      url: "/api/news",
      dataType: "json",
      type : "GET",
      success : function(r) {
        console.log(r);
      }
    });
  ```