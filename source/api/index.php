<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

include_once(dirname(__FILE__) . "/common/mysql_config.php");
include_once(dirname(__FILE__) . "/app/classes/Auth/Auth.php");
include_once(dirname(__FILE__) . "/common/Jsonp.php");

#require '../app/routes/session.php';

//require './app/routes/admin.php';

require './app/routes/import.php';

require './app/routes/user.php';
require './app/routes/fund.php';
require './app/routes/market.php';
require './app/routes/investconsult.php';
require './app/routes/exchangerate.php';
require './app/routes/news.php';
require './app/routes/misc.php';
require './app/routes/quiz.php';

$config = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];

$routerClasses = array(
	'UserRouter',
	'FundRouter',
	'MarketLevelRouter',
	'InvestconsultRouter',
	'ExchangeRateRouter',
	'ImportRouter',
	'NewsRouter',
	'MiscRouter',
	'QuizRouter',
	);

$config['RouterClasses'] = $routerClasses;

$c = new \Slim\Container($config);
$app = new \Slim\App($c);

$app->get('/', function($request, $response) use ($app) {
	# Or we can send
	$response = $response->withStatus(403);
	$response = $response->withJson(array('error' => "Permission Denied. [".$_SERVER['REMOTE_ADDR']."]"));

	return $response;
});

$app->group('/api', function () use ($app) {
	$app->get('[/]', function($request, $response) use ($app) {
		# Or we can send
		$response = $response->withStatus(403);
		$response = $response->withJson(array('error' => "Permission Denied. [".$_SERVER['REMOTE_ADDR']."]"));

		return $response;
	});

	foreach ($app->getContainer()->RouterClasses as $class) {
		$object = new $class($app);
		$object->createRoutes();
	}
})->add(new Slim\Auth\User($app->getContainer()));

$app->add(new Slim\Extras\Jsonp());

$app->run();
