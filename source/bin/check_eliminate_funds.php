<?php
require_once(dirname(__FILE__) . "/../vendor/autoload.php");

include_once(dirname(__FILE__) . "/../api/common/config.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/Fund.php");

include_once(dirname(__FILE__) . "/../api/common/mysql_config.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/UpdateDataTime.php");


class FundClass {
	protected $_dao;

	public function __construct() {
	    $this->_dao = new Fund();
	}

	public function process() {
	    $data = $this->_dao->get(array(), array(), 20000);
	    $count = 0;
	    
	    foreach ($data as $value) {
	    	unset($value['eliminate']);
	    	
	    	if($value['END_DATE'] != '0000-00-00') {
	    		$today = date("Y-m-d");

	    		$value['eliminate'] = (strtotime($value['END_DATE']) < strtotime($today)) ? 0 : 1;
	    	}
	    	else {
	    		$value['eliminate'] = 1;
	    	}
	        
	        if ($this->_dao->save($value)) {
	            $count++;
	        }
        }
        return $count;
	}
}

$_fund = new FundClass();
$id = $_fund->process();

var_dump($id);
