<?php
require_once(dirname(__FILE__) . "/../vendor/autoload.php");

include_once(dirname(__FILE__) . "/../api/common/config.php");
include_once(dirname(__FILE__) . "/../api/common/dataProcessUtilities.php");

include_once(dirname(__FILE__) . "/../api/common/mysql_config.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/UpdateDataTime.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/MarketECO.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/ECOList.php");

class CsvImporter { 
    private $fp; 
    private $parse_header; 
    private $header; 
    private $delimiter; 
    private $length; 
    //-------------------------------------------------------------------- 
    function __construct($file_name, $parse_header=false, $delimiter="\t", $length=8000) 
    { 
        $this->fp = fopen($file_name, "r"); 
        $this->parse_header = $parse_header; 
        $this->delimiter = $delimiter; 
        $this->length = $length; 
        //$this->lines = $lines; 

        if ($this->parse_header) 
        { 
           $this->header = fgetcsv($this->fp, $this->length, $this->delimiter); 
        } 

    } 
    //-------------------------------------------------------------------- 
    function __destruct() 
    { 
        if ($this->fp) 
        { 
            fclose($this->fp); 
        } 
    } 
    //-------------------------------------------------------------------- 
    function get($max_lines=0) 
    { 
        //if $max_lines is set to 0, then get all the data 

        $data = array(); 

        if ($max_lines > 0) 
            $line_count = 0; 
        else 
            $line_count = -1; // so loop limit is ignored 

        while ($line_count < $max_lines && ($row = fgetcsv($this->fp, $this->length, $this->delimiter)) !== FALSE) 
        { 
            if ($this->parse_header) 
            { 
                foreach ($this->header as $i => $heading_i) 
                { 
                    $row_new[$heading_i] = $row[$i]; 
                } 
                $data[] = $row_new; 
            } 
            else 
            { 
                $data[] = $row; 
            } 

            if ($max_lines > 0) 
                $line_count++; 
        } 
        return $data; 
    } 
    //-------------------------------------------------------------------- 

} 

class StockAIData {
	protected $keys;
	protected $keys_use;

	protected $lists;

	protected $_dao;

    public function __construct($listFileName = "stockAI.list", $keyFileName = "stockAI.key") {
    	//$this->app = $app;
    	$this->keys = file(COMMON_ROOT . "/" . $keyFileName);
		$this->keys_use = array();

		if (0) {
			//$this->lists = file(COMMON_ROOT . "/" . $listFileName);
			$json_str = file_get_contents(COMMON_ROOT . "/" . $listFileName);

			$this->lists = json_decode($json_str, true);
		}
		else {
			$_ECO_db = new ECOList();
			$this->lists = $_ECO_db->get();
		}

		$this->_dao = new MarketECO();
    }

    function getECOGroup() {
    	$data = array();
    	$groups = $this->_dao->grouping("ECO_NAME", Array("ECO_NAME"));
		foreach ($groups as $group) { 
        	//print_r($group["ECO_NAME"]);
        	$data[] = $group["ECO_NAME"];
    	}
    	return $data;
    }

    function exportToJson($records) {
	    file_put_contents(COMMON_ROOT . "/". ECO_LIST_JSON, json_encode($records));
	}

    function readCSVFile($key, $filePath) {
    	$importer = new CsvImporter($filePath, true, ","); 
		$data = $importer->get(2000);

		//print_r($data); 

		foreach ($data as $value) {
		    //print_r($value);
		    $ecoData = array(
	    		'YMD' => $value['Date'],
	    		'ECO_NAME' => $key,
	    		'ECO_VALUE' => $value['Value'],
	    		'FROM' => 'stockAI',
	    	);

	    	//var_dump($ecoData);
	    	$this->_dao->save($ecoData);
		}
    }

	function downloadFile($url, $filepath) {
		//return (filesize($filepath) > 0)? true : false;

		$ret = false;

		$fp = fopen($filepath, 'w+');
		$ch = curl_init($url);

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, false);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		//curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_exec($ch);

		if (!curl_errno($ch)) {
			$info = curl_getinfo($ch);
			switch ($info['http_code']) {
				case 200:  # OK
					$ret = true;
					echo 'Took ', $info['total_time'], ' seconds to send a request to ', $info['url'], "\n";
				  	break;
				default:
				  	echo 'Unexpected HTTP code: ', $info['http_code'], "\n";
			}
		}

		curl_close($ch);
		fclose($fp);

		return (filesize($filepath) > 0 && $ret == true) ? true : false;
	}

	function getKey() {
		foreach ($this->keys as $key) {
			if(!isset($this->keys_use[$key])) {
				$this->keys_use[$key] = 0;
			}
			if (intval($this->keys_use[$key]) < 10) {
				$this->keys_use[$key] += 1;

				return trim($key);
			}
		}

		return "";
	}

	function saveStockAIJson() {
		$objs = $this->lists;
		foreach ($objs as $num => $obj) {
		    echo "obj #{$num} : " . htmlspecialchars($obj['code']) . "\n";
		    $_ECO_db = new ECOList();

		    $_ECO_db->save(array(
		    	'CODE' => trim($obj['code']),
		    	'NAME' => trim($obj['name']),
		    	'FORM' => 'stockAI',
		    ));
		}
	}

	function processStockAIList() {
		$_folder = STOCKAI_FILE_PATH . "/" . date("Ymd");

		if (!is_dir(STOCKAI_FILE_PATH)) {
			mkdir(STOCKAI_FILE_PATH);         
		} 

		if (!is_dir($_folder)) {
			mkdir($_folder);         
		}

		$_objs = $this->lists;
		$res = array(
			"success" => 0,
			"failed" => 0,
		);

		// Loop through our array, show HTML source as HTML source; and line numbers too.
		foreach ($_objs as $line_num => $obj) {
		    $_key = trim($obj['CODE']);
		    $url = sprintf(STOCKAI_URL, $_key, $this->getKey());
		    $fileName = $_key . ".csv";
		    $filePath = $_folder . "/" . $fileName;

		    echo "[" . $filePath . "] " . $url . "\n";

		    // do download
		    if ($this->downloadFile($url, $filePath)) {
		    	// do unzip and insert db
		    	//echo "it's good.\n";
		    	$this->readCSVFile($_key, $filePath);
		    	$res['success']++;
		    }
		    else {
		    	//echo "download failed.\n";
		    	$res['failed']++;
		    }
		}

		return $res;
	}

	function runUpdate() {
		return $this->processStockAIList();
	}
}

$_dao = new UpdateDataTime();
$id = $_dao->start_update(UpdateDataTime::STOCKAI_DATA);

$_stockAI = new StockAIData();
$res = $_stockAI->runUpdate();

var_dump($res);

$group = $_stockAI->getECOGroup();
//$_stockAI->exportToJson($group);
//var_dump($group);

if ($id != false) {
	$_dao->finish_update($id, UpdateDataTime::STOCKAI_DATA,  json_encode($res));
}

