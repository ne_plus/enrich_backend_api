<?php
require_once(dirname(__FILE__) . "/../vendor/autoload.php");

include_once(dirname(__FILE__) . "/../api/common/config.php");
include_once(dirname(__FILE__) . "/../api/common/dataProcessUtilities.php");

include_once(dirname(__FILE__) . "/../api/common/mysql_config.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/UpdateDataTime.php");
include_once(dirname(__FILE__) . "/../api/app/controller/FundController.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/Fund.php");

class FundClass {
	protected $_dao;

	public function __construct() {
	    $this->_dao = new Fund();
	}

	public function process() {
	    $data = $this->_dao->get(array(), array(), 20000);
	    $count = 0;
	    
	    foreach ($data as $value) {
	    	unset($value['eliminate']);
	    	
	    	if($value['END_DATE'] != '0000-00-00') {
	    		$today = date("Y-m-d");

	    		$value['eliminate'] = (strtotime($value['END_DATE']) < strtotime($today)) ? 0 : 1;
	    	}
	    	else {
	    		$value['eliminate'] = 1;
	    	}
	        
	        if ($this->_dao->save($value)) {
	            $count++;
	        }
        }
        return $count;
	}
}

function getZipFile($path) {
	foreach (glob($path . "/*.zip") as $filename) {
		$extractDir = $path . "/" . basename($filename, ".zip");
	    //echo "$filename size " . filesize($filename) . "\n";
	    //echo $extractDir . "\n";

	    $zip = new ZipArchive;
		if ($zip->open($filename) === TRUE) {
		    $zip->extractTo($extractDir);
		    $zip->close();
		    echo "extract ok\n";

		    $_dataParser = new dataProcessUtilities();
		    $_dataParser->getDataFile($extractDir);
		    dataProcessUtilities::mvZipFile($filename);
		    dataProcessUtilities::recursiveRemoveDirectory($extractDir);
		} else {
		    echo "extract failed\n";
		}
	}
}

$_dao = new UpdateDataTime();
$id = $_dao->start_update(UpdateDataTime::INFOTIME_DATA);

getZipFile(DATA_FILE_PATH);


$_fund = new FundClass();
$_fund->process();

$date = new DateTime();

if (intval($date->format('H')) >= 8 && intval($date->format('H')) <= 24) {
	$_fundConttroller = new FundController();
	$data = $_fundConttroller->getCombination();
	$_fundConttroller->exportToJson($data);
}

if ($id != false) {
	$_dao->finish_update($id, UpdateDataTime::INFOTIME_DATA, '');
}
