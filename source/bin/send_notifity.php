<?php
require_once(dirname(__FILE__) . "/../vendor/autoload.php");

include_once(dirname(__FILE__) . "/../api/common/config.php");
include_once(dirname(__FILE__) . "/../api/common/mysql_config.php");
include_once(dirname(__FILE__) . "/../api/common/pushNotification/androidAPN.php");
include_once(dirname(__FILE__) . "/../api/common/pushNotification/iOSAPN.php");

include_once(dirname(__FILE__) . "/../api/app/classes/DAO/Market.php");
include_once(dirname(__FILE__) . "/../api/app/classes/MarketInfo.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/MarketAlert.php");

include_once(dirname(__FILE__) . "/../api/app/classes/DAO/Fund.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/UserWatchFunds.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/UserFundsRecord.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/UserDeviceInfo.php");

include_once(dirname(__FILE__) . "/../api/app/classes/DAO/PushNotification_Log.php");

function getFundListByMarketID($marketID) {
	$market = new MarketInfo('MarketLevelC', $marketID);

	$subMarkets = $market->getSubMarketList();
    $subMarketIDs = array_column($subMarkets, 'ID');

	$_fundDAO = new Fund();

    $conditions[] = DAO::setCondition('MARKET_ID', $subMarketIDs, 'IN');
    $conditions[] = DAO::setCondition('eliminate', 1, '=');

    $funds = $_fundDAO->get($conditions);
    if (count($funds) == 0) {
    	return array();
    }

    return array_column($funds, 'FUN_CODE');
}

function getUserListByFundID($FundIDs) {
	$_userFundsRecord = new UserFundsRecord();
    $_userWatchFunds = new UserWatchFunds();

    $conditions[] = DAO::setCondition('FUN_CODE', $FundIDs, 'IN');

    $_user1 = $_userFundsRecord->get($conditions);
    $_user2 = $_userWatchFunds->get($conditions);

    if (!empty($_user1)) {
    	$_user1 = array_column($_user1, 'USER_ID');
    }
    if (!empty($_user2)) {
    	$_user2 = array_column($_user2, 'USER_ID');
    }

    return array_unique(array_merge($_user1, $_user2));
}

function changeStatus($_dao, $_data, $_status) {
	$_settingStatus = array(
		'MarketID' => $_data['MarketID'],
		'YMD' => $_data['YMD'],
		'Notifity' => $_status,
	);
	$_dao->save($_settingStatus);
}

function getDeviceInfo($userIDs) {
	$_dao = new UserDeviceInfo();
	$ret = array();

	$orderBy[] = DAO::setOrderBy('lastlogindate', 'DESC');

	foreach ($userIDs as $_userID) {
		$conditions = array();

		$conditions[] = DAO::setCondition('ENABLE', 1);
        $conditions[] = DAO::setCondition('USER_ID', $_userID);

        $_data = $_dao->getOne($conditions, $orderBy);
        if (!empty($_data)) {
        	$ret[] = $_data;
        }
	}
	return $ret;
}

function _setMessageFormat($_data) {
	$format = '[大市警訊通知] 日期 : %s, 經由DFund系統計算, 針對[%s]大市發出推薦訊息如下 : EMA [%s], RSI [%s], 布林通道 [%s]. 快點使用App獲得更多資訊. 謝謝.';

	$market = new MarketInfo('MarketLevelC', $_data['MarketID']);
	$arr = array(
		$_data['YMD'],
		$market->marketInfo['NAME'],
		$_data['EMA'] == 1 ? '買入' : $_data['EMA'] == 2 ? '賣出' : '中立',
		$_data['RSI'] == 1 ? '買入' : $_data['RSI'] == 2 ? '賣出' : '中立',
		$_data['BBands'] == 1 ? '買入' : $_data['BBands'] == 2 ? '賣出' : '中立',
	);

	return vsprintf($format, $arr);
}

function _Send_PushNotification($_data, $_msg) {
	$ret = false;
	$device_token = $_data['DEVICE_ID'];
	$device_type = $_data['DEIVCE_TYPE'];

	if ($device_type == UserDeviceInfo::iOS) {
		$apns = new ApplePNs(APN_CERT);
		$apns->set_pass(PEMpassphrase);
		$ret = $apns->pushNotification($device_token , $_msg);
	}
	else if ($device_type == UserDeviceInfo::Android) {
		$gcm = new AndroidPNs(GOOGLE_API_KEY);
		$ret = $gcm->pushNotification($device_token , $_msg);
	}

	return $ret;
}

$alertDao = new MarketAlert();
$pushNotificationLogDao = new PushNotification_Log();

$count = 0;

do {
	$_conditions[] = DAO::setCondition('Notifity', 1);
	$_data = $alertDao->getOne($_conditions);

	if (empty($_data)) {
		break;
	}
	$_msg = _setMessageFormat($_data);

	changeStatus($alertDao, $_data, 2);
    $fundIDs = getFundListByMarketID($_data['MarketID']);

    $userIDs = getUserListByFundID($fundIDs);

    $devicesInfo = getDeviceInfo($userIDs);

    foreach ($devicesInfo as $device) {
    	$ret = _Send_PushNotification($device, $_msg);

    	$_log = array(
    		'DEVICE_ID' => $device['DEVICE_ID'],
    		'DEIVCE_TYPE' => $device['DEIVCE_TYPE'],
    		'MSG' => $_msg,
    		'RET' => $ret,
    	);
    	$pushNotificationLogDao->save($_log);
    }

	changeStatus($alertDao, $_data, 3);
	
	$count++;
} while ($count < 10);
