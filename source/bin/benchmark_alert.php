<?php
require_once(dirname(__FILE__) . "/../vendor/autoload.php");

include_once(dirname(__FILE__) . "/../api/common/config.php");

include_once(dirname(__FILE__) . "/../api/common/mysql_config.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/Market.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/MarketBenchMark.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/MarketAlert.php");

$_marketDAO = new Market('MarketLevelC');
$data = $_marketDAO->getData(array(), array(), array());

$benchmarkDao = new MarketBenchMark();
$alertDao = new MarketAlert();

$YMD = date("Y-m-d");
#$YMD = '2017-01-05';

$startDate = new DateTime( $YMD );
$endDate = new DateTime( $YMD );

if (count($argv) > 0) {
	if (!empty($argv[1])) $startDate = new DateTime( $argv[1] );
	if (!empty($argv[2])) $endDate = new DateTime( $argv[2] );
}

do {
	$_YMD = $startDate->format("Y-m-d");
	echo $_YMD , PHP_EOL;
	foreach ($data as $market) {
		echo "try to get market id : " . strval($market['ID']) ." benchmark alert\n";

		$_data = $benchmarkDao->getBenchmarkAlertData($market['ID'], $_YMD);

		$ret = array(
			'MarketID' => $market['ID'],
			'YMD' => $_YMD,
			'EMA' => 0,
			'RSI' => 0,
			'BBands' => 0,
		);

		//var_dump($_YMD);
		//var_dump($_data[0]);

		if (count($_data) > 0) {
			if ($_data[0]['YMD'] == $_YMD) {
				$ret['EMA'] = $benchmarkDao->EMA_Alert($_data, $_YMD);
				$ret['RSI'] = $benchmarkDao->RSI_Alert($_data);
				$ret['BBands'] = $benchmarkDao->BBands_Alert($_data);
			}
		}
		$alertDao->save($ret);
		#var_dump($ret);

		#echo "rebiuld market id : " . strval($market['ID']) . " finished." . strval($totalRes) . "\n";
	}

	$startDate->modify("+1 day");
} while ($startDate <= $endDate);
