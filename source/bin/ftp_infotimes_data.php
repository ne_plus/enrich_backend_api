<?php
require_once(dirname(__FILE__) . "/../vendor/autoload.php");

include_once(dirname(__FILE__) . "/../api/common/config.php");
include_once(dirname(__FILE__) . "/../api/common/mysql_config.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/UpdateDataTime.php");

function getRawDataFTP($host, $user, $pass) {
	try {
		$ftp = new \FtpClient\FtpClient();
		$ftp->connect($host);
		$ftp->login($user, $pass);
		// Turns passive mode on or off
		$ftp->pasv(true);
		$path = $ftp->pwd();
		//$path = "/20160617/";

		// count only the "files" in the current directory
		$total_file = $ftp->count($path, 'file');
		echo "will get $total_file download items.\n";

		// scan the current directory and returns the details of each item
		$items = $ftp->scanDir($path);
		//var_dump($items);

		foreach ($items as $item) {
			$ext = (new SplFileInfo($item['name']))->getExtension();
			$filename = basename($item['name'], "." . $ext);
			$filePath = $path . $item['name'];
			if (strcasecmp($ext, "zip")) {
				continue;
			}

			$tmpFilePath = DATA_FILE_PATH . "/" . $filename . "." . strtolower($ext) .".tmp";
			$size = $ftp->size($filePath);
			echo "Downloading [" . $item['name'] . "] ($size)\n";
			if ($ftp->get($tmpFilePath, $filePath, FTP_BINARY)) {
				$newFilePath = DATA_FILE_PATH . "/" . $filename . "." . strtolower($ext);
				rename($tmpFilePath, $newFilePath);
				echo "Downloaded [$newFilePath]\n";
			}
			else {
				echo "Download [" . $item['name'] . "] failed\n";
			}
		}

		$ftp->close();
	} catch (FtpException $e) {
		echo 'Error: ', $e->getMessage();
	}
}

$_dao = new UpdateDataTime();
$id = $_dao->start_update(UpdateDataTime::FTP);

getRawDataFTP(FTP_HOST, FTP_USER, FTP_PASS);

if ($id != false) {
	$_dao->finish_update($id, UpdateDataTime::FTP, '');
}
