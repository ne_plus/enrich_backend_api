<?php
require_once(dirname(__FILE__) . "/../vendor/autoload.php");

include_once(dirname(__FILE__) . "/../api/common/config.php");

include_once(dirname(__FILE__) . "/../api/common/mysql_config.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/Market.php");
include_once(dirname(__FILE__) . "/../api/app/classes/DAO/MarketBenchMark.php");


$_marketDAO = new Market('MarketLevelC');
$data = $_marketDAO->getData(array(), array(), array());

$benchmarkDao = new MarketBenchMark();
$dateInfo = array();

$startYear = intval(date("Y")) - 10;
$processYear = intval(date("Y")) + 1;
$modifyYear = 3;

if (count($argv) > 0) {
	if (!empty($argv[1])) $startYear = intval($argv[1]);
}

foreach ($data as $market) {
	$totalRes = 0;
	$currentYear = $startYear;

	echo "try to rebiuld market id : " . strval($market['ID']) ."\n";
	while ( $currentYear < $processYear ) {
		$date = new DateTime(strval($currentYear).'-01-01');
		$dateInfo['startDate'] = $date->format('Y-m-d');
		$date->modify('+' . strval($modifyYear) . ' year');
		$dateInfo['endDate'] = $date->format('Y-m-d');

		echo "process date " . $dateInfo['startDate'] ." - " . $dateInfo['endDate'] . "\n";

		$res = $benchmarkDao->rebuildBenchmark($market['ID'], $dateInfo);
		$totalRes += $res;

		$currentYear+=$modifyYear;
	}

	echo "rebiuld market id : " . strval($market['ID']) . " finished." . strval($totalRes) . "\n";
}