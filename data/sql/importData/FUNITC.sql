CREATE TABLE `FUNITC` (
  `ITC_CODE` varchar(5) NOT NULL DEFAULT '',
  `NM_C` varchar(50) DEFAULT NULL,
  `PRE_NM` varchar(10) DEFAULT NULL,
  `GEL_NM` varchar(10) DEFAULT NULL,
  `ZIP_CODE` varchar(5) DEFAULT NULL,
  `ADDRESS` varchar(80) DEFAULT NULL,
  `TEL` varchar(15) DEFAULT NULL,
  `FAX` varchar(15) DEFAULT NULL,
  `URL` varchar(70) DEFAULT NULL,
  `END_DATE` varchar(8) DEFAULT NULL,
  `MEMO` text,
  `CT_CODE` varchar(5) DEFAULT NULL,
  `SITE_YMD` varchar(8) DEFAULT NULL,
  `SICE_YMD` varchar(8) DEFAULT NULL,
  `SBSITE_YMD` varchar(8) DEFAULT NULL,
  `SBSICE_YMD` varchar(8) DEFAULT NULL,
  `OFUN_YMD` varchar(8) DEFAULT NULL,
  `FUTURE_YMD` varchar(8) DEFAULT NULL,
  PRIMARY KEY (`ITC_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;