# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 192.168.99.100 (MySQL 5.5.47-0ubuntu0.14.04.1)
# Database: enrich
# Generation Time: 2016-06-20 05:01:21 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table FUBIDM
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUBIDM`;

CREATE TABLE `FUBIDM` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `IDX_CODE` varchar(8) NOT NULL DEFAULT '',
  `YM` varchar(6) NOT NULL DEFAULT '',
  `DATA_VALUE` decimal(7,2) DEFAULT NULL,
  `FUND_TYPE` tinyint(4) NOT NULL DEFAULT '1',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`IDX_CODE`,`YM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNARE
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNARE`;

CREATE TABLE `FUNARE` (
  `ARE_CODE` varchar(4) NOT NULL DEFAULT '',
  `ARE_DESC` varchar(20) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ARE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNBAS
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNBAS`;

CREATE TABLE `FUNBAS` (
  `FUN_CODE` varchar(7) NOT NULL DEFAULT '',
  `NM_C` varchar(16) DEFAULT NULL,
  `NM_C_F` varchar(60) DEFAULT NULL,
  `NM_E` varchar(60) DEFAULT NULL,
  `START_DATE` date DEFAULT NULL,
  `NAV_E_DATE` date DEFAULT NULL,
  `FUN_MARK` varchar(1) DEFAULT NULL,
  `FUN_REG` varchar(15) DEFAULT NULL,
  `CUR_NM` varchar(6) DEFAULT NULL,
  `TYPE` varchar(1) DEFAULT NULL,
  `FUN_TYPE` varchar(1) DEFAULT NULL,
  `TXN_TYPE` varchar(1) DEFAULT NULL,
  `REG_TYPE` varchar(1) DEFAULT NULL,
  `OBJ_DESC` varchar(62) DEFAULT NULL,
  `ARE_DESC` varchar(62) DEFAULT NULL,
  `AST_AMT` decimal(12,0) DEFAULT NULL,
  `NAV_ORG` decimal(12,4) DEFAULT NULL,
  `FUN_INTT` varchar(1) DEFAULT NULL,
  `F_NM_C` varchar(50) DEFAULT NULL,
  `K_NM_C` varchar(50) DEFAULT NULL,
  `M_COST` varchar(20) DEFAULT NULL,
  `K_COST` varchar(20) DEFAULT NULL,
  `R_COST` varchar(20) DEFAULT NULL,
  `O_COST` varchar(20) DEFAULT NULL,
  `MF_REP` varchar(1) DEFAULT NULL,
  `AF_REP` varchar(1) DEFAULT NULL,
  `INV_PER` decimal(6,2) DEFAULT NULL,
  `M_NM_C` varchar(10) DEFAULT NULL,
  `OPR_DATE` date DEFAULT NULL,
  `EXP` varchar(40) DEFAULT NULL,
  `EDUCATE` varchar(30) DEFAULT NULL,
  `ABREV` varchar(3) DEFAULT NULL,
  `OBJ_CODE` varchar(14) DEFAULT NULL,
  `ARE_CODE` varchar(14) DEFAULT NULL,
  `MGR_ID` varchar(5) DEFAULT NULL,
  `RTN_RANGE` decimal(7,2) DEFAULT NULL,
  `CO_CODE` varchar(9) DEFAULT NULL,
  `RR_RATE` varchar(1) DEFAULT NULL,
  `FUN_S_AST` decimal(12,0) DEFAULT NULL,
  `ITC_CODE` varchar(5) DEFAULT NULL,
  `ITC_NM_C` varchar(50) DEFAULT NULL,
  `BUY_CURR` varchar(3) DEFAULT NULL,
  `SNG_LO_P` decimal(12,0) DEFAULT NULL,
  `REG_LO_P` decimal(8,0) DEFAULT NULL,
  `H_COST` varchar(20) DEFAULT NULL,
  `INV_OBJ` text,
  `NAV_FLAG` varchar(1) DEFAULT NULL,
  `TYPE_N` varchar(5) DEFAULT NULL,
  `NAV_S_DATE` date DEFAULT NULL,
  `FUN_BMRK` varchar(1) DEFAULT NULL,
  `INV_LIMI` varchar(60) DEFAULT NULL,
  `ORG_CODE` varchar(5) DEFAULT NULL,
  `FUN_INTD` varchar(23) DEFAULT NULL,
  `END_DATE` date DEFAULT NULL,
  `FCT_CODE` varchar(5) DEFAULT NULL,
  `KCT_CODE` varchar(5) DEFAULT NULL,
  `TYPE_F` varchar(1) DEFAULT NULL,
  `TRAD_UNIT` decimal(5,0) DEFAULT NULL,
  `ISIN_CODE` varchar(12) DEFAULT NULL,
  `ETF_TYPE` varchar(1) DEFAULT NULL,
  `BS_CODE` varchar(6) DEFAULT NULL,
  `FUND_TYPE` tinyint(4) NOT NULL DEFAULT '0',
  `MarketLevelD_ID` int(8) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNBBA
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNBBA`;

CREATE TABLE `FUNBBA` (
  `FUN_CODE` varchar(7) NOT NULL DEFAULT '',
  `NM_C` varchar(16) DEFAULT NULL,
  `NM_C_F` varchar(60) DEFAULT NULL,
  `NM_E` varchar(60) DEFAULT NULL,
  `START_DATE` date DEFAULT NULL,
  `NAV_E_DATE` date DEFAULT NULL,
  `FUN_MARK` varchar(1) DEFAULT NULL,
  `FUN_REG` varchar(15) DEFAULT NULL,
  `CUR_NM` varchar(6) DEFAULT NULL,
  `TYPE` varchar(1) DEFAULT NULL,
  `FUN_TYPE` varchar(1) DEFAULT NULL,
  `TXN_TYPE` varchar(1) DEFAULT NULL,
  `REG_TYPE` varchar(1) DEFAULT NULL,
  `OBJ_DESC` varchar(62) DEFAULT NULL,
  `ARE_DESC` varchar(62) DEFAULT NULL,
  `AST_AMT` decimal(12,0) DEFAULT NULL,
  `NAV_ORG` decimal(12,4) DEFAULT NULL,
  `FUN_INTT` varchar(1) DEFAULT NULL,
  `F_NM_C` varchar(50) DEFAULT NULL,
  `K_NM_C` varchar(50) DEFAULT NULL,
  `M_COST` varchar(20) DEFAULT NULL,
  `K_COST` varchar(20) DEFAULT NULL,
  `R_COST` varchar(20) DEFAULT NULL,
  `O_COST` varchar(20) DEFAULT NULL,
  `MF_REP` varchar(1) DEFAULT NULL,
  `AF_REP` varchar(1) DEFAULT NULL,
  `INV_PER` decimal(6,2) DEFAULT NULL,
  `M_NM_C` varchar(10) DEFAULT NULL,
  `OPR_DATE` date DEFAULT NULL,
  `EXP` varchar(40) DEFAULT NULL,
  `EDUCATE` varchar(30) DEFAULT NULL,
  `ABREV` varchar(3) DEFAULT NULL,
  `OBJ_CODE` varchar(14) DEFAULT NULL,
  `ARE_CODE` varchar(14) DEFAULT NULL,
  `MGR_ID` varchar(5) DEFAULT NULL,
  `RTN_RANGE` decimal(7,2) DEFAULT NULL,
  `CO_CODE` varchar(9) DEFAULT NULL,
  `RR_RATE` varchar(1) DEFAULT NULL,
  `ITC_CODE` varchar(5) DEFAULT NULL,
  `ITC_NM_C` varchar(50) DEFAULT NULL,
  `ISIN_CODE` varchar(12) DEFAULT NULL,
  `FSC_ID` varchar(30) DEFAULT NULL,
  `BUY_CURR` varchar(3) DEFAULT NULL,
  `SNG_LO_P` decimal(12,0) DEFAULT NULL,
  `REG_LO_P` decimal(8,0) DEFAULT NULL,
  `H_COST` varchar(20) DEFAULT NULL,
  `NORPT_RMK` varchar(12) DEFAULT NULL,
  `INV_OBJ` text,
  `NAV_FLAG` varchar(1) DEFAULT NULL,
  `TYPE_N` varchar(5) DEFAULT NULL,
  `AST_CUR` varchar(3) DEFAULT NULL,
  `NAV_S_DATE` date DEFAULT NULL,
  `FUN_BMRK` varchar(1) DEFAULT NULL,
  `INV_LIMI` varchar(60) DEFAULT NULL,
  `FUN_INTD` varchar(23) DEFAULT NULL,
  `END_DATE` date DEFAULT NULL,
  `S_IN_DATE` date DEFAULT NULL,
  `E_IN_DATE` date DEFAULT NULL,
  `FUN_LAMT` varchar(2) DEFAULT NULL,
  `FCT_CODE` varchar(5) DEFAULT NULL,
  `KCT_CODE` varchar(5) DEFAULT NULL,
  `ACTV_DATE` date DEFAULT NULL,
  `TRAD_UNIT` decimal(5,0) DEFAULT NULL,
  `FUND_TYPE` tinyint(4) NOT NULL DEFAULT '1',
  `MarketLevelD_ID` int(8) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNBDT
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNBDT`;

CREATE TABLE `FUNBDT` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `DTR_YMD1` date NOT NULL,
  `DTR_AMT` decimal(12,6) DEFAULT NULL,
  `DTR_YMD2` date DEFAULT NULL,
  `FUND_TYPE` tinyint(4) NOT NULL DEFAULT '1',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`DTR_YMD1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNBNA
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNBNA`;

CREATE TABLE `FUNBNA` (
  `FUN_CODE` varchar(7) NOT NULL DEFAULT '',
  `YMD` date NOT NULL,
  `CUR_NM` varchar(10) DEFAULT NULL,
  `NAV_ORG` decimal(12,4) DEFAULT NULL,
  `NAV_NT` decimal(12,4) DEFAULT NULL,
  `RET_1D` decimal(8,2) DEFAULT NULL,
  `RET_1M` decimal(8,2) DEFAULT NULL,
  `RET_3M` decimal(8,2) DEFAULT NULL,
  `RET_6M` decimal(8,2) DEFAULT NULL,
  `RET_9M` decimal(8,2) DEFAULT NULL,
  `RET_1Y` decimal(8,2) DEFAULT NULL,
  `RET_2Y` decimal(8,2) DEFAULT NULL,
  `RET_3Y` decimal(8,2) DEFAULT NULL,
  `RET_CUR_Y` decimal(8,2) DEFAULT NULL,
  `RET_BGN` decimal(8,2) DEFAULT NULL,
  `RET_L_M` decimal(8,2) DEFAULT NULL,
  `RET_L_Q` decimal(8,2) DEFAULT NULL,
  `UP_DN` decimal(10,4) DEFAULT NULL,
  `RET_5Y` decimal(8,2) DEFAULT NULL,
  `UP_DN_NT` decimal(12,4) DEFAULT NULL,
  `RET_1D_NT` decimal(8,2) DEFAULT NULL,
  `RET_1W` decimal(8,2) DEFAULT NULL,
  `FUND_TYPE` tinyint(4) NOT NULL DEFAULT '1',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`YMD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNBNQ
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNBNQ`;

CREATE TABLE `FUNBNQ` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `YQ` varchar(5) NOT NULL DEFAULT '',
  `NAV_ORG` decimal(12,4) DEFAULT NULL,
  `NAV_NT` decimal(12,4) DEFAULT NULL,
  `RTN_ORG` decimal(7,2) DEFAULT NULL,
  `RTN_NT` decimal(7,2) DEFAULT NULL,
  `RTN_6M` decimal(7,2) DEFAULT NULL,
  `RTN_9M` decimal(7,2) DEFAULT NULL,
  `RTN_1Y` decimal(7,2) DEFAULT NULL,
  `RTN_2Y` decimal(7,2) DEFAULT NULL,
  `RTN_3Y` decimal(7,2) DEFAULT NULL,
  `CUR_NM` varchar(10) DEFAULT NULL,
  `UP_DN` decimal(12,4) DEFAULT NULL,
  `UP_DN_NT` decimal(12,4) DEFAULT NULL,
  `RET_3M` decimal(8,2) DEFAULT NULL,
  `RET_6M` decimal(8,2) DEFAULT NULL,
  `RET_9M` decimal(8,2) DEFAULT NULL,
  `RET_1Y` decimal(8,2) DEFAULT NULL,
  `RET_2Y` decimal(8,2) DEFAULT NULL,
  `RET_3Y` decimal(8,2) DEFAULT NULL,
  `RET_5Y` decimal(8,2) DEFAULT NULL,
  `RET_CUR_Y` decimal(8,2) DEFAULT NULL,
  `RET_BGN` decimal(8,2) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`YQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNBRT
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNBRT`;

CREATE TABLE `FUNBRT` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `YMD` date NOT NULL,
  `RET_3M` decimal(7,2) DEFAULT NULL,
  `RET_6M` decimal(7,2) DEFAULT NULL,
  `RET_9M` decimal(7,2) DEFAULT NULL,
  `RET_1Y` decimal(7,2) DEFAULT NULL,
  `RET_2Y` decimal(7,2) DEFAULT NULL,
  `RET_3Y` decimal(7,2) DEFAULT NULL,
  `RET_5Y` decimal(7,2) DEFAULT NULL,
  `FUND_TYPE` tinyint(4) NOT NULL DEFAULT '1',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`YMD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNCOD
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNCOD`;

CREATE TABLE `FUNCOD` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `NM_C` varchar(16) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNCOR
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNCOR`;

CREATE TABLE `FUNCOR` (
  `TYPE` varchar(1) NOT NULL DEFAULT '',
  `XCT_CODE` varchar(5) NOT NULL DEFAULT '',
  `NM_C_F` varchar(50) DEFAULT NULL,
  `NM_E_F` varchar(50) DEFAULT NULL,
  `ADD_C` varchar(70) DEFAULT NULL,
  `ADD_E` varchar(70) DEFAULT NULL,
  `TEL` varchar(31) DEFAULT NULL,
  `FAX` varchar(15) DEFAULT NULL,
  `CON_PER` varchar(30) DEFAULT NULL,
  `PRE_NM_C` varchar(30) DEFAULT NULL,
  `PRE_NM_E` varchar(50) DEFAULT NULL,
  `GEL_NM_C` varchar(30) DEFAULT NULL,
  `GEL_NM_E` varchar(50) DEFAULT NULL,
  `URL` varchar(60) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`TYPE`,`XCT_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNDATA
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNDATA`;

CREATE TABLE `FUNDATA` (
  `FUN_CODE` varchar(7) NOT NULL DEFAULT '',
  `NM_C` varchar(16) DEFAULT NULL,
  `NM_C_F` varchar(60) DEFAULT NULL,
  `NM_E` varchar(60) DEFAULT NULL,
  `START_DATE` date DEFAULT NULL,
  `NAV_E_DATE` date DEFAULT NULL,
  `FUN_MARK` varchar(1) DEFAULT NULL,
  `FUN_REG` varchar(15) DEFAULT NULL,
  `CUR_NM` varchar(6) DEFAULT NULL,
  `TYPE` varchar(1) DEFAULT NULL,
  `FUN_TYPE` varchar(1) DEFAULT NULL,
  `TXN_TYPE` varchar(1) DEFAULT NULL,
  `REG_TYPE` varchar(1) DEFAULT NULL,
  `OBJ_DESC` varchar(62) DEFAULT NULL,
  `ARE_DESC` varchar(62) DEFAULT NULL,
  `AST_AMT` decimal(12,0) DEFAULT NULL,
  `NAV_ORG` decimal(12,4) DEFAULT NULL,
  `FUN_INTT` varchar(1) DEFAULT NULL,
  `F_NM_C` varchar(50) DEFAULT NULL,
  `K_NM_C` varchar(50) DEFAULT NULL,
  `M_COST` varchar(20) DEFAULT NULL,
  `K_COST` varchar(20) DEFAULT NULL,
  `R_COST` varchar(20) DEFAULT NULL,
  `O_COST` varchar(20) DEFAULT NULL,
  `MF_REP` varchar(1) DEFAULT NULL,
  `AF_REP` varchar(1) DEFAULT NULL,
  `INV_PER` decimal(6,2) DEFAULT NULL,
  `M_NM_C` varchar(10) DEFAULT NULL,
  `OPR_DATE` date DEFAULT NULL,
  `EXP` varchar(40) DEFAULT NULL,
  `EDUCATE` varchar(30) DEFAULT NULL,
  `ABREV` varchar(3) DEFAULT NULL,
  `OBJ_CODE` varchar(14) DEFAULT NULL,
  `ARE_CODE` varchar(14) DEFAULT NULL,
  `MGR_ID` varchar(5) DEFAULT NULL,
  `RTN_RANGE` decimal(7,2) DEFAULT NULL,
  `CO_CODE` varchar(9) DEFAULT NULL,
  `RR_RATE` varchar(1) DEFAULT NULL,
  `ITC_CODE` varchar(5) DEFAULT NULL,
  `ITC_NM_C` varchar(50) DEFAULT NULL,
  `BUY_CURR` varchar(3) DEFAULT NULL,
  `SNG_LO_P` decimal(12,0) DEFAULT NULL,
  `REG_LO_P` decimal(8,0) DEFAULT NULL,
  `H_COST` varchar(20) DEFAULT NULL,
  `INV_OBJ` text,
  `NAV_FLAG` varchar(1) DEFAULT NULL,
  `TYPE_N` varchar(5) DEFAULT NULL,
  `NAV_S_DATE` date DEFAULT NULL,
  `FUN_BMRK` varchar(1) DEFAULT NULL,
  `INV_LIMI` varchar(60) DEFAULT NULL,
  `FUN_INTD` varchar(23) DEFAULT NULL,
  `END_DATE` date DEFAULT NULL,
  `FCT_CODE` varchar(5) DEFAULT NULL,
  `KCT_CODE` varchar(5) DEFAULT NULL,
  `TRAD_UNIT` decimal(5,0) DEFAULT NULL,
  `ISIN_CODE` varchar(12) DEFAULT NULL,
  `FUND_TYPE` tinyint(4) DEFAULT NULL,
  `MARKET_ID` int(8) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNDTR
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNDTR`;

CREATE TABLE `FUNDTR` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `DTR_YMD1` date NOT NULL,
  `DTR_AMT` decimal(12,6) DEFAULT NULL,
  `DTR_YMD2` date DEFAULT NULL,
  `FUND_TYPE` tinyint(4) NOT NULL DEFAULT '0',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`DTR_YMD1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNDTR_DATA
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNDTR_DATA`;

CREATE TABLE `FUNDTR_DATA` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `DTR_YMD1` date NOT NULL,
  `DTR_AMT` decimal(12,6) DEFAULT NULL,
  `DTR_YMD2` date DEFAULT NULL,
  `FUND_TYPE` tinyint(4) NOT NULL DEFAULT '0',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`DTR_YMD1`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNFEM
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNFEM`;

CREATE TABLE `FUNFEM` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `YM` varchar(6) NOT NULL DEFAULT '',
  `DC_HDCHG` decimal(18,0) DEFAULT NULL,
  `DC_HDCHGR` decimal(5,2) DEFAULT NULL,
  `DC_TRNTAX` decimal(18,0) DEFAULT NULL,
  `DC_TRNTAXR` decimal(5,2) DEFAULT NULL,
  `DC_TOTAL` decimal(18,0) DEFAULT NULL,
  `DC_RATIO` decimal(5,2) DEFAULT NULL,
  `EP_MGNFEE` decimal(18,0) DEFAULT NULL,
  `EP_MGNFEER` decimal(5,2) DEFAULT NULL,
  `EP_KEPFEE` decimal(18,0) DEFAULT NULL,
  `EP_KEPFEER` decimal(5,2) DEFAULT NULL,
  `EP_GENFEE` decimal(18,0) DEFAULT NULL,
  `EP_GENFEER` decimal(5,2) DEFAULT NULL,
  `EP_OTHFEE` decimal(18,0) DEFAULT NULL,
  `EP_OTHFEER` decimal(5,2) DEFAULT NULL,
  `EP_TOTAL` decimal(18,0) DEFAULT NULL,
  `EP_RATIO` decimal(5,2) DEFAULT NULL,
  `EP_DC_TOT` decimal(18,0) DEFAULT NULL,
  `EP_DC_RAT` decimal(5,2) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`YM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNIDM
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNIDM`;

CREATE TABLE `FUNIDM` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `IDX_CODE` varchar(8) NOT NULL DEFAULT '',
  `YM` varchar(6) NOT NULL DEFAULT '',
  `DATA_VALUE` decimal(7,2) DEFAULT NULL,
  `FUND_TYPE` tinyint(4) NOT NULL DEFAULT '0',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`IDX_CODE`,`YM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNIDM_DATA
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNIDM_DATA`;

CREATE TABLE `FUNIDM_DATA` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `IDX_CODE` varchar(8) NOT NULL DEFAULT '',
  `YM` varchar(6) NOT NULL DEFAULT '',
  `DATA_VALUE` decimal(7,2) DEFAULT NULL,
  `FUND_TYPE` tinyint(4) NOT NULL DEFAULT '0',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`IDX_CODE`,`YM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNITB
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNITB`;

CREATE TABLE `FUNITB` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `ITC_CODE` varchar(5) NOT NULL DEFAULT '',
  `TYPE` varchar(1) NOT NULL DEFAULT '',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`ITC_CODE`,`TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNITC
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNITC`;

CREATE TABLE `FUNITC` (
  `ITC_CODE` varchar(5) NOT NULL DEFAULT '',
  `NM_C` varchar(50) DEFAULT NULL,
  `PRE_NM` varchar(10) DEFAULT NULL,
  `GEL_NM` varchar(10) DEFAULT NULL,
  `ZIP_CODE` varchar(5) DEFAULT NULL,
  `ADDRESS` varchar(80) DEFAULT NULL,
  `TEL` varchar(15) DEFAULT NULL,
  `FAX` varchar(15) DEFAULT NULL,
  `URL` varchar(70) DEFAULT NULL,
  `END_DATE` date DEFAULT NULL,
  `MEMO` text,
  `CT_CODE` varchar(5) DEFAULT NULL,
  `SITE_YMD` date DEFAULT NULL,
  `SICE_YMD` date DEFAULT NULL,
  `SBSITE_YMD` date DEFAULT NULL,
  `SBSICE_YMD` date DEFAULT NULL,
  `OFUN_YMD` date DEFAULT NULL,
  `FUTURE_YMD` date DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ITC_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNITM
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNITM`;

CREATE TABLE `FUNITM` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `ITC_CODE` varchar(5) NOT NULL DEFAULT '',
  `TYPE` varchar(1) NOT NULL DEFAULT '',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`ITC_CODE`,`TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNMGC
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNMGC`;

CREATE TABLE `FUNMGC` (
  `MGR_ID` varchar(5) NOT NULL DEFAULT '',
  `NM_C` varchar(30) DEFAULT NULL,
  `EXP1` varchar(40) DEFAULT NULL,
  `EXP2` varchar(40) DEFAULT NULL,
  `EXP3` varchar(40) DEFAULT NULL,
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `OPR_DATE` date NOT NULL,
  `END_DATE` date DEFAULT NULL,
  `EDUCATE` varchar(30) DEFAULT NULL,
  `TYPRTN_RANGEE` varchar(7) DEFAULT NULL,
  `EXP4` varchar(40) DEFAULT NULL,
  `EXP5` varchar(40) DEFAULT NULL,
  `EXP6` varchar(40) DEFAULT NULL,
  `EXP7` varchar(40) DEFAULT NULL,
  `EXP8` varchar(40) DEFAULT NULL,
  `EXP9` varchar(40) DEFAULT NULL,
  `EXP10` varchar(40) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MGR_ID`,`FUN_CODE`,`OPR_DATE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNMIP
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNMIP`;

CREATE TABLE `FUNMIP` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `INV_CODE` varchar(3) NOT NULL DEFAULT '',
  `YM` varchar(7) NOT NULL DEFAULT '',
  `INV_PER` varchar(6) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`INV_CODE`,`YM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNNAQ
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNNAQ`;

CREATE TABLE `FUNNAQ` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `YQ` varchar(5) NOT NULL DEFAULT '',
  `NAV_ORG` decimal(12,4) DEFAULT NULL,
  `NAV_NT` decimal(12,4) DEFAULT NULL,
  `RTN_NT` decimal(7,2) DEFAULT NULL,
  `RTN_ORG` decimal(7,2) DEFAULT NULL,
  `RTN_6M` decimal(7,2) DEFAULT NULL,
  `RTN_9M` decimal(7,2) DEFAULT NULL,
  `RTN_1Y` decimal(7,2) DEFAULT NULL,
  `RTN_2Y` decimal(7,2) DEFAULT NULL,
  `RTN_3Y` decimal(7,2) DEFAULT NULL,
  `CUR_NM` varchar(10) DEFAULT NULL,
  `UP_DN` decimal(12,4) DEFAULT NULL,
  `UP_DN_NT` decimal(12,4) DEFAULT NULL,
  `N_C_R` decimal(7,2) DEFAULT NULL,
  `N_C_D` decimal(12,4) DEFAULT NULL,
  `PRICE` decimal(7,2) DEFAULT NULL,
  `CLOSE_UP_DN` decimal(7,2) DEFAULT NULL,
  `CLOSE_UP_DN_R` decimal(7,2) DEFAULT NULL,
  `RET_3M` decimal(8,2) DEFAULT NULL,
  `RET_6M` decimal(8,2) DEFAULT NULL,
  `RET_9M` decimal(8,2) DEFAULT NULL,
  `RET_1Y` decimal(8,2) DEFAULT NULL,
  `RET_2Y` decimal(8,2) DEFAULT NULL,
  `RET_3Y` decimal(8,2) DEFAULT NULL,
  `RET_5Y` decimal(8,2) DEFAULT NULL,
  `RET_CUR_Y` decimal(8,2) DEFAULT NULL,
  `RET_BGN` decimal(8,2) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`YQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNNAV
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNNAV`;

CREATE TABLE `FUNNAV` (
  `FUN_CODE` varchar(7) NOT NULL DEFAULT '',
  `YMD` date NOT NULL,
  `NAV_ORG` decimal(12,4) DEFAULT NULL,
  `N_C_R` decimal(7,2) DEFAULT NULL,
  `N_C_D` decimal(12,4) DEFAULT NULL,
  `PRICE` decimal(7,2) DEFAULT NULL,
  `RET_1D` decimal(8,2) DEFAULT NULL,
  `AVG_N_C_R` decimal(7,2) DEFAULT NULL,
  `RET_1M` decimal(8,2) DEFAULT NULL,
  `RET_3M` decimal(8,2) DEFAULT NULL,
  `RET_6M` decimal(8,2) DEFAULT NULL,
  `RET_9M` decimal(8,2) DEFAULT NULL,
  `RET_1Y` decimal(8,2) DEFAULT NULL,
  `RET_2Y` decimal(8,2) DEFAULT NULL,
  `RET_3Y` decimal(8,2) DEFAULT NULL,
  `RET_CUR_Y` decimal(8,2) DEFAULT NULL,
  `RET_BGN` decimal(8,2) DEFAULT NULL,
  `RET_L_M` decimal(8,2) DEFAULT NULL,
  `RET_L_Q` decimal(8,2) DEFAULT NULL,
  `UP_DN` decimal(10,4) DEFAULT NULL,
  `CLOSE_UP_DN` decimal(7,2) DEFAULT NULL,
  `CLOSE_UP_DN_R` decimal(7,2) DEFAULT NULL,
  `RET_5Y` decimal(8,2) DEFAULT NULL,
  `CUR_NM` varchar(10) DEFAULT NULL,
  `NAV_NT` decimal(12,4) DEFAULT NULL,
  `UP_DN_NT` decimal(12,4) DEFAULT NULL,
  `RET_1D_NT` decimal(8,2) DEFAULT NULL,
  `RET_1W` decimal(8,2) DEFAULT NULL,
  `FUND_TYPE` tinyint(4) NOT NULL DEFAULT '0',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`YMD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNNAV_DATA
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNNAV_DATA`;

CREATE TABLE `FUNNAV_DATA` (
  `FUN_CODE` varchar(7) NOT NULL DEFAULT '',
  `YMD` date NOT NULL,
  `NAV_ORG` decimal(12,4) DEFAULT NULL,
  `RET_1D` decimal(8,2) DEFAULT NULL,
  `RET_1M` decimal(8,2) DEFAULT NULL,
  `RET_3M` decimal(8,2) DEFAULT NULL,
  `RET_6M` decimal(8,2) DEFAULT NULL,
  `RET_9M` decimal(8,2) DEFAULT NULL,
  `RET_1Y` decimal(8,2) DEFAULT NULL,
  `RET_2Y` decimal(8,2) DEFAULT NULL,
  `RET_3Y` decimal(8,2) DEFAULT NULL,
  `RET_CUR_Y` decimal(8,2) DEFAULT NULL,
  `RET_BGN` decimal(8,2) DEFAULT NULL,
  `RET_L_M` decimal(8,2) DEFAULT NULL,
  `RET_L_Q` decimal(8,2) DEFAULT NULL,
  `UP_DN` decimal(10,4) DEFAULT NULL,
  `RET_5Y` decimal(8,2) DEFAULT NULL,
  `CUR_NM` varchar(10) DEFAULT NULL,
  `NAV_NT` decimal(12,4) DEFAULT NULL,
  `UP_DN_NT` decimal(12,4) DEFAULT NULL,
  `RET_1D_NT` decimal(8,2) DEFAULT NULL,
  `RET_1W` decimal(8,2) DEFAULT NULL,
  `FUND_TYPE` tinyint(4) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`YMD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNNAVC
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNNAVC`;

CREATE TABLE `FUNNAVC` (
  `YMD` date NOT NULL,
  `LSC_CODE` varchar(6) NOT NULL DEFAULT '',
  `NM_C` varchar(12) DEFAULT NULL,
  `MKT` varchar(4) DEFAULT NULL,
  `ZCLOSE` decimal(7,2) DEFAULT NULL,
  `PREV_CLOSE` decimal(7,2) DEFAULT NULL,
  `AVG_CLOSE_3M` decimal(7,2) DEFAULT NULL,
  `STKNO` decimal(15,3) DEFAULT NULL,
  `NAV_NT` decimal(12,4) DEFAULT NULL,
  `CO_CODE` varchar(9) DEFAULT NULL,
  `CO_NM_C` varchar(14) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`YMD`,`LSC_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNNAVO
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNNAVO`;

CREATE TABLE `FUNNAVO` (
  `YMD` date NOT NULL,
  `FUN_CODE` varchar(7) NOT NULL DEFAULT '',
  `NM_C_F` varchar(60) DEFAULT NULL,
  `CO_CODE` varchar(9) DEFAULT NULL,
  `NAV_NT` decimal(12,4) DEFAULT NULL,
  `PREV_NAV` decimal(12,4) DEFAULT NULL,
  `AVG_NAV_NT_3M` decimal(12,4) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`YMD`,`FUN_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNOBJ
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNOBJ`;

CREATE TABLE `FUNOBJ` (
  `OBJ_CODE` varchar(4) NOT NULL DEFAULT '',
  `OBJ_DESC` varchar(20) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`OBJ_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNRET_DATA
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNRET_DATA`;

CREATE TABLE `FUNRET_DATA` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `YMD` date NOT NULL,
  `RET_3M` decimal(7,2) DEFAULT NULL,
  `RET_6M` decimal(7,2) DEFAULT NULL,
  `RET_9M` decimal(7,2) DEFAULT NULL,
  `RET_1Y` decimal(7,2) DEFAULT NULL,
  `RET_2Y` decimal(7,2) DEFAULT NULL,
  `RET_3Y` decimal(7,2) DEFAULT NULL,
  `RET_5Y` decimal(7,2) DEFAULT NULL,
  `FUND_TYPE` tinyint(4) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`YMD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNRET1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNRET1`;

CREATE TABLE `FUNRET1` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `YMD` date NOT NULL,
  `RET_3M` decimal(7,2) DEFAULT NULL,
  `RET_6M` decimal(7,2) DEFAULT NULL,
  `RET_9M` decimal(7,2) DEFAULT NULL,
  `RET_1Y` decimal(7,2) DEFAULT NULL,
  `RET_2Y` decimal(7,2) DEFAULT NULL,
  `RET_3Y` decimal(7,2) DEFAULT NULL,
  `RET_5Y` decimal(7,2) DEFAULT NULL,
  `FUND_TYPE` tinyint(4) NOT NULL DEFAULT '0',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`,`YMD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNSCU
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNSCU`;

CREATE TABLE `FUNSCU` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `LIST_CODE` varchar(6) DEFAULT NULL,
  `NM_C4` varchar(8) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`FUN_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FUNWIP
# ------------------------------------------------------------

DROP TABLE IF EXISTS `FUNWIP`;

CREATE TABLE `FUNWIP` (
  `FUN_CODE` varchar(4) NOT NULL DEFAULT '',
  `INV_CODE` varchar(3) NOT NULL DEFAULT '',
  `YMW` varchar(7) NOT NULL DEFAULT '',
  `INV_PER` varchar(6) DEFAULT NULL,
  `UP_DN` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`FUN_CODE`,`INV_CODE`,`YMW`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Investconsult
# ------------------------------------------------------------

DROP TABLE IF EXISTS `Investconsult`;

CREATE TABLE `Investconsult` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `YQ` varchar(8) NOT NULL DEFAULT '',
  `INVESTCONSULT` text NOT NULL,
  `DATA` text NOT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='投顧看法';



# Dump of table MarketLevelA
# ------------------------------------------------------------

DROP TABLE IF EXISTS `MarketLevelA`;

CREATE TABLE `MarketLevelA` (
  `ID` int(7) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(30) NOT NULL DEFAULT '',
  `Description` text,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分類';



# Dump of table MarketLevelB
# ------------------------------------------------------------

DROP TABLE IF EXISTS `MarketLevelB`;

CREATE TABLE `MarketLevelB` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) NOT NULL DEFAULT '',
  `UpMarketLevelID` int(8) DEFAULT NULL,
  `ABREV` varchar(6) NOT NULL DEFAULT '',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='投顧看法';



# Dump of table MarketLevelC
# ------------------------------------------------------------

DROP TABLE IF EXISTS `MarketLevelC`;

CREATE TABLE `MarketLevelC` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) NOT NULL DEFAULT '',
  `BenchmarkName` varchar(100) NOT NULL DEFAULT '',
  `UpMarketLevelID` int(8) DEFAULT NULL,
  `ECOIndicator_Field` varchar(30) DEFAULT NULL,
  `ECO_1` varchar(50) DEFAULT NULL,
  `ECO_1_MEMO` varchar(100) DEFAULT NULL,
  `ECO_1_SETTING` tinyint(4) DEFAULT NULL,
  `ECO_2` varchar(50) DEFAULT NULL,
  `ECO_2_MEMO` varchar(100) DEFAULT NULL,
  `ECO_2_SETTING` tinyint(4) DEFAULT NULL,
  `ECO_3` varchar(50) DEFAULT NULL,
  `ECO_3_MEMO` varchar(100) DEFAULT NULL,
  `ECO_3_SETTING` tinyint(4) DEFAULT NULL,
  `ECO_4` varchar(50) DEFAULT NULL,
  `ECO_4_MEMO` varchar(100) DEFAULT NULL,
  `ECO_4_SETTING` tinyint(4) DEFAULT NULL,
  `ECO_5` varchar(50) DEFAULT NULL,
  `ECO_5_MEMO` varchar(100) DEFAULT NULL,
  `ECO_5_SETTING` tinyint(4) DEFAULT NULL,
  `ECO_6` varchar(50) DEFAULT NULL,
  `ECO_6_MEMO` varchar(100) DEFAULT NULL,
  `ECO_6_SETTING` tinyint(4) DEFAULT NULL,
  `ECO_7` varchar(50) DEFAULT NULL,
  `ECO_7_MEMO` varchar(100) DEFAULT NULL,
  `ECO_7_SETTING` tinyint(4) DEFAULT NULL,
  `ECO_8` varchar(50) DEFAULT NULL,
  `ECO_8_MEMO` varchar(100) DEFAULT NULL,
  `ECO_8_SETTING` tinyint(4) DEFAULT NULL,
  `ECO_9` varchar(50) DEFAULT NULL,
  `ECO_9_MEMO` varchar(100) DEFAULT NULL,
  `ECO_9_SETTING` tinyint(4) DEFAULT NULL,
  `ECO_10` varchar(50) DEFAULT NULL,
  `ECO_10_MEMO` varchar(100) DEFAULT NULL,
  `ECO_10_SETTING` tinyint(4) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='大市';



# Dump of table MarketLevelC_BenchMark
# ------------------------------------------------------------

DROP TABLE IF EXISTS `MarketLevelC_BenchMark`;

CREATE TABLE `MarketLevelC_BenchMark` (
  `MarketID` int(8) NOT NULL,
  `YMD` date NOT NULL,
  `BenchMark_Value` decimal(10,5) DEFAULT '0.00000',
  `BenchMark_Quantity` decimal(10,5) DEFAULT '0.00000',
  `EMA_12` decimal(10,5) DEFAULT '0.00000',
  `EMA_20` decimal(10,5) DEFAULT '0.00000',
  `EMA_24` decimal(10,5) DEFAULT '0.00000',
  `EMA_60` decimal(10,5) DEFAULT '0.00000',
  `EMA_260` decimal(10,5) DEFAULT '0.00000',
  `RSI_1` decimal(10,5) DEFAULT '0.00000',
  `MACD_1` decimal(10,5) DEFAULT '0.00000',
  `MACD_2` decimal(10,5) DEFAULT '0.00000',
  `MACD_3` decimal(10,5) DEFAULT '0.00000',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MarketID`,`YMD`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='大市BenchMark';



# Dump of table MarketLevelD
# ------------------------------------------------------------

DROP TABLE IF EXISTS `MarketLevelD`;

CREATE TABLE `MarketLevelD` (
  `ID` int(8) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) NOT NULL DEFAULT '',
  `UpMarketLevelID` int(8) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='小市';



# Dump of table MarketLevelD_BenchMark
# ------------------------------------------------------------

DROP TABLE IF EXISTS `MarketLevelD_BenchMark`;

CREATE TABLE `MarketLevelD_BenchMark` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `MarketID` int(8) NOT NULL,
  `DateTime` datetime NOT NULL,
  `BenchMark_Value` decimal(10,5) DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='小市BenchMark';



# Dump of table USER
# ------------------------------------------------------------

DROP TABLE IF EXISTS `USER`;

CREATE TABLE `USER` (
  `USER_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PASSWORD` char(50) NOT NULL DEFAULT '',
  `EMAIL` varchar(120) NOT NULL DEFAULT '',
  `NAME` varchar(80) DEFAULT NULL,
  `BUDGET` int(11) DEFAULT NULL,
  `AGE` smallint(6) DEFAULT NULL,
  `GANDER` varchar(6) DEFAULT NULL,
  `ASSETS` smallint(6) DEFAULT NULL,
  `WORKS` smallint(6) DEFAULT NULL,
  `INFOSOURCE` smallint(6) DEFAULT NULL,
  `INVESTMENT` smallint(6) DEFAULT NULL,
  `OBJECTIVE` smallint(6) DEFAULT NULL,
  `PROFIT` smallint(6) DEFAULT NULL,
  `LOSSSITUATION` smallint(6) DEFAULT NULL,
  `DURING` smallint(6) DEFAULT NULL,
  `RiskTYPE` tinyint(20) DEFAULT NULL,
  `ROLE` tinyint(2) DEFAULT '1',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table USER_DEVICE_INFO
# ------------------------------------------------------------

DROP TABLE IF EXISTS `USER_DEVICE_INFO`;

CREATE TABLE `USER_DEVICE_INFO` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DEIVCE_TYPE` int(11) DEFAULT NULL,
  `DEVICE_ID` int(11) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `ENABLE` tinyint(11) NOT NULL DEFAULT '1',
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table USER_FUNDS_RECORD
# ------------------------------------------------------------

DROP TABLE IF EXISTS `USER_FUNDS_RECORD`;

CREATE TABLE `USER_FUNDS_RECORD` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `YMD` date NOT NULL,
  `FUN_CODE` varchar(7) NOT NULL DEFAULT '',
  `NAV_ORG` decimal(12,4) NOT NULL DEFAULT '0.0000',
  `DTR_AMT` decimal(12,6) NOT NULL DEFAULT '0.000000',
  `QUANTITY` int(11) NOT NULL DEFAULT '0',
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='個人基金資料清單';



# Dump of table USER_TOKEN
# ------------------------------------------------------------

DROP TABLE IF EXISTS `USER_TOKEN`;

CREATE TABLE `USER_TOKEN` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `TOKEN` varchar(100) NOT NULL DEFAULT '',
  `TOKEN_EXPIRE` datetime DEFAULT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`,`USER_ID`,`TOKEN`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table USER_WATCH_FUNDS
# ------------------------------------------------------------

DROP TABLE IF EXISTS `USER_WATCH_FUNDS`;

CREATE TABLE `USER_WATCH_FUNDS` (
  `USER_ID` int(11) NOT NULL,
  `FUN_CODE` varchar(7) NOT NULL DEFAULT '',
  `FUND_TYPE` tinyint(4) NOT NULL,
  `createdate` timestamp NULL DEFAULT NULL,
  `lastupdatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`USER_ID`,`FUN_CODE`,`FUND_TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='個人觀察基金清單';




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
