### Git repository
The source files for this project can be found here: [https://bitbucket.org/ne_plus/backend_api](https://bitbucket.org/ne_plus/backend_api)

To simply get git repository:
```
git clone https://Cartor@bitbucket.org/ne_plus/backend_api.git
```


## Set up Docker Environment 
To simply run docker:
```
docker-compose up -d
```

## Php framework setting
Terminal into docker
```
docker exec -t -i <CONTAINER_NAME> /bin/bash
```

To simply set php composer:
```
cd /usr/share/nginx/html
composer install
```

To test php environment:
```
http://<CONTAINER_IP>:8080/
```